/*
 * generated by Xtext 2.11.0
 */
package org.pragmaticmodeling.pxgen.ide

import com.google.inject.Guice
import org.eclipse.xtext.util.Modules2
import org.pragmaticmodeling.pxgen.PxGenRuntimeModule
import org.pragmaticmodeling.pxgen.PxGenStandaloneSetup

/**
 * Initialization support for running Xtext languages as language servers.
 */
class PxGenIdeSetup extends PxGenStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new PxGenRuntimeModule, new PxGenIdeModule))
	}
	
}
