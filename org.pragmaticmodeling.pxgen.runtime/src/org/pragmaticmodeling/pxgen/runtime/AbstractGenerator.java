package org.pragmaticmodeling.pxgen.runtime;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

public abstract class AbstractGenerator extends AbstractGeneratorFragment implements IGenerator {

	private String rootLocation;
	private String baseName;
	private List<ProjectDescriptor> projects = new ArrayList<ProjectDescriptor>();
	private boolean needsMavenBuild;
	private boolean needsGradleBuild;
	
	private static final Logger logger = Logger.getLogger(AbstractGenerator.class);

	public AbstractGenerator() {
		super();
	}

	@Override
	public void generate() {
		super.generate();
		generateResultFile();
	}

	private void generateResultFile() {
		BufferedWriter writer = null;
		File resultFile = new File(getRootLocation(), ".pxgenProjects").getAbsoluteFile();

		try {
			// create a temporary file
			writer = new BufferedWriter(new FileWriter(resultFile));
			writer.write(projectList(getAllProjects()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				writer.close();
			} catch (IOException e1) {
				logger.error(e1.getMessage());
			}
			if (resultFile.exists()) {
				resultFile.delete();
			}
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
	}

	private String projectList(List<ProjectDescriptor> allProjects) {
		String list = "";
		if (allProjects.size() > 1) {
			for (int i = 0; i < allProjects.size() - 1; i++) {
				list += allProjects.get(i).getLocation() + ", ";
			}
			list += allProjects.get(allProjects.size() - 1).getLocation();
		} else if (!allProjects.isEmpty()) {
			list = allProjects.get(0).getLocation();
		}
		return list;
	}

	@Override
	public IGenerator getGenerator() {
		return this;
	}

	@Override
	public List<ProjectDescriptor> getProjects() {
		return projects;
	}

	@Override
	public String getBaseName() {
		return baseName;
	}

	@Override
	public void setBaseName(String baseName) {
		this.baseName = baseName;
	}

	@Override
	public String getRootLocation() {
		return rootLocation;
	}

	@Override
	public void setRootLocation(String location) {
		this.rootLocation = location;
	}

	@Override
	public List<ProjectDescriptor> getEnabledProjects() {
		List<ProjectDescriptor> enabledProjects = new ArrayList<ProjectDescriptor>();
		for (ProjectDescriptor prj : getAllProjects()) {
			if (prj.isEnabled()) {
				enabledProjects.add(prj);
			}
		}
		return enabledProjects;
	}

	@Override
	public List<ProjectDescriptor> getAllProjectDescriptors() {
		List<ProjectDescriptor> descriptors = new ArrayList<ProjectDescriptor>();
		for (ProjectDescriptor p : getAllProjects()) {
			if (p instanceof ProjectDescriptor) {
				descriptors.add((ProjectDescriptor) p);
			}
		}
		return descriptors;
	}

	@Override
	public boolean needsMavenBuild() {
		return needsMavenBuild;
	}

	@Override
	public boolean needsGradleBuild() {
		return needsGradleBuild;
	}

	@Override
	public void setNeedsGradleBuild(boolean value) {
		this.needsGradleBuild = value;
	}

	@Override
	public void setNeedsMavenBuild(boolean value) {
		this.needsMavenBuild = value;
	}

}
