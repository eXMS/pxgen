package org.pragmaticmodeling.pxgen.runtime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.xtext.xtext.generator.Issues;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

import com.google.inject.Injector;

public abstract class AbstractGeneratorFragment implements IGeneratorFragment {

	Set<IGeneratorFragment> generatorFragments = new HashSet<IGeneratorFragment>();
	
	IGeneratorFragmentConfig parentConfig;

	private List<ProjectDescriptor> projects = new ArrayList<ProjectDescriptor>();

	private IGeneratorFragment parentFragment;
	
	@Override
	public void initialize(Injector injector) {
		injector.injectMembers(this);
	}

	@Override
	public void checkConfiguration(Issues issues) {

	}
	
	@Override
	public IGenerator getGenerator() {
		return getParentFragment().getGenerator();
	}
	
	@Override
	public IGeneratorFragment getParentFragment() {
		return parentFragment;
	}
	
	@Override
	public Set<IGeneratorFragment> getFragments() {
		return generatorFragments;
	}
	
	public IGeneratorFragmentConfig getParentConfig() {
		return parentConfig;
	}
	
	public void setParentConfig(IGeneratorFragmentConfig parentConfig) {
		this.parentConfig = parentConfig;
	}
	
	@Override
	public void addFragment(IGeneratorFragment fragment) {
		this.getFragments().add(fragment);
		fragment.setParentFragment(this);
	}
	
	@Override
	public List<ProjectDescriptor> getProjects() {
		return projects;
	}
	
	@Override
	public List<ProjectDescriptor> getAllProjects() {
		List<ProjectDescriptor> allProjects = new ArrayList<ProjectDescriptor>();
		allProjects.addAll(getProjects());
		for (IGeneratorFragment fragment : getFragments()) {
			allProjects.addAll(fragment.getAllProjects());
		}
		return allProjects;
	}
	
	@Override
	public void addProject(ProjectDescriptor project) {
		getProjects().add(project);
	}
	
	@Override
	public void setParentFragment(IGeneratorFragment parent) {
		this.parentFragment = parent;
	}

	public abstract void doGenerate();
	
	@Override
	public void generate() {
		for (IGeneratorFragment subGen : generatorFragments) {
			subGen.generate();
		}
		doGenerate();
	}
}
