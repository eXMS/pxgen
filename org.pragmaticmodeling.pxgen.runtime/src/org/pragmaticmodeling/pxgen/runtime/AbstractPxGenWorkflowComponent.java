package org.pragmaticmodeling.pxgen.runtime;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.emf.mwe.core.WorkflowContext;
import org.eclipse.emf.mwe.core.issues.Issues;
import org.eclipse.emf.mwe.core.lib.AbstractWorkflowComponent2;
import org.eclipse.emf.mwe.core.monitor.ProgressMonitor;
import org.eclipse.emf.mwe.utils.StandaloneSetup;
import org.eclipse.xtend.lib.annotations.Accessors;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;

public abstract class AbstractPxGenWorkflowComponent extends AbstractWorkflowComponent2 {

	@Inject
	private IProjectCreator projectCreator;
	
	private final static Logger logger = Logger.getLogger(AbstractPxGenWorkflowComponent.class);

	protected void handleException(Exception ex, Issues issues) {
		issues.addError(this, "GeneratorException: ", null, ex, null);
	}

	@Override
	protected void invokeInternal(WorkflowContext arg0, ProgressMonitor arg1, Issues issues) {

		try {
			// cleaner.clean();

			logger.info("Starting Generation...");
			initialize();
			prepareProjects();
			doGenerate();
			finishProjects();
		} catch (Exception e) {
			handleException(e, issues);
		}
	}

	protected abstract void doGenerate();

	public IGeneratorConfig getGeneratorConfig() {
		if (this.getGenerator() != null)
			return (IGeneratorConfig) getGenerator();
		return null;
	}

	public abstract IGenerator getGenerator();

	private void prepareProjects() {
		projectCreator.setProjectDescriptors(getGenerator().getAllProjectDescriptors());
		projectCreator.prepareProjects();
	}
	
	private void finishProjects() {
		projectCreator.setProjectDescriptors(getGenerator().getAllProjectDescriptors());
		projectCreator.finishProjects();
	}

	Set<IGeneratorFragment> generatorFragments = new HashSet<IGeneratorFragment>();

	@Accessors
	XtextGeneratorStandaloneSetup standaloneSetup = new XtextGeneratorStandaloneSetup();

	Injector injector;

	public void initialize() {
		if (injector == null) {
			logger.info("Initializing Xtext generator");
			new StandaloneSetup().addRegisterGeneratedEPackage("org.eclipse.xtext.common.types.TypesPackage");
			// initializeEncoding();
			injector = createInjector();
			injector.injectMembers(this);
			getGenerator().initialize(injector);//
			standaloneSetup.initialize(injector);
		}
	}

	protected Injector createInjector() {
		return Guice.createInjector(getConfiguration());
	}

	protected abstract Module getConfiguration();

	public Set<IGeneratorFragment> getGeneratorFragments() {
		return generatorFragments;
	}

	@Override
	protected void checkConfigurationInternal(Issues issues) {
		initialize();
		super.checkConfigurationInternal(issues);
	}

}
