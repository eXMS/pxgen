package org.pragmaticmodeling.pxgen.runtime;

import org.eclipse.xtext.ISetup;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

public abstract class AbstractStandaloneSetup implements ISetup {

	private Module module;

	public AbstractStandaloneSetup(Module module) {
		this.module = module;
	}
	
	@Override
	public Injector createInjectorAndDoEMFRegistration() {
		Injector injector = createInjector();
		return injector;
	}

	
	public Injector createInjector() {
		return Guice.createInjector(module);
	}
	
}
