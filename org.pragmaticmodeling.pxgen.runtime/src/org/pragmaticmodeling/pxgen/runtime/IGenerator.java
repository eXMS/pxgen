package org.pragmaticmodeling.pxgen.runtime;

import java.util.List;

import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;


public interface IGenerator extends IGeneratorFragment, IGeneratorConfig {

	List<ProjectDescriptor> getAllProjectDescriptors();

}
