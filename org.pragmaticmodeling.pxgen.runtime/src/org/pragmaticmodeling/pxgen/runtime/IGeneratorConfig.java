package org.pragmaticmodeling.pxgen.runtime;

import java.util.List;

import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

public interface IGeneratorConfig extends IGeneratorFragmentConfig {


	public List<? extends ProjectDescriptor> getEnabledProjects();
	public String getBasePackage();
	public void setBasePackage(String basePackage);
	public boolean needsMavenBuild();
	public void setNeedsMavenBuild(boolean value);
	public boolean needsGradleBuild();
	public void setNeedsGradleBuild(boolean value);
	
}
