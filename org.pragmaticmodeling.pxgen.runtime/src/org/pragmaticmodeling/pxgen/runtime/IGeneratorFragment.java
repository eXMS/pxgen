package org.pragmaticmodeling.pxgen.runtime;

import java.util.List;
import java.util.Set;

import org.eclipse.xtext.xtext.generator.IXtextGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

public interface IGeneratorFragment extends IXtextGeneratorFragment {

	Set<IGeneratorFragment> getFragments();

	void addFragment(IGeneratorFragment fragment);

	IGeneratorFragment getParentFragment();

	IGenerator getGenerator();

	List<ProjectDescriptor> getProjects();

	List<ProjectDescriptor> getAllProjects();

	void addProject(ProjectDescriptor project);
	
	void setParentFragment(IGeneratorFragment parent);

}
