package org.pragmaticmodeling.pxgen.runtime;

import java.util.List;

import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.Issues;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

public interface IGeneratorFragmentConfig extends IGuiceAwareGeneratorComponent {

	List<ProjectDescriptor> getProjects();

	void checkConfiguration(Issues issues);
	
	public String getBaseName();
	
	public void setBaseName(String baseName);

	public String getRootLocation();
	
	public void setRootLocation(String location);
	
	public IGeneratorFragmentConfig getParentConfig();
	
}
