package org.pragmaticmodeling.pxgen.runtime;

import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;

public interface IGeneratorStandaloneSetup extends IGuiceAwareGeneratorComponent {

}
