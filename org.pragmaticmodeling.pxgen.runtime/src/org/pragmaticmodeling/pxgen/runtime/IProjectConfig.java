package org.pragmaticmodeling.pxgen.runtime;

import java.util.Map;

//import org.eclipse.xtext.ui.util.ProjectFactory;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.IXtextGeneratorFileSystemAccess;

public interface IProjectConfig extends IGuiceAwareGeneratorComponent {
	
	boolean isEnabled();

	void setEnabled(boolean value);

	String getName();

	void addDirectory(String path, boolean overwrite);

	Map<String, IXtextGeneratorFileSystemAccess> getFileSystemAccesses();

	IXtextGeneratorFileSystemAccess getRoot();
	
}
