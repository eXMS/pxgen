package org.pragmaticmodeling.pxgen.runtime;

import java.io.File;
import java.util.List;
import java.util.Set;

import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

public interface IProjectCreator {

	public void setProjectDescriptors(List<ProjectDescriptor> projectDescriptors);
	Set<File> prepareProjects();
	Set<File> finishProjects();
}
