package org.pragmaticmodeling.pxgen.runtime;

import java.util.Set;

public interface IPxGeneratorsExtensionManager {

	Set<String> getGeneratorFragmentPlugins();
	
	Set<String> getGeneratorFragmentIds();

	Set<String> getGeneratorFragmentPlugins(String match);

	boolean isFragmentClass(String clazz);

	String getBundle(String clazz);

	Set<String> getFragmentsExtending(String string);

	String getParentFragmentClass(String fragmentClass);
	
}
