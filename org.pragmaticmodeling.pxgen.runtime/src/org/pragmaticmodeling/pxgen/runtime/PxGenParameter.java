package org.pragmaticmodeling.pxgen.runtime;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
public @interface PxGenParameter {
}