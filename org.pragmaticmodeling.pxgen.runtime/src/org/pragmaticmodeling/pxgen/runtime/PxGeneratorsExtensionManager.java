package org.pragmaticmodeling.pxgen.runtime;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IContributor;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.equinox.nonosgi.registry.RegistryFactoryHelper;

import com.google.inject.Singleton;

@Singleton
public class PxGeneratorsExtensionManager implements IPxGeneratorsExtensionManager {

	private static final String FRAGMENT = "fragment";
	private static final String CLASS = "class";
	private static final String EXTENDS = "extends";
	private static final String ID = "id";
	private static final String PLATFORM_ENABLER_EXTENSION = "org.pragmaticmodeling.pxgen.runtime.pxGenerators";
	private static final String MODEL = "model";
	private Map<String, FragmentInfo> fragments;

	public PxGeneratorsExtensionManager() {
		super();
		init();
	}

	private void init() {
		fragments = new HashMap<String, FragmentInfo>();
		IExtensionRegistry extensionRegistry = RegistryFactoryHelper.getRegistry();
		IConfigurationElement[] config = extensionRegistry
				.getConfigurationElementsFor(PLATFORM_ENABLER_EXTENSION);
		for (IConfigurationElement e : config) {
			IContributor contributor = e.getContributor();
			String id = e.getAttribute(ID);
			String bundleId = contributor.getName();
			String clazz = e.getAttribute(CLASS);
			String model = e.getAttribute(MODEL);
			String parentFragmentId = null;
			String elementKind = e.getName();
			if (FRAGMENT.equals(elementKind)) {
				IConfigurationElement[] extensions = e.getChildren(EXTENDS);
				if (extensions.length == 1) {
					IConfigurationElement extension = extensions[0];
					parentFragmentId = extension.getAttribute("generator");
					if (parentFragmentId == null) {
						parentFragmentId = extension.getAttribute("fragment");
					}
				}
			}
			fragments.put(id, new FragmentInfo(bundleId, clazz, parentFragmentId, model));
		}
		// resolve parentIds with effective class names
		for (FragmentInfo info : fragments.values()) {
			String parentId = info.parentFragment;
			if (parentId != null) {
				FragmentInfo parentFragment = fragments.get(parentId);
				if (parentFragment != null) {
					info.parentFragment = parentFragment.javaClass;
				}
			}
		}
	}

	@Override
	public Set<String> getGeneratorFragmentPlugins() {
		Set<String> allPlugins = new HashSet<String>();
		for (FragmentInfo f : fragments.values()) {
			allPlugins.add(f.bundle);
		}
		return allPlugins;
	}

	@Override
	public Set<String> getGeneratorFragmentPlugins(String match) {
		Set<String> result = new HashSet<String>();
		for (String bundle : getGeneratorFragmentPlugins()) {
			if (bundle.indexOf(match) > -1) {
				result.add(bundle);
			}
		}
		return result;
	}

	class FragmentInfo {
		private String bundle;
		private String javaClass;
		private String parentFragment;
		@SuppressWarnings("unused")
		private String model;

		public FragmentInfo(String bundle, String javaClass, String parentFragment, String model) {
			super();
			this.bundle = bundle;
			this.javaClass = javaClass;
			this.parentFragment = parentFragment;
			this.model = model;
		}

	}

	@Override
	public boolean isFragmentClass(String clazz) {
		for (FragmentInfo f : fragments.values()) {
			if (f.javaClass.equals(clazz))
				return true;
		}
		return false;
	}

	@Override
	public String getBundle(String clazz) {
		for (FragmentInfo f : fragments.values()) {
			if (f.javaClass.equals(clazz))
				return f.bundle;
		}
		return null;
	}

	@Override
	public Set<String> getFragmentsExtending(String clazz) {
		Set<String> result = new HashSet<String>();
		for (FragmentInfo f : fragments.values()) {
			if (f.parentFragment != null && f.parentFragment.equals(clazz)) {
				result.add(f.javaClass);
			}
		}
		return result;
	}

	@Override
	public Set<String> getGeneratorFragmentIds() {
		return fragments.keySet();
	}

	@Override
	public String getParentFragmentClass(String fragmentClass) {
		for (FragmentInfo f : fragments.values()) {
			if (f.javaClass != null && f.javaClass.equals(fragmentClass)) {
				return f.parentFragment;
			}
		}
		return null;
	}
}
