package org.pragmaticmodeling.pxgen.runtime

import com.google.inject.Inject
import com.google.inject.Injector
import org.eclipse.emf.mwe.utils.ProjectMapping
import org.eclipse.emf.mwe.utils.StandaloneSetup
import org.eclipse.xtend.lib.annotations.Accessors

/** 
 * @noextend
 */
@SuppressWarnings("all") class XtextGeneratorStandaloneSetup implements IGeneratorStandaloneSetup {
	
	@Inject
	IGenerator generatorConfig

	@Accessors boolean scanClasspath = true

	override initialize(Injector injector) {
		injector.injectMembers(this)
		setup
	}

	private def void setup() {
		val delegate = new StandaloneSetup
		delegate.scanClassPath = scanClasspath
		projectMappings.forEach [ mapping |
			delegate.addProjectMapping(new ProjectMapping => [
				projectName = mapping.key
				path = mapping.value
			])
		]
	}

	private def getProjectMappings() {
		generatorConfig.enabledProjects.filter[name !== null && root !== null].map[name -> root.path]
	}
}
