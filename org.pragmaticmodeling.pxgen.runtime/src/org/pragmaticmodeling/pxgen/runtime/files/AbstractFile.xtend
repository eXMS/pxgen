package org.pragmaticmodeling.pxgen.runtime.files

import org.eclipse.xtend.lib.annotations.Accessors
import org.pragmaticmodeling.pxgen.runtime.IContext
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor

abstract class AbstractFile<T extends IContext> implements IGeneratedFile<T> {

	@Accessors val String relativePath = ""
	@Accessors val ProjectDescriptor project = null
	@Accessors val boolean executable = false
	@Accessors val T context
	
	new (T context) {
		super();
		this.context = context
	}
	
	override getPath() {
		
	}
	
}