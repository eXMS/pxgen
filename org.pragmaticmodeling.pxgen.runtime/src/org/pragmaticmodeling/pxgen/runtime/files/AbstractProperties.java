package org.pragmaticmodeling.pxgen.runtime.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public abstract class AbstractProperties {

	private static final Logger logger = Logger.getLogger(AbstractProperties.class);

	private File file;

	private LinkedHashMap<String, List<String>> map;

	public AbstractProperties(File file) {
		super();
		this.file = file;
		init();
	}

	private void init() {
		try {
			load();
		} catch (FileNotFoundException e) {

		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	protected Map<String, List<String>> getProperties() {
		return map;
	}

	private void load() throws FileNotFoundException, IOException {
		map = new LinkedHashMap<String, List<String>>();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String attribute = null;
			for (String line; (line = br.readLine()) != null;) {
				if (line.length() > 0) {
					if (isNewAttribute(line)) {
						// a new attribute begins
						int index = line.indexOf(getAttributeSeparator());
						attribute = line.substring(0, index);
						String value = line.substring(index + getAttributeSeparator().length()).trim();
						if (value.endsWith(getLineSeparator()))
							value = value.substring(0, value.length() - getLineSeparator().length());
						addAttribute(attribute, value);
					} else {
						// attribute continued
						String value = line.trim();
						if (value.endsWith(getLineSeparator()))
							value = value.substring(0, value.length() - getLineSeparator().length());
						addAttribute(attribute, value);
					}
				}
			}
		}
	}

	protected abstract String getAttributeSeparator();

	protected abstract String getLineSeparator();

	protected abstract boolean isNewAttribute(String line);

	protected void addAttribute(String attribute, String value) {
		if (!map.containsKey(attribute)) {
			map.put(attribute, new ArrayList<String>());
		}
		map.get(attribute).add(value);
	}

	public void save() throws IOException {
		FileOutputStream fos = new FileOutputStream(file);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
		writeFile(bw);
		bw.close();
	}

	protected void writeFile(BufferedWriter bw) throws IOException {
		for (String attribute : getProperties().keySet()) {
			List<String> values = getProperties().get(attribute);
			int lastIndex = values.size() - 1;
			String shift = "";
			for (int i = 0; i < values.size(); i++) {
				String value = values.get(i);
				if (i == 0) {
					String attrDecl = attribute + " =";
					bw.write(attrDecl);
					shift = getSpaces(attrDecl.length());
				}
				if (i > 0) {
					bw.write(shift);
				}
				bw.write(" " + value);
				if (i < lastIndex) {
					bw.write(getLineSeparator());
				}
				bw.newLine();
			}
		}
	}

	private String getSpaces(int length) {
		String shift = "";
		for (int i = 0; i < length; i++)
			shift += " ";
		return shift;
	}

	public void addProperty(String name, String value) {
		if (!getProperties().containsKey(name)) {
			addAttribute(name, value);
		} else {
			if (!getProperties().get(name).contains(value))
				addAttribute(name, value);
		}
	}
	
	public void setProperty(String name, String value) {
		if (getProperties().containsKey(name)) {
			map.get(name).clear();
		}
		addAttribute(name, value);
	}

}
