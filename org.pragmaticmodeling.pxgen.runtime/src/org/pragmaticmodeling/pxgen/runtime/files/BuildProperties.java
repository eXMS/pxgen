package org.pragmaticmodeling.pxgen.runtime.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BuildProperties extends AbstractProperties {

	private static final String SOURCE = "source..";
	private static final String BIN_INCLUDES = "bin.includes";
	private static final String ADDITIONAL_BUNDLES = "additional.bundles";

	public BuildProperties(File file) throws FileNotFoundException, IOException {
		super(file);
	}

	@Override
	protected String getAttributeSeparator() {
		return " =";
	}

	@Override
	protected String getLineSeparator() {
		return ",\\";
	}

	@Override
	protected boolean isNewAttribute(String line) {
		return line.indexOf("=")>-1;
	}

	public void addSource(String folder) {
	  addProperty(SOURCE, folder);
	}

	public void addBinInclude(String resource) {
		addProperty(BIN_INCLUDES, resource);
	}

	public void addAdditionalBundle(String bundle) {
		addProperty(ADDITIONAL_BUNDLES, bundle);
	}

	public List<String> getAdditionalBundles() {
		List<String> result = getProperties().get(ADDITIONAL_BUNDLES);
		if (result == null)
			result = new ArrayList<String>();
		return result;
	}
	
}
