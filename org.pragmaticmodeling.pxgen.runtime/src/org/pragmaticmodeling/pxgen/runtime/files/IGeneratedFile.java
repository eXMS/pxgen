package org.pragmaticmodeling.pxgen.runtime.files;

import org.eclipse.xtext.xtext.generator.model.IXtextGeneratorFileSystemAccess;
import org.pragmaticmodeling.pxgen.runtime.IContext;

public interface IGeneratedFile<T extends IContext> {

	public T getContext();

	public String getRelativePath();
	
	public void setRelativePath(String path);

	public boolean isExecutable();

	public String getPath();
	public IXtextGeneratorFileSystemAccess getRoot();
}
