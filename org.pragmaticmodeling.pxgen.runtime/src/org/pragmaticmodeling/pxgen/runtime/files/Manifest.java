package org.pragmaticmodeling.pxgen.runtime.files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class Manifest extends AbstractProperties {

	private static final String REQUIRE_BUNDLE = "Require-Bundle";
	private static final String EXPORT_PACKAGE = "Export-Package";
	private static final String IMPORT_PACKAGE = "Import-Package";

	public Manifest(File file) throws FileNotFoundException, IOException {
		super(file);
	}

	public void addPluginDependency(String bundleDependency) {
		int index = bundleDependency.indexOf(";");
		String plugin = null;
		if (index == -1) {
			plugin = bundleDependency;
		} else {
			plugin = bundleDependency.substring(0, index);
		}
		if (getProperties().containsKey(REQUIRE_BUNDLE)) {
			for (String value : getProperties().get(REQUIRE_BUNDLE)) {
				if (value.startsWith(plugin) && (value.length() <= plugin.length()
						|| value.charAt(plugin.length()) == ',' || value.charAt(plugin.length()) == ';')) {
					return;
				}
			}
		}
		addAttribute(REQUIRE_BUNDLE, bundleDependency);
	}

	public void addExportedPackage(String pkgExpression) {
		int index = pkgExpression.indexOf(";");
		String pkg = null;
		if (index == -1) {
			pkg = pkgExpression;
		} else {
			pkg = pkgExpression.substring(0, index);
		}
		if (getProperties().containsKey(EXPORT_PACKAGE)) {
			for (String value : getProperties().get(EXPORT_PACKAGE)) {
				if (value.startsWith(pkg) && (value.length() <= pkg.length() || value.charAt(pkg.length()) == ','
						|| value.charAt(pkg.length()) == ';')) {
					return;
				}
			}
		}
		addAttribute(EXPORT_PACKAGE, pkgExpression);
	}

	public void addImportedPackage(String pkgExpression) {
		int index = pkgExpression.indexOf(";");
		String pkg = null;
		if (index == -1) {
			pkg = pkgExpression;
		} else {
			pkg = pkgExpression.substring(0, index);
		}
		if (getProperties().containsKey(IMPORT_PACKAGE)) {
			for (String value : getProperties().get(IMPORT_PACKAGE)) {
				if (value.startsWith(pkg) && (value.length() <= pkg.length() || value.charAt(pkg.length()) == ','
						|| value.charAt(pkg.length()) == ';')) {
					return;
				}
			}
		}
		addAttribute(IMPORT_PACKAGE, pkgExpression);
	}



	@Override
	protected boolean isNewAttribute(String line) {
		return !line.startsWith(" ");
	}

	@Override
	protected String getAttributeSeparator() {
		return ": ";
	}

	@Override
	protected String getLineSeparator() {
		return ",";
	}

	@Override
	protected void writeFile(BufferedWriter bw) throws IOException {
		for (String attribute : getProperties().keySet()) {
			List<String> values = getProperties().get(attribute);
			int lastIndex = values.size() - 1;
			for (int i = 0; i < values.size(); i++) {
				String value = values.get(i);
				if (i == 0) {
					bw.write(attribute + ":");
				}
				bw.write(" " + value);
				if (i < lastIndex) {
					bw.write(",");
				}
				bw.newLine();
			}
		}
	}

}
