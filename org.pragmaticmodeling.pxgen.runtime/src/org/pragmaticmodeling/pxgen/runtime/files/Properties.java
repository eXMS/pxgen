package org.pragmaticmodeling.pxgen.runtime.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class Properties extends AbstractProperties {

	private static final String SOURCE = "source..";
	private static final String BIN_INCLUDES = "bin.includes";
	private static final String ADDITIONAL_BUNDLES = "additional.bundles";

	public Properties(File file) throws FileNotFoundException, IOException {
		super(file);
	}

	@Override
	protected String getAttributeSeparator() {
		return " =";
	}

	@Override
	protected String getLineSeparator() {
		return ",\\";
	}

	@Override
	protected boolean isNewAttribute(String line) {
		return line.indexOf("=")>-1;
	}

	public void addSource(String folder) {
	  if (!getProperties().containsKey(SOURCE)) {
		  addAttribute(SOURCE, folder);
	  } else {
		  if (!getProperties().get(SOURCE).contains(folder))
			  addAttribute(SOURCE, folder);
	  }
	}

	public void addBinInclude(String resource) {
		if (!getProperties().containsKey(BIN_INCLUDES)) {
			addAttribute(BIN_INCLUDES, resource);
		} else {
			  if (!getProperties().get(BIN_INCLUDES).contains(resource))
			  addAttribute(BIN_INCLUDES, resource);
		  }
	}

	public void addAdditionalBundle(String bundle) {
		if (!getProperties().containsKey(ADDITIONAL_BUNDLES)) {
			addAttribute(ADDITIONAL_BUNDLES, bundle);
		} else {
			  if (!getProperties().get(ADDITIONAL_BUNDLES).contains(bundle))
			  addAttribute(ADDITIONAL_BUNDLES, bundle);
		  }
	}

	public List<String> get(String name) {
	  return getProperties().get(name);
	}
	
}
