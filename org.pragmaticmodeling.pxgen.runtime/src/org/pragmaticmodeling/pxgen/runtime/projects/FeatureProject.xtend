package org.pragmaticmodeling.pxgen.runtime.projects

import java.util.ArrayList
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

class FeatureProject extends PlainProject {

	@Accessors
	var List<String> bundles = new ArrayList<String>
	
	@Accessors
	var List<String> includedFeatures = new ArrayList<String>
	
	@Accessors
	var String featureLabel 
	
	@Accessors
	var boolean withSources 
	
}
