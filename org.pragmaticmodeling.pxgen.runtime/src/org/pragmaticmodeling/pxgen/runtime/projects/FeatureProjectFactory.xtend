package org.pragmaticmodeling.pxgen.runtime.projects

import java.io.File
import org.pragmaticmodeling.pxgen.runtime.files.BuildProperties

class FeatureProjectFactory extends ProjectFactory {
	
	static String FEATURE_FILENAME = "feature.xml";
	static String BUILDPROPS_FILENAME = "build.properties";
	
	@Override
	override protected void enhanceProject(ProjectDescriptor project)  {
		super.enhanceProject(project);
		
	}
	
	override protected createProjectFiles(ProjectDescriptor project) {
		super.createProjectFiles(project)
		var featureProject = project as FeatureProject
		createFeature(featureProject);
		createBuildProperties(featureProject);
	}


	def private void createBuildProperties(FeatureProject project) {
		val File projectFile = new File(project.getLocation())
		val File file = new File(projectFile, BUILDPROPS_FILENAME)
		val buildProperties = new BuildProperties(file);
		buildProperties.addBinInclude(FEATURE_FILENAME);
		buildProperties.save
	}

	def private void createFeature(FeatureProject project) {
		var projectName = project.name
	
		var content = '''
			<?xml version="1.0" encoding="UTF-8"?>
			<feature id="«projectName»"
				label="«if(!project.featureLabel.nullOrEmpty) project.featureLabel else projectName + " Feature"»"
				version="1.0.0.qualifier">
				«FOR includedFeature: project.includedFeatures»
				<includes
					id="«includedFeature»"
					version="0.0.0"/>
					    	«ENDFOR»
				«FOR containedBundle: project.bundles»
				<plugin
						id="«containedBundle»"
						download-size="0"
						install-size="0"
						version="0.0.0"
						unpack="false"/>
				«ENDFOR»
			</feature>
		'''
		project.root.generateFile(FEATURE_FILENAME, content.toString())
	}
	
}
