/*******************************************************************************
 * Copyright (c) 2012 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.pragmaticmodeling.pxgen.runtime.projects;

import java.io.File;

/**
 * @author dhuebner - Initial contribution and API
 * @since 2.3
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface IProjectFactoryContributor {
	void contributeFiles(File project, IFileCreator fileWriter);

	public interface IFileCreator {
		File writeToFile(CharSequence chars, String fileName);
	}
}
