package org.pragmaticmodeling.pxgen.runtime.projects;

public enum JavaBree {
	JavaSE16, JavaSE17, JavaSE18;
	
	@Override
	public String toString() {
		switch (this) {
		case JavaSE16:
			return "JavaSE-1.6";
		case JavaSE17:
			return "JavaSE-1.7";
		case JavaSE18:
			return "JavaSE-1.8";
		}
		return super.toString();
	}
}
