package org.pragmaticmodeling.pxgen.runtime.projects

import java.util.ArrayList
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

class JavaProject extends PlainProject {
	@Accessors
	var List<String> classpathEntries = new ArrayList<String>
	
	@Accessors
	var List<String> containerClasspathEntries = new ArrayList<String>
	
	@Accessors
	var String jreContainerEntry
	
	@Accessors
	var String defaultOutput = "bin";
	
	@Accessors
	var JavaBree breeToUse
	
	@Accessors
	var List<String> folders = new ArrayList<String>
	
}
