/** 
 * Copyright (c) 2010 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.pragmaticmodeling.pxgen.runtime.projects

import java.io.File
import org.eclipse.core.runtime.CoreException
import org.eclipse.jdt.core.IClasspathEntry
import org.eclipse.jdt.core.JavaCore

/** 
 * @author Sebastian Zarnekow - Initial contribution and API
 */
class JavaProjectFactory extends ProjectFactory {
	/** 
	 * The plug-in identifier of the Java core support (value
	 * <code>"org.eclipse.jdt.core"</code>).
	 */
	public static final String JDT = "org.eclipse.jdt.core"

	/** 
	 * The identifier for the Java builder (value
	 * <code>"org.eclipse.jdt.core.javabuilder"</code>).
	 */
	public static final String JDT_BUILDER_ID = JDT + ".javabuilder"

	override protected void enhanceProject(ProjectDescriptor project) {
		super.enhanceProject(project)
		var JavaProject javaProject = (project as JavaProject)
		javaProject.getProjectNatures().add(JavaCore.NATURE_ID)
		javaProject.getBuilderIds().add(JavaCore.BUILDER_ID)
		if (javaProject.getConfig().needsMavenBuild()) {
			javaProject.setDefaultOutput("target/classes")
			if (!isPluginProject(project)) {
				javaProject.containerClasspathEntries.add("org.eclipse.m2e.MAVEN2_CLASSPATH_CONTAINER")
			}
		}
		if (javaProject.getConfig().needsGradleBuild() && !isPluginProject(project)) {
			javaProject.containerClasspathEntries.add("org.eclipse.buildship.core.gradleclasspathcontainer")
		}
		if (javaProject.getBuilderIds().contains(JDT_BUILDER_ID)) {
			for (ProjectDescriptor referencedProject : javaProject.getReferencedProjects()) {
				javaProject.getClasspathEntries().add(referencedProject.getLocation()) // FIXME
			}
			var File projectPath = new File(javaProject.getLocation())
			for (String folderName : javaProject.getFolders()) {
				var File sourceFolderPath = new File(projectPath, folderName)
				if (!sourceFolderPath.exists)
					createRecursive(sourceFolderPath)
			}
		}
	}

	override protected void createProjectFiles(ProjectDescriptor project) {
		super.createProjectFiles(project)
		val javaProject = project as JavaProject
		// create .project file
		project.getRoot().generateFile(".classpath", 
		'''
		<?xml version="1.0" encoding="UTF-8"?>
		<classpath>
		«FOR sourceFolder : javaProject.folders»
			«sourceFolder.toSourceFolderEntry»
		«ENDFOR»
			<classpathentry kind="con" path="org.eclipse.jdt.launching.JRE_CONTAINER/org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/«javaProject.getJavaBree.toString»"/>
			<classpathentry kind="output" path="«javaProject.defaultOutput»"/>«addMoreClasspathEntriesTo(javaProject)»
		</classpath>
			
		''')
	}

	def String toSourceFolderEntry(String sourceFolder) {
		'''	<classpathentry kind="src" path="«sourceFolder»"/>'''
	}

	def String kind(IClasspathEntry entry) {
		switch entry.entryKind {
		}
	}

	def String getJavaBree(JavaProject project) {
		if (project.breeToUse === null) {
			return JavaBree.JavaSE18.toString
		} else {
			return project.breeToUse.toString
		}
	}

	def private boolean isPluginProject(ProjectDescriptor project) {
		return project instanceof PluginProject
	}

	def protected String addMoreClasspathEntriesTo(JavaProject javaProject) {
	}

	def protected void createFolders(JavaProject project) throws CoreException {
		if (project.getFolders() !== null) {
			for (String folderName : project.getFolders()) {
				val File folder = new File(project.getLocation(), folderName)
				if (!folder.exists()) {
					createRecursive(folder)
				}
			}
		}
	}
}
