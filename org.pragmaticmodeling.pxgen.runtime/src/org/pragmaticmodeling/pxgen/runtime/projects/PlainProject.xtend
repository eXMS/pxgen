package org.pragmaticmodeling.pxgen.runtime.projects

import org.eclipse.xtend.lib.annotations.Accessors

class PlainProject extends ProjectDescriptor {

	var String projectModelName

	@Accessors
	var boolean partOfMavenBuild

	@Accessors
	var boolean partOfGradleBuild

	/**
	 * @since 2.4
	 */
	protected String defaultCharset;

	override getSourceFolders() {
		emptySet
	}
	
	def void setProjectModelName(String name) {
		this.projectModelName = name
	}

}
