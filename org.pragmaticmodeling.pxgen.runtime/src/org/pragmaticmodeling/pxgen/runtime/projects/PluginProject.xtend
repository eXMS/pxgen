package org.pragmaticmodeling.pxgen.runtime.projects

import com.google.inject.Injector
import java.io.File
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtext.xtext.generator.model.IXtextGeneratorFileSystemAccess

class PluginProject extends JavaProject {
	@Accessors
	var List<String> requiredBundles = new ArrayList<String>
	@Accessors
	var List<String> exportedPackages = new ArrayList<String>
	@Accessors
	var List<String> importedPackages = new ArrayList<String>
	@Accessors
	var String activatorClassName
	@Accessors
	var List<String> developmentTimeBundles = new ArrayList<String>
	@Accessors
	var List<String> buildBinIncludes = new ArrayList<String>
	
	@Accessors
	var Map<String, String> bundleProperties = new HashMap<String, String>()
	
	@Accessors
	var boolean withPluginXml = true;
	
	IXtextGeneratorFileSystemAccess metaInf

	def IXtextGeneratorFileSystemAccess getMetaInf() {
		if (this.metaInf === null) {
			var rootPath = new File(config.rootLocation, name)
			this.metaInf = newFileSystemAccess(rootPath.absolutePath + "/META-INF", false)
		}
		return this.metaInf
	}
	
	override initialize(Injector injector) {
		super.initialize(injector)
		getMetaInf.initialize(injector)
	}
	
}
