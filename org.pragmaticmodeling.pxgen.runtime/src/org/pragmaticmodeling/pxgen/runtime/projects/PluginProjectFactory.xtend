/** 
 * Copyright (c) 2010 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.pragmaticmodeling.pxgen.runtime.projects

import com.google.common.base.Strings
import com.google.common.collect.Iterables
import java.io.File
import java.util.Iterator
import java.util.List
import org.pragmaticmodeling.pxgen.runtime.files.BuildProperties
import org.pragmaticmodeling.pxgen.runtime.files.Manifest
import org.pragmaticmodeling.pxgen.runtime.files.Properties

/** 
 * @author Sebastian Zarnekow - Initial contribution and API
 */
class PluginProjectFactory extends JavaProjectFactory {

	override protected void enhanceProject(ProjectDescriptor descriptor) {
		super.enhanceProject(descriptor)
		var PluginProject project = (descriptor as PluginProject)
		project.getProjectNatures().add("org.eclipse.pde.PluginNature")
		project.getBuilderIds().add("org.eclipse.pde.ManifestBuilder")
		project.getBuilderIds().add("org.eclipse.pde.SchemaBuilder")

	}

	override protected createProjectFiles(ProjectDescriptor descriptor) {
		super.createProjectFiles(descriptor)
		var PluginProject project = (descriptor as PluginProject)
		if (project.getProjectNatures() !== null &&
			project.getProjectNatures().contains("org.eclipse.pde.PluginNature")) {
			createManifest(project)
			createBuildProperties(project)
			createPluginProperties(project)
		}
	}

	override protected addMoreClasspathEntriesTo(JavaProject javaProject) {
		'''	
		
		<classpathentry kind="con" path="org.eclipse.pde.core.requiredPlugins"/>'''
	}

	def protected void createBuildProperties(PluginProject project) {
		val File projectFile = new File(project.getLocation())
		val File file = new File(projectFile, "build.properties")
		val buildProperties = new BuildProperties(file);
		for (folder : project.getFolders()) {
			buildProperties.addSource(withEndingSlash(folder));
		}
		for (binInclude : project.buildBinIncludes) {
			var File fileToInclude = new File(project.getLocation(), binInclude)
			if (fileToInclude.isDirectory) {
				buildProperties.addBinInclude(withEndingSlash(binInclude));
			} else {
				buildProperties.addBinInclude(binInclude);
			}
		}
		buildProperties.addBinInclude("META-INF/");
		buildProperties.addBinInclude(".");
		if (!project.bundleProperties.empty) {
			buildProperties.addBinInclude("plugin.properties");
		}
		if (project.isWithPluginXml()) {
			buildProperties.addBinInclude("plugin.xml");
		}
		for (bundle : project.getDevelopmentTimeBundles()) {
			buildProperties.addAdditionalBundle(bundle);
		}
		buildProperties.save
	}
	
	def protected void createPluginProperties(PluginProject project) {
		var propsMap = project.bundleProperties
		if (propsMap.empty)
			return
		val File projectFile = new File(project.getLocation())
		val File file = new File(projectFile, "plugin.properties")
		val pluginProperties = new Properties(file);
		for (name : propsMap.keySet) {
			var props = propsMap.get(name)
			if (props !== null && !props.empty) {
				pluginProperties.setProperty(name, project.bundleProperties.get(name));
			}
		}
		pluginProperties.save
	}
	
	def withEndingSlash(String string) {
		var value = string.trim
		if (!value.endsWith("/"))
		value += "/"
		return value
	}

	def protected void createManifest(PluginProject project) {
		val File projectFile = new File(project.getLocation())
		val File metaInf = new File(projectFile, "META-INF")
		if (!metaInf.exists())
			metaInf.mkdir()
		var manifestFile = new File(metaInf, "MANIFEST.MF");
		if (manifestFile.exists) {
			var manifest = new Manifest(manifestFile)
			for (b : project.requiredBundles) {
				manifest.addPluginDependency(b)
			}
			for (p : project.exportedPackages) {
				manifest.addExportedPackage(p)
			}
			for (p : project.importedPackages) {
				manifest.addImportedPackage(p)
			}
			if (!project.bundleProperties.empty) {
				manifest.addProperty("Bundle-Localization", "plugin")
			}
			manifest.save();
		} else {
			val StringBuilder content = new StringBuilder("Manifest-Version: 1.0\n")
			content.append("Bundle-ManifestVersion: 2\n")
			content.append('''Bundle-Name: «project.getName()»
''')
			content.append("Bundle-Vendor: My Company\n")
			content.append("Bundle-Version: 1.0.0.qualifier\n")
			content.append('''Bundle-SymbolicName: «project.getName()»; singleton:=true
''')
			if (null !== project.getActivatorClassName()) {
				content.append('''Bundle-Activator: «project.getActivatorClassName()»
''')
			}
			if (!project.bundleProperties.empty) {
				content.append("Bundle-Localization: plugin\n")
			}
			content.append("Bundle-ActivationPolicy: lazy\n")
			addToContent(content, project.getRequiredBundles(), "Require-Bundle")
			addToContent(content, project.getExportedPackages(), "Export-Package")
			addToContent(content, project.getImportedPackages(), "Import-Package")
			content.append("Bundle-RequiredExecutionEnvironment: ")
			content.append('''«getJavaBree(project)»
''')
			project.getMetaInf().generateFile("MANIFEST.MF", content.toString())
		}
	}

	def protected void addToContent(StringBuilder content, List<String> entries, String prefix) {
		if (entries !== null && !entries.isEmpty()) {
			content.append(prefix).append(": ").append(entries.get(0))
			for (var int i = 1, var int x = entries.size(); i < x; i++) {
				content.append(",\n ").append(entries.get(i))
			}
			content.append("\n")
		}
	}

	/** 
	 * @since 2.8
	 */
	def protected void addToBuildProperties(StringBuilder content, Iterable<String> entries, String entryName) {
		if (entries !== null && !Iterables.isEmpty(entries)) {
			var String assigment = '''«entryName» = '''
			var String indent = Strings.repeat(" ", assigment.length())
			content.append(assigment)
			for (val Iterator<String> iterator = entries.iterator(); iterator.hasNext();) {
				content.append(iterator.next())
				if (iterator.hasNext()) {
					content.append(",\\\n")
					content.append(indent)
				}
			}
		}
	}

}
