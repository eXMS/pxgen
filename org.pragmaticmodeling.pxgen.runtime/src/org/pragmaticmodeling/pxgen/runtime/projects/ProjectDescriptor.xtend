/*******************************************************************************
 * Copyright (c) 2015 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.pragmaticmodeling.pxgen.runtime.projects

import com.google.inject.Inject
import com.google.inject.Injector
import java.io.File
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Set
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent
import org.eclipse.xtext.xtext.generator.model.IXtextGeneratorFileSystemAccess
import org.eclipse.xtext.xtext.generator.model.XtextGeneratorFileSystemAccess
import org.pragmaticmodeling.pxgen.runtime.IGenerator
import org.pragmaticmodeling.pxgen.runtime.IGeneratorConfig
import org.pragmaticmodeling.pxgen.runtime.IGeneratorFragment

abstract class ProjectDescriptor implements IGuiceAwareGeneratorComponent {

	var IGeneratorConfig config

	@Accessors
	@Inject
	var IGenerator generator

	@Accessors
	boolean enabled

	@Accessors
	Map<String, IXtextGeneratorFileSystemAccess> fsaMap = new HashMap<String, IXtextGeneratorFileSystemAccess>()

	@Accessors
	var List<ProjectDescriptor> referencedProjects = new ArrayList<ProjectDescriptor>
	@Accessors
	var List<String> projectNatures = new ArrayList<String>
	@Accessors
	var List<String> builderIds = new ArrayList<String>
	IXtextGeneratorFileSystemAccess root

	var String name
	var String nameQualifier

	IGeneratorFragment parentFragment

	def IGeneratorFragment getParentFragment() {
		parentFragment
	}

	def void setParentFragment(IGeneratorFragment parent) {
		this.parentFragment = parent
		this.generator = getParentFragment.generator
		this.config = generator as IGeneratorConfig
	}

	def String getName() {
		if (name === null) {
			return getConfig().basePackage + getNameQualifier
		} else {
			return name
		}
	}

	def void setName(String newName) {
		this.name = newName
	}

	def IGeneratorConfig getConfig() {
		config
	}

	def String getNameQualifier() {
		if (nameQualifier === null)
			""
		else
			nameQualifier
	}
	
	def void setNameQualifier(String value) {
		nameQualifier = value
	}

	def IXtextGeneratorFileSystemAccess getRoot() {
		if (this.root === null) {
			var rootPath = new File(config.rootLocation, getName())
			this.root = newFileSystemAccess(rootPath.absolutePath, true)
		}
		return this.root
	}

	def Map<String, IXtextGeneratorFileSystemAccess> getFileSystemAccesses() {
		return fsaMap
	}

	def String getLocation() {
		return getRoot().path
	}

	def Set<? extends ProjectDescriptor> getUpstreamProjects() {
		emptySet
	}

	def Set<String> getSourceFolders()

	override void initialize(Injector injector) {
		injector.injectMembers(this)
		for (IXtextGeneratorFileSystemAccess fsa : getFileSystemAccesses().values()) {
			fsa.initialize(injector)
		} 
	}

	public def void addGenerationDirectory(String dir, boolean overwrite) {
		var File projectLocation = new File(config.rootLocation, getName)
		var directory = ""
		if (".".equals(dir)) {
			directory = projectLocation.absolutePath
		} else {
			val file = new File(projectLocation, dir)
			if (!file.exists) {
				file.mkdirs
			}
			directory = file.absolutePath
		}
		var fsa = newFileSystemAccess(directory, overwrite);
		fileSystemAccesses.put(dir, fsa)
	}

	def protected IXtextGeneratorFileSystemAccess newFileSystemAccess(String path, boolean overWrite) {
		return new XtextGeneratorFileSystemAccess(path, overWrite)
	}

}
