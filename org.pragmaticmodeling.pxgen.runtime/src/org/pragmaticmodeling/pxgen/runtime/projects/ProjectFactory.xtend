/** 
 * Copyright (c) 2010 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.pragmaticmodeling.pxgen.runtime.projects

import java.io.File
import java.util.ArrayList
import java.util.List
import org.eclipse.core.resources.ICommand
import org.eclipse.core.resources.IProjectDescription

/** 
 * @author Holger Schill - Initial contribution and API
 * @author Sebastian Zarnekow
 * 
 * 
 */
class ProjectFactory {
//	static final Logger logger = Logger.getLogger(ProjectFactory)

	/** 
	 * @since 2.4
	 */
	protected String defaultCharset

	/** 
	 * @since 2.4
	 */
	def void setProjectDefaultCharset(String encoding) {
		this.defaultCharset = encoding
	}

	def File prepareProject(ProjectDescriptor description) {
		var File project = getProject(description)
		project = prepare(description)
		enhanceProject(description)
		return project
	}

	def File finishProject(ProjectDescriptor description) {
		var File project = getProject(description)
		createProjectFiles(description)
		return project
	}

	def protected void createProjectFiles(ProjectDescriptor project) {
		// create .project file
		project.getRoot().generateFile(".project", '''
			<?xml version="1.0" encoding="UTF-8"?>
			<projectDescription>
				<name>«project.name»</name>
				<comment></comment>
				<projects>
				</projects>
				<buildSpec>
					«FOR builder : project.builderIds»
						<buildCommand>
							<name>«builder»</name>
							<arguments>
							</arguments>
						</buildCommand>
					«ENDFOR»
				</buildSpec>
				<natures>
					«FOR nature : project.projectNatures»
						<nature>«nature»</nature>
					«ENDFOR»
				</natures>
			</projectDescription>
		''')
	}

	def protected File prepare(ProjectDescriptor description) {
		var File project = getProject(description)
		if (!project.exists()) {
			createRecursive(project)
		}
		return project
	}

	def private File getProject(ProjectDescriptor description) {
		var String loc = description.getLocation()
		var File project = new File(loc)
		return project
	}

	def protected void enhanceProject(ProjectDescriptor project) {
		if (project.getConfig().needsMavenBuild()) {
			project.getProjectNatures().add("org.eclipse.m2e.core.maven2Nature")
			project.getBuilderIds().add("org.eclipse.m2e.core.maven2Builder")
		}
		if (project.getConfig().needsGradleBuild()) {
			project.getProjectNatures().add("org.eclipse.buildship.core.gradleprojectnature")
			project.getBuilderIds().add("org.eclipse.buildship.core.gradleprojectbuilder") 
		}

	}

	def protected void setBuilder(IProjectDescription projectDescription, String[] builders) {
		var List<ICommand> commands = new ArrayList<ICommand>()
		for (var int i = 0; i < builders.length; i++) {
			var ICommand command = projectDescription.newCommand()
			command.setBuilderName({
				val _rdIndx_builders = i
				builders.get(_rdIndx_builders)
			})
			commands.add(command)
		}
		projectDescription.setBuildSpec(commands.toArray(newArrayOfSize(commands.size())))
	}

	def protected void createRecursive(File resource) {
		if (!resource.exists()) {
			if (!resource.getParentFile().exists()) {
				createRecursive(resource.getParentFile())
			}
			if (resource.isDirectory()) {
				resource.mkdir()
			}
		}
	}
}
