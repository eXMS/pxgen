package org.pragmaticmodeling.pxgen.runtime.projects;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.pragmaticmodeling.pxgen.runtime.IProjectCreator;

import com.google.inject.Inject;
import com.google.inject.Provider;

public class PxGenProjectCreator implements IProjectCreator {
	
	@Inject
	private Provider<PluginProjectFactory> pluginProjectProvider;
	@Inject
	private Provider<JavaProjectFactory> javaProjectProvider;
	@Inject
	private Provider<ProjectFactory> plainProjectProvider;
	
	@Inject
	private Provider<FeatureProjectFactory> featureProjectProvider;

	private List<ProjectDescriptor> projectDescriptors;
	Map<ProjectDescriptor, File> createdProjects = new HashMap<ProjectDescriptor, File>();

	public Set<File> prepareProjects()  {
		for (ProjectDescriptor descriptor : projectDescriptors) {
			File project = prepareProject(descriptor);
			createdProjects.put(descriptor, project);
		}
		return new HashSet<File>(createdProjects.values());
	}

	private File prepareProject(ProjectDescriptor descriptor) {
		if (isPluginProject(descriptor)) {
			return preparePluginProject((PluginProject)descriptor);
		} else if (isFeatureProject(descriptor)) {
			return prepareFeatureProject((FeatureProject)descriptor);
		} else if (isJavaProject(descriptor)) {
			return prepareJavaProject((JavaProject)descriptor);
		} else {
			return preparePlainProject((PlainProject)descriptor);
		}
	}
	
	private File finishProject(ProjectDescriptor descriptor) {
		if (isPluginProject(descriptor)) {
			return finishPluginProject((PluginProject)descriptor);
		} else if (isFeatureProject(descriptor)) {
			return finishFeatureProject((FeatureProject)descriptor);
		} else if (isJavaProject(descriptor)) {
			return finishJavaProject((JavaProject)descriptor);
		} else {
			return finishPlainProject((PlainProject)descriptor);
		}
	}

	private File preparePluginProject(PluginProject descriptor) {
		PluginProjectFactory factory = pluginProjectProvider.get();
		return factory.prepareProject(descriptor);
	}
	
	private File finishPluginProject(PluginProject descriptor) {
		PluginProjectFactory factory = pluginProjectProvider.get();
		return factory.finishProject(descriptor);
	}

	private File prepareJavaProject(JavaProject descriptor) {
		JavaProjectFactory factory = javaProjectProvider.get();
		return factory.prepareProject(descriptor);
	}
	
	private File finishJavaProject(JavaProject descriptor) {
		JavaProjectFactory factory = javaProjectProvider.get();
		return factory.finishProject(descriptor);
	}

	private File prepareFeatureProject(FeatureProject descriptor) {
		FeatureProjectFactory factory = featureProjectProvider.get();
		descriptor.getProjectNatures().add("org.eclipse.pde.FeatureNature");
		descriptor.getBuilderIds().add("org.eclipse.pde.FeatureBuilder");
		return factory.prepareProject(descriptor);
	}
	
	private File finishFeatureProject(FeatureProject descriptor) {
		FeatureProjectFactory factory = featureProjectProvider.get();
		return factory.finishProject(descriptor);
	}

	private File preparePlainProject(PlainProject descriptor) {
		ProjectFactory factory = plainProjectProvider.get();
		return factory.prepareProject(descriptor);
	}
	
	private File finishPlainProject(PlainProject descriptor) {
		ProjectFactory factory = plainProjectProvider.get();
		return factory.finishProject(descriptor);
	}

	private boolean isPluginProject(ProjectDescriptor descriptor) {
		return descriptor instanceof PluginProject;
	}

	private boolean isFeatureProject(ProjectDescriptor descriptor) {
		return descriptor instanceof FeatureProject;
	}

	private boolean isJavaProject(ProjectDescriptor descriptor) {
		return descriptor instanceof JavaProject;
	}

	public void setProjectDescriptors(List<ProjectDescriptor> projectDescriptors) {
		this.projectDescriptors = projectDescriptors;
	}

	@Override
	public Set<File> finishProjects() {
		for (ProjectDescriptor descriptor : projectDescriptors) {
			File project = finishProject(descriptor);
			createdProjects.put(descriptor, project);
		}
		
		return new HashSet<File>(createdProjects.values());
	}


}