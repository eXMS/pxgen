package org.pragmaticmodeling.pxgen.runtime;

import com.google.inject.Inject;
import com.google.inject.Injector;
import java.util.function.Consumer;
import org.eclipse.emf.mwe.utils.ProjectMapping;
import org.eclipse.emf.mwe.utils.StandaloneSetup;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Pair;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.eclipse.xtext.xbase.lib.Pure;
import org.pragmaticmodeling.pxgen.runtime.IGenerator;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorStandaloneSetup;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

/**
 * @noextend
 */
@SuppressWarnings("all")
public class XtextGeneratorStandaloneSetup implements IGeneratorStandaloneSetup {
  @Inject
  private IGenerator generatorConfig;
  
  @Accessors
  private boolean scanClasspath = true;
  
  @Override
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
    this.setup();
  }
  
  private void setup() {
    final StandaloneSetup delegate = new StandaloneSetup();
    delegate.setScanClassPath(this.scanClasspath);
    final Consumer<Pair<String, String>> _function = (Pair<String, String> mapping) -> {
      ProjectMapping _projectMapping = new ProjectMapping();
      final Procedure1<ProjectMapping> _function_1 = (ProjectMapping it) -> {
        it.setProjectName(mapping.getKey());
        it.setPath(mapping.getValue());
      };
      ProjectMapping _doubleArrow = ObjectExtensions.<ProjectMapping>operator_doubleArrow(_projectMapping, _function_1);
      delegate.addProjectMapping(_doubleArrow);
    };
    this.getProjectMappings().forEach(_function);
  }
  
  private Iterable<Pair<String, String>> getProjectMappings() {
    final Function1<ProjectDescriptor, Boolean> _function = (ProjectDescriptor it) -> {
      return Boolean.valueOf(((it.getName() != null) && (it.getRoot() != null)));
    };
    final Function1<ProjectDescriptor, Pair<String, String>> _function_1 = (ProjectDescriptor it) -> {
      String _name = it.getName();
      String _path = it.getRoot().getPath();
      return Pair.<String, String>of(_name, _path);
    };
    return IterableExtensions.map(IterableExtensions.filter(this.generatorConfig.getEnabledProjects(), _function), _function_1);
  }
  
  @Pure
  public boolean isScanClasspath() {
    return this.scanClasspath;
  }
  
  public void setScanClasspath(final boolean scanClasspath) {
    this.scanClasspath = scanClasspath;
  }
}
