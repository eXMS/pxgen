package org.pragmaticmodeling.pxgen.runtime.files;

import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.pragmaticmodeling.pxgen.runtime.IContext;
import org.pragmaticmodeling.pxgen.runtime.files.IGeneratedFile;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

@SuppressWarnings("all")
public abstract class AbstractFile<T extends IContext> implements IGeneratedFile<T> {
  @Accessors
  private final String relativePath = "";
  
  @Accessors
  private final ProjectDescriptor project = null;
  
  @Accessors
  private final boolean executable = false;
  
  @Accessors
  private final T context;
  
  public AbstractFile(final T context) {
    super();
    this.context = context;
  }
  
  @Override
  public String getPath() {
    return null;
  }
  
  @Pure
  public String getRelativePath() {
    return this.relativePath;
  }
  
  @Pure
  public ProjectDescriptor getProject() {
    return this.project;
  }
  
  @Pure
  public boolean isExecutable() {
    return this.executable;
  }
  
  @Pure
  public T getContext() {
    return this.context;
  }
}
