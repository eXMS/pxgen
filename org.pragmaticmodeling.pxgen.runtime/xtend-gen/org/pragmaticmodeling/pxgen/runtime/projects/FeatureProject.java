package org.pragmaticmodeling.pxgen.runtime.projects;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.pragmaticmodeling.pxgen.runtime.projects.PlainProject;

@SuppressWarnings("all")
public class FeatureProject extends PlainProject {
  @Accessors
  private List<String> bundles = new ArrayList<String>();
  
  @Accessors
  private List<String> includedFeatures = new ArrayList<String>();
  
  @Accessors
  private String featureLabel;
  
  @Accessors
  private boolean withSources;
  
  @Pure
  public List<String> getBundles() {
    return this.bundles;
  }
  
  public void setBundles(final List<String> bundles) {
    this.bundles = bundles;
  }
  
  @Pure
  public List<String> getIncludedFeatures() {
    return this.includedFeatures;
  }
  
  public void setIncludedFeatures(final List<String> includedFeatures) {
    this.includedFeatures = includedFeatures;
  }
  
  @Pure
  public String getFeatureLabel() {
    return this.featureLabel;
  }
  
  public void setFeatureLabel(final String featureLabel) {
    this.featureLabel = featureLabel;
  }
  
  @Pure
  public boolean isWithSources() {
    return this.withSources;
  }
  
  public void setWithSources(final boolean withSources) {
    this.withSources = withSources;
  }
}
