package org.pragmaticmodeling.pxgen.runtime.projects;

import java.io.File;
import java.util.List;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.pragmaticmodeling.pxgen.runtime.files.BuildProperties;
import org.pragmaticmodeling.pxgen.runtime.projects.FeatureProject;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectFactory;

@SuppressWarnings("all")
public class FeatureProjectFactory extends ProjectFactory {
  private static String FEATURE_FILENAME = "feature.xml";
  
  private static String BUILDPROPS_FILENAME = "build.properties";
  
  @Override
  protected void enhanceProject(final ProjectDescriptor project) {
    super.enhanceProject(project);
  }
  
  @Override
  protected void createProjectFiles(final ProjectDescriptor project) {
    super.createProjectFiles(project);
    FeatureProject featureProject = ((FeatureProject) project);
    this.createFeature(featureProject);
    this.createBuildProperties(featureProject);
  }
  
  private void createBuildProperties(final FeatureProject project) {
    try {
      String _location = project.getLocation();
      final File projectFile = new File(_location);
      final File file = new File(projectFile, FeatureProjectFactory.BUILDPROPS_FILENAME);
      final BuildProperties buildProperties = new BuildProperties(file);
      buildProperties.addBinInclude(FeatureProjectFactory.FEATURE_FILENAME);
      buildProperties.save();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  private void createFeature(final FeatureProject project) {
    String projectName = project.getName();
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<feature id=\"");
    _builder.append(projectName);
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("label=\"");
    String _xifexpression = null;
    boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(project.getFeatureLabel());
    boolean _not = (!_isNullOrEmpty);
    if (_not) {
      _xifexpression = project.getFeatureLabel();
    } else {
      _xifexpression = (projectName + " Feature");
    }
    _builder.append(_xifexpression, "\t");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("version=\"1.0.0.qualifier\">");
    _builder.newLine();
    {
      List<String> _includedFeatures = project.getIncludedFeatures();
      for(final String includedFeature : _includedFeatures) {
        _builder.append("\t");
        _builder.append("<includes");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("id=\"");
        _builder.append(includedFeature, "\t\t");
        _builder.append("\"");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("version=\"0.0.0\"/>");
        _builder.newLine();
      }
    }
    {
      List<String> _bundles = project.getBundles();
      for(final String containedBundle : _bundles) {
        _builder.append("\t");
        _builder.append("<plugin");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("\t\t");
        _builder.append("id=\"");
        _builder.append(containedBundle, "\t\t\t");
        _builder.append("\"");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t\t");
        _builder.append("download-size=\"0\"");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("\t\t");
        _builder.append("install-size=\"0\"");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("\t\t");
        _builder.append("version=\"0.0.0\"");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("\t\t");
        _builder.append("unpack=\"false\"/>");
        _builder.newLine();
      }
    }
    _builder.append("</feature>");
    _builder.newLine();
    String content = _builder.toString();
    project.getRoot().generateFile(FeatureProjectFactory.FEATURE_FILENAME, content.toString());
  }
}
