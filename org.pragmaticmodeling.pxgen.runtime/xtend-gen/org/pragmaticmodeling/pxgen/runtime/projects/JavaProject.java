package org.pragmaticmodeling.pxgen.runtime.projects;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.pragmaticmodeling.pxgen.runtime.projects.JavaBree;
import org.pragmaticmodeling.pxgen.runtime.projects.PlainProject;

@SuppressWarnings("all")
public class JavaProject extends PlainProject {
  @Accessors
  private List<String> classpathEntries = new ArrayList<String>();
  
  @Accessors
  private List<String> containerClasspathEntries = new ArrayList<String>();
  
  @Accessors
  private String jreContainerEntry;
  
  @Accessors
  private String defaultOutput = "bin";
  
  @Accessors
  private JavaBree breeToUse;
  
  @Accessors
  private List<String> folders = new ArrayList<String>();
  
  @Pure
  public List<String> getClasspathEntries() {
    return this.classpathEntries;
  }
  
  public void setClasspathEntries(final List<String> classpathEntries) {
    this.classpathEntries = classpathEntries;
  }
  
  @Pure
  public List<String> getContainerClasspathEntries() {
    return this.containerClasspathEntries;
  }
  
  public void setContainerClasspathEntries(final List<String> containerClasspathEntries) {
    this.containerClasspathEntries = containerClasspathEntries;
  }
  
  @Pure
  public String getJreContainerEntry() {
    return this.jreContainerEntry;
  }
  
  public void setJreContainerEntry(final String jreContainerEntry) {
    this.jreContainerEntry = jreContainerEntry;
  }
  
  @Pure
  public String getDefaultOutput() {
    return this.defaultOutput;
  }
  
  public void setDefaultOutput(final String defaultOutput) {
    this.defaultOutput = defaultOutput;
  }
  
  @Pure
  public JavaBree getBreeToUse() {
    return this.breeToUse;
  }
  
  public void setBreeToUse(final JavaBree breeToUse) {
    this.breeToUse = breeToUse;
  }
  
  @Pure
  public List<String> getFolders() {
    return this.folders;
  }
  
  public void setFolders(final List<String> folders) {
    this.folders = folders;
  }
}
