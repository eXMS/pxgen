/**
 * Copyright (c) 2010 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.pragmaticmodeling.pxgen.runtime.projects;

import java.io.File;
import java.util.List;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.model.IXtextGeneratorFileSystemAccess;
import org.pragmaticmodeling.pxgen.runtime.projects.JavaBree;
import org.pragmaticmodeling.pxgen.runtime.projects.JavaProject;
import org.pragmaticmodeling.pxgen.runtime.projects.PluginProject;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectFactory;

/**
 * @author Sebastian Zarnekow - Initial contribution and API
 */
@SuppressWarnings("all")
public class JavaProjectFactory extends ProjectFactory {
  /**
   * The plug-in identifier of the Java core support (value
   * <code>"org.eclipse.jdt.core"</code>).
   */
  public final static String JDT = "org.eclipse.jdt.core";
  
  /**
   * The identifier for the Java builder (value
   * <code>"org.eclipse.jdt.core.javabuilder"</code>).
   */
  public final static String JDT_BUILDER_ID = (JavaProjectFactory.JDT + ".javabuilder");
  
  @Override
  protected void enhanceProject(final ProjectDescriptor project) {
    super.enhanceProject(project);
    JavaProject javaProject = ((JavaProject) project);
    javaProject.getProjectNatures().add(JavaCore.NATURE_ID);
    javaProject.getBuilderIds().add(JavaCore.BUILDER_ID);
    boolean _needsMavenBuild = javaProject.getConfig().needsMavenBuild();
    if (_needsMavenBuild) {
      javaProject.setDefaultOutput("target/classes");
      boolean _isPluginProject = this.isPluginProject(project);
      boolean _not = (!_isPluginProject);
      if (_not) {
        javaProject.getContainerClasspathEntries().add("org.eclipse.m2e.MAVEN2_CLASSPATH_CONTAINER");
      }
    }
    if ((javaProject.getConfig().needsGradleBuild() && (!this.isPluginProject(project)))) {
      javaProject.getContainerClasspathEntries().add("org.eclipse.buildship.core.gradleclasspathcontainer");
    }
    boolean _contains = javaProject.getBuilderIds().contains(JavaProjectFactory.JDT_BUILDER_ID);
    if (_contains) {
      List<ProjectDescriptor> _referencedProjects = javaProject.getReferencedProjects();
      for (final ProjectDescriptor referencedProject : _referencedProjects) {
        javaProject.getClasspathEntries().add(referencedProject.getLocation());
      }
      String _location = javaProject.getLocation();
      File projectPath = new File(_location);
      List<String> _folders = javaProject.getFolders();
      for (final String folderName : _folders) {
        {
          File sourceFolderPath = new File(projectPath, folderName);
          boolean _exists = sourceFolderPath.exists();
          boolean _not_1 = (!_exists);
          if (_not_1) {
            this.createRecursive(sourceFolderPath);
          }
        }
      }
    }
  }
  
  @Override
  protected void createProjectFiles(final ProjectDescriptor project) {
    super.createProjectFiles(project);
    final JavaProject javaProject = ((JavaProject) project);
    IXtextGeneratorFileSystemAccess _root = project.getRoot();
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<classpath>");
    _builder.newLine();
    {
      List<String> _folders = javaProject.getFolders();
      for(final String sourceFolder : _folders) {
        String _sourceFolderEntry = this.toSourceFolderEntry(sourceFolder);
        _builder.append(_sourceFolderEntry);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("<classpathentry kind=\"con\" path=\"org.eclipse.jdt.launching.JRE_CONTAINER/org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/");
    String _string = this.getJavaBree(javaProject).toString();
    _builder.append(_string, "\t");
    _builder.append("\"/>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<classpathentry kind=\"output\" path=\"");
    String _defaultOutput = javaProject.getDefaultOutput();
    _builder.append(_defaultOutput, "\t");
    _builder.append("\"/>");
    String _addMoreClasspathEntriesTo = this.addMoreClasspathEntriesTo(javaProject);
    _builder.append(_addMoreClasspathEntriesTo, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("</classpath>");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _root.generateFile(".classpath", _builder);
  }
  
  public String toSourceFolderEntry(final String sourceFolder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("<classpathentry kind=\"src\" path=\"");
    _builder.append(sourceFolder, "\t");
    _builder.append("\"/>");
    return _builder.toString();
  }
  
  public String kind(final IClasspathEntry entry) {
    Object _switchResult = null;
    int _entryKind = entry.getEntryKind();
    switch (_entryKind) {
    }
    return ((String)_switchResult);
  }
  
  public String getJavaBree(final JavaProject project) {
    JavaBree _breeToUse = project.getBreeToUse();
    boolean _tripleEquals = (_breeToUse == null);
    if (_tripleEquals) {
      return JavaBree.JavaSE18.toString();
    } else {
      return project.getBreeToUse().toString();
    }
  }
  
  private boolean isPluginProject(final ProjectDescriptor project) {
    return (project instanceof PluginProject);
  }
  
  protected String addMoreClasspathEntriesTo(final JavaProject javaProject) {
    return null;
  }
  
  protected void createFolders(final JavaProject project) throws CoreException {
    List<String> _folders = project.getFolders();
    boolean _tripleNotEquals = (_folders != null);
    if (_tripleNotEquals) {
      List<String> _folders_1 = project.getFolders();
      for (final String folderName : _folders_1) {
        {
          String _location = project.getLocation();
          final File folder = new File(_location, folderName);
          boolean _exists = folder.exists();
          boolean _not = (!_exists);
          if (_not) {
            this.createRecursive(folder);
          }
        }
      }
    }
  }
}
