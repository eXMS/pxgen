package org.pragmaticmodeling.pxgen.runtime.projects;

import java.util.Set;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Pure;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

@SuppressWarnings("all")
public class PlainProject extends ProjectDescriptor {
  private String projectModelName;
  
  @Accessors
  private boolean partOfMavenBuild;
  
  @Accessors
  private boolean partOfGradleBuild;
  
  /**
   * @since 2.4
   */
  protected String defaultCharset;
  
  @Override
  public Set<String> getSourceFolders() {
    return CollectionLiterals.<String>emptySet();
  }
  
  public void setProjectModelName(final String name) {
    this.projectModelName = name;
  }
  
  @Pure
  public boolean isPartOfMavenBuild() {
    return this.partOfMavenBuild;
  }
  
  public void setPartOfMavenBuild(final boolean partOfMavenBuild) {
    this.partOfMavenBuild = partOfMavenBuild;
  }
  
  @Pure
  public boolean isPartOfGradleBuild() {
    return this.partOfGradleBuild;
  }
  
  public void setPartOfGradleBuild(final boolean partOfGradleBuild) {
    this.partOfGradleBuild = partOfGradleBuild;
  }
}
