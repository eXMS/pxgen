package org.pragmaticmodeling.pxgen.runtime.projects;

import com.google.inject.Injector;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.eclipse.xtext.xtext.generator.model.IXtextGeneratorFileSystemAccess;
import org.pragmaticmodeling.pxgen.runtime.projects.JavaProject;

@SuppressWarnings("all")
public class PluginProject extends JavaProject {
  @Accessors
  private List<String> requiredBundles = new ArrayList<String>();
  
  @Accessors
  private List<String> exportedPackages = new ArrayList<String>();
  
  @Accessors
  private List<String> importedPackages = new ArrayList<String>();
  
  @Accessors
  private String activatorClassName;
  
  @Accessors
  private List<String> developmentTimeBundles = new ArrayList<String>();
  
  @Accessors
  private List<String> buildBinIncludes = new ArrayList<String>();
  
  @Accessors
  private Map<String, String> bundleProperties = new HashMap<String, String>();
  
  @Accessors
  private boolean withPluginXml = true;
  
  private IXtextGeneratorFileSystemAccess metaInf;
  
  public IXtextGeneratorFileSystemAccess getMetaInf() {
    if ((this.metaInf == null)) {
      String _rootLocation = this.getConfig().getRootLocation();
      String _name = this.getName();
      File rootPath = new File(_rootLocation, _name);
      String _absolutePath = rootPath.getAbsolutePath();
      String _plus = (_absolutePath + "/META-INF");
      this.metaInf = this.newFileSystemAccess(_plus, false);
    }
    return this.metaInf;
  }
  
  @Override
  public void initialize(final Injector injector) {
    super.initialize(injector);
    this.getMetaInf().initialize(injector);
  }
  
  @Pure
  public List<String> getRequiredBundles() {
    return this.requiredBundles;
  }
  
  public void setRequiredBundles(final List<String> requiredBundles) {
    this.requiredBundles = requiredBundles;
  }
  
  @Pure
  public List<String> getExportedPackages() {
    return this.exportedPackages;
  }
  
  public void setExportedPackages(final List<String> exportedPackages) {
    this.exportedPackages = exportedPackages;
  }
  
  @Pure
  public List<String> getImportedPackages() {
    return this.importedPackages;
  }
  
  public void setImportedPackages(final List<String> importedPackages) {
    this.importedPackages = importedPackages;
  }
  
  @Pure
  public String getActivatorClassName() {
    return this.activatorClassName;
  }
  
  public void setActivatorClassName(final String activatorClassName) {
    this.activatorClassName = activatorClassName;
  }
  
  @Pure
  public List<String> getDevelopmentTimeBundles() {
    return this.developmentTimeBundles;
  }
  
  public void setDevelopmentTimeBundles(final List<String> developmentTimeBundles) {
    this.developmentTimeBundles = developmentTimeBundles;
  }
  
  @Pure
  public List<String> getBuildBinIncludes() {
    return this.buildBinIncludes;
  }
  
  public void setBuildBinIncludes(final List<String> buildBinIncludes) {
    this.buildBinIncludes = buildBinIncludes;
  }
  
  @Pure
  public Map<String, String> getBundleProperties() {
    return this.bundleProperties;
  }
  
  public void setBundleProperties(final Map<String, String> bundleProperties) {
    this.bundleProperties = bundleProperties;
  }
  
  @Pure
  public boolean isWithPluginXml() {
    return this.withPluginXml;
  }
  
  public void setWithPluginXml(final boolean withPluginXml) {
    this.withPluginXml = withPluginXml;
  }
}
