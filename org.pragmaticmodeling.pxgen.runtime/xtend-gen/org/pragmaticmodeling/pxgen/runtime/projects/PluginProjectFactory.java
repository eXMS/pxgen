/**
 * Copyright (c) 2010 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.pragmaticmodeling.pxgen.runtime.projects;

import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.pragmaticmodeling.pxgen.runtime.files.BuildProperties;
import org.pragmaticmodeling.pxgen.runtime.files.Manifest;
import org.pragmaticmodeling.pxgen.runtime.files.Properties;
import org.pragmaticmodeling.pxgen.runtime.projects.JavaProject;
import org.pragmaticmodeling.pxgen.runtime.projects.JavaProjectFactory;
import org.pragmaticmodeling.pxgen.runtime.projects.PluginProject;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

/**
 * @author Sebastian Zarnekow - Initial contribution and API
 */
@SuppressWarnings("all")
public class PluginProjectFactory extends JavaProjectFactory {
  @Override
  protected void enhanceProject(final ProjectDescriptor descriptor) {
    super.enhanceProject(descriptor);
    PluginProject project = ((PluginProject) descriptor);
    project.getProjectNatures().add("org.eclipse.pde.PluginNature");
    project.getBuilderIds().add("org.eclipse.pde.ManifestBuilder");
    project.getBuilderIds().add("org.eclipse.pde.SchemaBuilder");
  }
  
  @Override
  protected void createProjectFiles(final ProjectDescriptor descriptor) {
    super.createProjectFiles(descriptor);
    PluginProject project = ((PluginProject) descriptor);
    if (((project.getProjectNatures() != null) && 
      project.getProjectNatures().contains("org.eclipse.pde.PluginNature"))) {
      this.createManifest(project);
      this.createBuildProperties(project);
      this.createPluginProperties(project);
    }
  }
  
  @Override
  protected String addMoreClasspathEntriesTo(final JavaProject javaProject) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("<classpathentry kind=\"con\" path=\"org.eclipse.pde.core.requiredPlugins\"/>");
    return _builder.toString();
  }
  
  protected void createBuildProperties(final PluginProject project) {
    try {
      String _location = project.getLocation();
      final File projectFile = new File(_location);
      final File file = new File(projectFile, "build.properties");
      final BuildProperties buildProperties = new BuildProperties(file);
      List<String> _folders = project.getFolders();
      for (final String folder : _folders) {
        buildProperties.addSource(this.withEndingSlash(folder));
      }
      List<String> _buildBinIncludes = project.getBuildBinIncludes();
      for (final String binInclude : _buildBinIncludes) {
        {
          String _location_1 = project.getLocation();
          File fileToInclude = new File(_location_1, binInclude);
          boolean _isDirectory = fileToInclude.isDirectory();
          if (_isDirectory) {
            buildProperties.addBinInclude(this.withEndingSlash(binInclude));
          } else {
            buildProperties.addBinInclude(binInclude);
          }
        }
      }
      buildProperties.addBinInclude("META-INF/");
      buildProperties.addBinInclude(".");
      boolean _isEmpty = project.getBundleProperties().isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        buildProperties.addBinInclude("plugin.properties");
      }
      boolean _isWithPluginXml = project.isWithPluginXml();
      if (_isWithPluginXml) {
        buildProperties.addBinInclude("plugin.xml");
      }
      List<String> _developmentTimeBundles = project.getDevelopmentTimeBundles();
      for (final String bundle : _developmentTimeBundles) {
        buildProperties.addAdditionalBundle(bundle);
      }
      buildProperties.save();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected void createPluginProperties(final PluginProject project) {
    try {
      Map<String, String> propsMap = project.getBundleProperties();
      boolean _isEmpty = propsMap.isEmpty();
      if (_isEmpty) {
        return;
      }
      String _location = project.getLocation();
      final File projectFile = new File(_location);
      final File file = new File(projectFile, "plugin.properties");
      final Properties pluginProperties = new Properties(file);
      Set<String> _keySet = propsMap.keySet();
      for (final String name : _keySet) {
        {
          String props = propsMap.get(name);
          if (((props != null) && (!props.isEmpty()))) {
            pluginProperties.setProperty(name, project.getBundleProperties().get(name));
          }
        }
      }
      pluginProperties.save();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public String withEndingSlash(final String string) {
    String value = string.trim();
    boolean _endsWith = value.endsWith("/");
    boolean _not = (!_endsWith);
    if (_not) {
      String _value = value;
      value = (_value + "/");
    }
    return value;
  }
  
  protected void createManifest(final PluginProject project) {
    try {
      String _location = project.getLocation();
      final File projectFile = new File(_location);
      final File metaInf = new File(projectFile, "META-INF");
      boolean _exists = metaInf.exists();
      boolean _not = (!_exists);
      if (_not) {
        metaInf.mkdir();
      }
      File manifestFile = new File(metaInf, "MANIFEST.MF");
      boolean _exists_1 = manifestFile.exists();
      if (_exists_1) {
        Manifest manifest = new Manifest(manifestFile);
        List<String> _requiredBundles = project.getRequiredBundles();
        for (final String b : _requiredBundles) {
          manifest.addPluginDependency(b);
        }
        List<String> _exportedPackages = project.getExportedPackages();
        for (final String p : _exportedPackages) {
          manifest.addExportedPackage(p);
        }
        List<String> _importedPackages = project.getImportedPackages();
        for (final String p_1 : _importedPackages) {
          manifest.addImportedPackage(p_1);
        }
        boolean _isEmpty = project.getBundleProperties().isEmpty();
        boolean _not_1 = (!_isEmpty);
        if (_not_1) {
          manifest.addProperty("Bundle-Localization", "plugin");
        }
        manifest.save();
      } else {
        final StringBuilder content = new StringBuilder("Manifest-Version: 1.0\n");
        content.append("Bundle-ManifestVersion: 2\n");
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Bundle-Name: ");
        String _name = project.getName();
        _builder.append(_name);
        _builder.newLineIfNotEmpty();
        content.append(_builder);
        content.append("Bundle-Vendor: My Company\n");
        content.append("Bundle-Version: 1.0.0.qualifier\n");
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Bundle-SymbolicName: ");
        String _name_1 = project.getName();
        _builder_1.append(_name_1);
        _builder_1.append("; singleton:=true");
        _builder_1.newLineIfNotEmpty();
        content.append(_builder_1);
        String _activatorClassName = project.getActivatorClassName();
        boolean _tripleNotEquals = (null != _activatorClassName);
        if (_tripleNotEquals) {
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("Bundle-Activator: ");
          String _activatorClassName_1 = project.getActivatorClassName();
          _builder_2.append(_activatorClassName_1);
          _builder_2.newLineIfNotEmpty();
          content.append(_builder_2);
        }
        boolean _isEmpty_1 = project.getBundleProperties().isEmpty();
        boolean _not_2 = (!_isEmpty_1);
        if (_not_2) {
          content.append("Bundle-Localization: plugin\n");
        }
        content.append("Bundle-ActivationPolicy: lazy\n");
        this.addToContent(content, project.getRequiredBundles(), "Require-Bundle");
        this.addToContent(content, project.getExportedPackages(), "Export-Package");
        this.addToContent(content, project.getImportedPackages(), "Import-Package");
        content.append("Bundle-RequiredExecutionEnvironment: ");
        StringConcatenation _builder_3 = new StringConcatenation();
        String _javaBree = this.getJavaBree(project);
        _builder_3.append(_javaBree);
        _builder_3.newLineIfNotEmpty();
        content.append(_builder_3);
        project.getMetaInf().generateFile("MANIFEST.MF", content.toString());
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected void addToContent(final StringBuilder content, final List<String> entries, final String prefix) {
    if (((entries != null) && (!entries.isEmpty()))) {
      content.append(prefix).append(": ").append(entries.get(0));
      {
        int i = 1;
        int x = entries.size();
        boolean _while = (i < x);
        while (_while) {
          content.append(",\n ").append(entries.get(i));
          i++;
          _while = (i < x);
        }
      }
      content.append("\n");
    }
  }
  
  /**
   * @since 2.8
   */
  protected void addToBuildProperties(final StringBuilder content, final Iterable<String> entries, final String entryName) {
    if (((entries != null) && (!Iterables.isEmpty(entries)))) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(entryName);
      _builder.append(" = ");
      String assigment = _builder.toString();
      String indent = Strings.repeat(" ", assigment.length());
      content.append(assigment);
      for (final Iterator<String> iterator = entries.iterator(); iterator.hasNext();) {
        {
          content.append(iterator.next());
          boolean _hasNext = iterator.hasNext();
          if (_hasNext) {
            content.append(",\\\n");
            content.append(indent);
          }
        }
      }
    }
  }
}
