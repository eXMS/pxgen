/**
 * Copyright (c) 2015 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.pragmaticmodeling.pxgen.runtime.projects;

import com.google.inject.Inject;
import com.google.inject.Injector;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Pure;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.IXtextGeneratorFileSystemAccess;
import org.eclipse.xtext.xtext.generator.model.XtextGeneratorFileSystemAccess;
import org.pragmaticmodeling.pxgen.runtime.IGenerator;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorConfig;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorFragment;

@SuppressWarnings("all")
public abstract class ProjectDescriptor implements IGuiceAwareGeneratorComponent {
  private IGeneratorConfig config;
  
  @Accessors
  @Inject
  private IGenerator generator;
  
  @Accessors
  private boolean enabled;
  
  @Accessors
  private Map<String, IXtextGeneratorFileSystemAccess> fsaMap = new HashMap<String, IXtextGeneratorFileSystemAccess>();
  
  @Accessors
  private List<ProjectDescriptor> referencedProjects = new ArrayList<ProjectDescriptor>();
  
  @Accessors
  private List<String> projectNatures = new ArrayList<String>();
  
  @Accessors
  private List<String> builderIds = new ArrayList<String>();
  
  private IXtextGeneratorFileSystemAccess root;
  
  private String name;
  
  private String nameQualifier;
  
  private IGeneratorFragment parentFragment;
  
  public IGeneratorFragment getParentFragment() {
    return this.parentFragment;
  }
  
  public void setParentFragment(final IGeneratorFragment parent) {
    this.parentFragment = parent;
    this.generator = this.getParentFragment().getGenerator();
    this.config = ((IGeneratorConfig) this.generator);
  }
  
  public String getName() {
    if ((this.name == null)) {
      String _basePackage = this.getConfig().getBasePackage();
      String _nameQualifier = this.getNameQualifier();
      return (_basePackage + _nameQualifier);
    } else {
      return this.name;
    }
  }
  
  public void setName(final String newName) {
    this.name = newName;
  }
  
  public IGeneratorConfig getConfig() {
    return this.config;
  }
  
  public String getNameQualifier() {
    String _xifexpression = null;
    if ((this.nameQualifier == null)) {
      _xifexpression = "";
    } else {
      _xifexpression = this.nameQualifier;
    }
    return _xifexpression;
  }
  
  public void setNameQualifier(final String value) {
    this.nameQualifier = value;
  }
  
  public IXtextGeneratorFileSystemAccess getRoot() {
    if ((this.root == null)) {
      String _rootLocation = this.config.getRootLocation();
      String _name = this.getName();
      File rootPath = new File(_rootLocation, _name);
      this.root = this.newFileSystemAccess(rootPath.getAbsolutePath(), true);
    }
    return this.root;
  }
  
  public Map<String, IXtextGeneratorFileSystemAccess> getFileSystemAccesses() {
    return this.fsaMap;
  }
  
  public String getLocation() {
    return this.getRoot().getPath();
  }
  
  public Set<? extends ProjectDescriptor> getUpstreamProjects() {
    return CollectionLiterals.<ProjectDescriptor>emptySet();
  }
  
  public abstract Set<String> getSourceFolders();
  
  @Override
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
    Collection<IXtextGeneratorFileSystemAccess> _values = this.getFileSystemAccesses().values();
    for (final IXtextGeneratorFileSystemAccess fsa : _values) {
      fsa.initialize(injector);
    }
  }
  
  public void addGenerationDirectory(final String dir, final boolean overwrite) {
    String _rootLocation = this.config.getRootLocation();
    String _name = this.getName();
    File projectLocation = new File(_rootLocation, _name);
    String directory = "";
    boolean _equals = ".".equals(dir);
    if (_equals) {
      directory = projectLocation.getAbsolutePath();
    } else {
      final File file = new File(projectLocation, dir);
      boolean _exists = file.exists();
      boolean _not = (!_exists);
      if (_not) {
        file.mkdirs();
      }
      directory = file.getAbsolutePath();
    }
    IXtextGeneratorFileSystemAccess fsa = this.newFileSystemAccess(directory, overwrite);
    this.getFileSystemAccesses().put(dir, fsa);
  }
  
  protected IXtextGeneratorFileSystemAccess newFileSystemAccess(final String path, final boolean overWrite) {
    return new XtextGeneratorFileSystemAccess(path, overWrite);
  }
  
  @Pure
  public IGenerator getGenerator() {
    return this.generator;
  }
  
  public void setGenerator(final IGenerator generator) {
    this.generator = generator;
  }
  
  @Pure
  public boolean isEnabled() {
    return this.enabled;
  }
  
  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;
  }
  
  @Pure
  public Map<String, IXtextGeneratorFileSystemAccess> getFsaMap() {
    return this.fsaMap;
  }
  
  public void setFsaMap(final Map<String, IXtextGeneratorFileSystemAccess> fsaMap) {
    this.fsaMap = fsaMap;
  }
  
  @Pure
  public List<ProjectDescriptor> getReferencedProjects() {
    return this.referencedProjects;
  }
  
  public void setReferencedProjects(final List<ProjectDescriptor> referencedProjects) {
    this.referencedProjects = referencedProjects;
  }
  
  @Pure
  public List<String> getProjectNatures() {
    return this.projectNatures;
  }
  
  public void setProjectNatures(final List<String> projectNatures) {
    this.projectNatures = projectNatures;
  }
  
  @Pure
  public List<String> getBuilderIds() {
    return this.builderIds;
  }
  
  public void setBuilderIds(final List<String> builderIds) {
    this.builderIds = builderIds;
  }
}
