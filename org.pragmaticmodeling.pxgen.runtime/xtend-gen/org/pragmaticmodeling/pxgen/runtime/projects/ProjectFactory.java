/**
 * Copyright (c) 2010 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.pragmaticmodeling.pxgen.runtime.projects;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.model.IXtextGeneratorFileSystemAccess;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

/**
 * @author Holger Schill - Initial contribution and API
 * @author Sebastian Zarnekow
 */
@SuppressWarnings("all")
public class ProjectFactory {
  /**
   * @since 2.4
   */
  protected String defaultCharset;
  
  /**
   * @since 2.4
   */
  public void setProjectDefaultCharset(final String encoding) {
    this.defaultCharset = encoding;
  }
  
  public File prepareProject(final ProjectDescriptor description) {
    File project = this.getProject(description);
    project = this.prepare(description);
    this.enhanceProject(description);
    return project;
  }
  
  public File finishProject(final ProjectDescriptor description) {
    File project = this.getProject(description);
    this.createProjectFiles(description);
    return project;
  }
  
  protected void createProjectFiles(final ProjectDescriptor project) {
    IXtextGeneratorFileSystemAccess _root = project.getRoot();
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<projectDescription>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<name>");
    String _name = project.getName();
    _builder.append(_name, "\t");
    _builder.append("</name>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<comment></comment>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<projects>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</projects>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<buildSpec>");
    _builder.newLine();
    {
      List<String> _builderIds = project.getBuilderIds();
      for(final String builder : _builderIds) {
        _builder.append("\t\t");
        _builder.append("<buildCommand>");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("<name>");
        _builder.append(builder, "\t\t\t");
        _builder.append("</name>");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("<arguments>");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("</arguments>");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("</buildCommand>");
        _builder.newLine();
      }
    }
    _builder.append("\t");
    _builder.append("</buildSpec>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<natures>");
    _builder.newLine();
    {
      List<String> _projectNatures = project.getProjectNatures();
      for(final String nature : _projectNatures) {
        _builder.append("\t\t");
        _builder.append("<nature>");
        _builder.append(nature, "\t\t");
        _builder.append("</nature>");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("</natures>");
    _builder.newLine();
    _builder.append("</projectDescription>");
    _builder.newLine();
    _root.generateFile(".project", _builder);
  }
  
  protected File prepare(final ProjectDescriptor description) {
    File project = this.getProject(description);
    boolean _exists = project.exists();
    boolean _not = (!_exists);
    if (_not) {
      this.createRecursive(project);
    }
    return project;
  }
  
  private File getProject(final ProjectDescriptor description) {
    String loc = description.getLocation();
    File project = new File(loc);
    return project;
  }
  
  protected void enhanceProject(final ProjectDescriptor project) {
    boolean _needsMavenBuild = project.getConfig().needsMavenBuild();
    if (_needsMavenBuild) {
      project.getProjectNatures().add("org.eclipse.m2e.core.maven2Nature");
      project.getBuilderIds().add("org.eclipse.m2e.core.maven2Builder");
    }
    boolean _needsGradleBuild = project.getConfig().needsGradleBuild();
    if (_needsGradleBuild) {
      project.getProjectNatures().add("org.eclipse.buildship.core.gradleprojectnature");
      project.getBuilderIds().add("org.eclipse.buildship.core.gradleprojectbuilder");
    }
  }
  
  protected void setBuilder(final IProjectDescription projectDescription, final String[] builders) {
    List<ICommand> commands = new ArrayList<ICommand>();
    for (int i = 0; (i < builders.length); i++) {
      {
        ICommand command = projectDescription.newCommand();
        String _xblockexpression = null;
        {
          final int _rdIndx_builders = i;
          _xblockexpression = builders[_rdIndx_builders];
        }
        command.setBuilderName(_xblockexpression);
        commands.add(command);
      }
    }
    projectDescription.setBuildSpec(commands.<ICommand>toArray(new ICommand[commands.size()]));
  }
  
  protected void createRecursive(final File resource) {
    boolean _exists = resource.exists();
    boolean _not = (!_exists);
    if (_not) {
      boolean _exists_1 = resource.getParentFile().exists();
      boolean _not_1 = (!_exists_1);
      if (_not_1) {
        this.createRecursive(resource.getParentFile());
      }
      boolean _isDirectory = resource.isDirectory();
      if (_isDirectory) {
        resource.mkdir();
      }
    }
  }
}
