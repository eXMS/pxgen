package org.pragmaticmodeling.pxgen.launcher;

import static org.pragmaticmodeling.pxgen.launcher.WorkflowLaunchUtils.*;

import org.eclipse.core.resources.IResource;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;

public class GenerateArtifactsLaunchShortcut implements ILaunchShortcut {

	@Override
	public void launch(ISelection selection, String mode) {
		IResource workflowFile = workflowFileFor(selection);
		if (workflowFile != null) {
			runWorkflow(workflowFile, mode);
		}
	}

	@Override
	public void launch(IEditorPart editor, String mode) {
		IResource workflowFile = workflowFileFor(editor);
		if (workflowFile != null) {
			runWorkflow(workflowFile, mode);
		}
	}

}
