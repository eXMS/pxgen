package org.pragmaticmodeling.pxgen.launcher;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

public class NewProjectsImporterJob extends Job {
	private boolean running = true;
	private IResource worflowFile;
	private static final Logger logger = Logger.getLogger(NewProjectsImporterJob.class);

	public NewProjectsImporterJob() {
		this(null);
	}

	public NewProjectsImporterJob(IResource workflowFile) {
		super("New pxGen Projects Importer Job");
		this.worflowFile = workflowFile;
	}

	protected IStatus run(IProgressMonitor monitor) {
		IWorkspaceRoot wRoot = ResourcesPlugin.getWorkspace().getRoot(); 
		File resultFile = findProjectsFile();
		if (resultFile.exists()) {
			try {
				String[] projects = readFile(resultFile.getAbsolutePath(), Charset.defaultCharset()).split(",");
				for (String prj : projects) {
					String project = prj.trim();
					File projectFolder = new File(project);
					String projectName = projectFolder.getName();
					IProject iProject = wRoot.getProject(projectName);
					if (!iProject.exists()) {
						IProjectDescription description = ResourcesPlugin.getWorkspace()
								.loadProjectDescription(new Path(project + "/.project"));
						iProject = ResourcesPlugin.getWorkspace().getRoot().getProject(description.getName());
						iProject.create(description, null);
						iProject.open(null);
					}
				}
				resultFile.delete();
				stop();
				return Status.OK_STATUS;
			} catch (CoreException e) {
				logger.error(e.getMessage());
				resultFile.delete();
				stop();
			} catch (Exception e) {
				logger.error(e.getMessage());
				resultFile.delete();
				stop();
			}
		}
		schedule(2000);
		return Status.OK_STATUS;
	}

	private File findProjectsFile() {
		if (worflowFile != null) {
			File wFile = worflowFile.getLocation().toFile();
			File container = wFile.getParentFile();
			while (container != null) {
				File member = new File(container, ".pxgenProjects");
				if (member.exists()) {
					return member;
				}
				container = container.getParentFile();
			}
		}
		
		IWorkspaceRoot wRoot = ResourcesPlugin.getWorkspace().getRoot(); 
		File root = wRoot.getLocation().toFile();
		File resultFile = new File(root, ".pxgenProjects");
		return resultFile;
	}

	public boolean shouldSchedule() {
		return running;
	}

	public void stop() {
		running = false;
	}

	protected String getContents(IFile file) throws Exception {
		java.io.InputStream is = file.getContents();
		byte[] b = new byte[is.available()];
		is.read(b);
		is.close();

		return (new String(b));
	}

	static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
}