package org.pragmaticmodeling.pxgen.ui.highlighting

import java.util.ArrayList
import java.util.List
import org.eclipse.xtext.EnumRule
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor
import org.eclipse.xtext.ide.editor.syntaxcoloring.ISemanticHighlightingCalculator
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.util.CancelIndicator
import org.eclipse.xtext.xbase.ide.highlighting.XbaseHighlightingCalculator

class PxGenHighlightingCalculator extends XbaseHighlightingCalculator implements ISemanticHighlightingCalculator {
	static List<String> pxgenKeywords = null
	static List<String> pxgenKeywordFeatures = null

	new() {
		super()
		// keywords
		pxgenKeywords = new ArrayList<String>()
		pxgenKeywords.add("generatorConf")
		pxgenKeywords.add("enable")
		pxgenKeywords.add("file")
		pxgenKeywords.add("project")
		pxgenKeywords.add("generatorDef")
		pxgenKeywords.add("extends")
		pxgenKeywords.add("param")
		pxgenKeywords.add("run")
		pxgenKeywords.add("generate-file")
		pxgenKeywords.add("generate-java")
		pxgenKeywords.add("source")
		pxgenKeywords.add("dir")
		
		// properties
		pxgenKeywordFeatures = new ArrayList<String>()
		pxgenKeywordFeatures.add("enabled:")
		pxgenKeywordFeatures.add("path:")
		pxgenKeywordFeatures.add("name:")
		pxgenKeywordFeatures.add("genDir:")
		pxgenKeywordFeatures.add("to:")
		pxgenKeywordFeatures.add("overwrite:")

	}

	override void provideHighlightingFor(XtextResource resource, IHighlightedPositionAcceptor acceptor,
		CancelIndicator cancelIndicator) {
		super.provideHighlightingFor(resource, acceptor, cancelIndicator)
		if(resource === null || resource.getParseResult() === null) return;
		var INode root = resource.getParseResult().getRootNode()
		for (INode node : root.getAsTreeIterable()) {
			var grammarElement = node.grammarElement
			if (grammarElement instanceof RuleCall && (grammarElement as RuleCall).rule instanceof EnumRule) {
				acceptor.addPosition(node.getOffset(), node.getLength(),
						PxGenHighlightingConfiguration.PXGEN_ENUM_ID)
			} else if (pxgenKeywords.contains(node.getText())) {
				acceptor.addPosition(node.getOffset(), node.getLength(),
					PxGenHighlightingConfiguration.PXGEN_KEYWORD_ID)
			} else if (pxgenKeywordFeatures.contains(node.getText())) {
				acceptor.addPosition(node.getOffset(), node.getLength(),
					PxGenHighlightingConfiguration.PXGEN_KEYWORD_FEATURE_ID)
			}
		}
	}
}
