package org.pragmaticmodeling.pxgen.ui.highlighting

import org.eclipse.jface.text.TextAttribute
import org.eclipse.swt.SWT
import org.eclipse.swt.graphics.RGB
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor
import org.eclipse.xtext.ui.editor.utils.TextStyle

class PxGenHighlightingConfiguration extends DefaultHighlightingConfiguration {
	public static final String PXGEN_KEYWORD_ID = "pxgen.keyword"
	public static final String PXGEN_KEYWORD_FEATURE_ID = "pxdoc.keyword.feature"
	public static final String PXGEN_ENUM_ID = "pxdoc.enum"
	public static final String STATIC_METHOD_INVOCATION = "xbase.static.method.invocation"
	public static final String STATIC_FIELD = "xbase.static.field"
	public static final String FIELD = "xbase.field"
	public static final String ANNOTATION = "xbase.annotation"
	public static final String EXTENSION_METHOD_INVOCATION = "xbase.extension.method.invacation"
	public static final String DEPRECATED_MEMBERS = "xbase.deprecated.members"

	override void configure(IHighlightingConfigurationAcceptor acceptor) {
		acceptor.acceptDefaultHighlighting(STATIC_METHOD_INVOCATION, "Static method invocation",
			staticMethodInvocation())
		acceptor.acceptDefaultHighlighting(STATIC_FIELD, "Static Field", staticField())
		acceptor.acceptDefaultHighlighting(FIELD, "Field", field())
		acceptor.acceptDefaultHighlighting(ANNOTATION, "Annotation", annotation())
		acceptor.acceptDefaultHighlighting(EXTENSION_METHOD_INVOCATION, "Extension method invocation",
			extensionMethodInvocation())
		acceptor.acceptDefaultHighlighting(DEPRECATED_MEMBERS, "Deprecated members", deprecatedMembers())
		acceptor.acceptDefaultHighlighting(PXGEN_KEYWORD_ID, "pxGen Keywords", pxgenKeywordTextStyle())
		acceptor.acceptDefaultHighlighting(PXGEN_KEYWORD_FEATURE_ID, "pxGen Keywords Features",
			pxgenKeywordFeatureTextStyle())
		acceptor.acceptDefaultHighlighting(PXGEN_ENUM_ID, "Enumeration values", pxgenEnumTextStyle())
		super.configure(acceptor)
	}

	def TextStyle staticMethodInvocation() {
		var TextStyle textStyle = defaultTextStyle().copy()
		textStyle.setStyle(SWT.ITALIC)
		return textStyle
	}

	def TextStyle staticField() {
		var TextStyle textStyle = defaultTextStyle().copy()
		textStyle.setStyle(SWT.ITALIC)
		textStyle.setColor(new RGB(0, 0, 192))
		return textStyle
	}

	def TextStyle field() {
		var TextStyle textStyle = defaultTextStyle().copy()
		textStyle.setColor(new RGB(0, 26, 171))
		return textStyle
	}

	def TextStyle annotation() {
		var TextStyle textStyle = defaultTextStyle().copy()
		textStyle.setColor(new RGB(100, 100, 100))
		return textStyle
	}

	def TextStyle extensionMethodInvocation() {
		var TextStyle textStyle = defaultTextStyle().copy()
		textStyle.setColor(new RGB(171, 48, 0))
		return textStyle
	}

	def TextStyle deprecatedMembers() {
		var TextStyle textStyle = defaultTextStyle().copy()
		textStyle.setStyle(TextAttribute.STRIKETHROUGH)
		return textStyle
	}

	def TextStyle pxgenKeywordTextStyle() {
		var TextStyle textStyle = new TextStyle()
		textStyle.setColor(new RGB(105, 0, 210))
		textStyle.setStyle(SWT.BOLD)
		return textStyle
	}
	
	def TextStyle pxgenEnumTextStyle() {
		var TextStyle textStyle = new TextStyle()
		textStyle.setColor(new RGB(0, 0, 192))
		textStyle.setStyle(PxGenHighlightingHelper.italicAndBold)
		return textStyle
	}

	def TextStyle pxgenStyleTextStyle() {
		var TextStyle textStyle = new TextStyle()
		textStyle.setStyle(SWT.ITALIC)
		return textStyle
	}

	def TextStyle pxgenKeywordFeatureTextStyle() {
		var TextStyle textStyle = new TextStyle()
		textStyle.setColor(new RGB(0, 164, 242))
		textStyle.setStyle(SWT.ITALIC)
		return textStyle
	}
}
