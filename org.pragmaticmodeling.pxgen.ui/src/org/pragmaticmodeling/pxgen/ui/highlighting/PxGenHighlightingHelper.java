package org.pragmaticmodeling.pxgen.ui.highlighting;

import org.eclipse.swt.SWT;

public class PxGenHighlightingHelper {

	public static int italicAndBold() {
		return SWT.ITALIC | SWT.BOLD;
	}
}
