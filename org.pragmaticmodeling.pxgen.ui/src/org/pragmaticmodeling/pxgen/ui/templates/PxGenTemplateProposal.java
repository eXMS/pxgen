package org.pragmaticmodeling.pxgen.ui.templates;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.swt.graphics.Image;
import org.eclipse.xtext.ui.editor.templates.XtextTemplateProposal;
import org.pragmaticmodeling.pxgen.runtime.files.BuildProperties;

public class PxGenTemplateProposal extends XtextTemplateProposal {

	private IProject project;
	private String bundle;

	public PxGenTemplateProposal(IProject project, Template template, TemplateContext context, IRegion region, Image image, int relevance) {
		super(template, context, region, image);
		this.project = project;
		String id = template.getContextTypeId();
		int index = id.indexOf("@"); 
		if (index>-1) {
			int lastIndex = id.lastIndexOf("@");
			if (lastIndex != index) {
				this.bundle = id.substring(lastIndex+1);
			}
		}
	}
	
	@Override
	public void apply(ITextViewer viewer, char trigger, int stateMask, int offset) {
		super.apply(viewer, trigger, stateMask, offset);
		IFile file = project.getFile("build.properties");
		if (file != null && file.exists()) {

				WorkspaceJob job = new WorkspaceJob("Build Project") {

					@Override
					public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {
						BuildProperties buildProperties;
						try {
							buildProperties = new BuildProperties(file.getLocation().toFile());
							buildProperties.addAdditionalBundle(bundle);
							buildProperties.save();
							project.refreshLocal(IResource.DEPTH_INFINITE, monitor);
							project.build(IncrementalProjectBuilder.FULL_BUILD, monitor);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return Status.OK_STATUS;
					}
					
				};
				job.schedule();
				

		}
	}

	
}
