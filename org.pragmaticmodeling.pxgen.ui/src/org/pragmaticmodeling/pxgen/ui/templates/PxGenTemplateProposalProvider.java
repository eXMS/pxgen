package org.pragmaticmodeling.pxgen.ui.templates;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.jface.text.templates.TemplateProposal;
import org.eclipse.jface.text.templates.persistence.TemplateStore;
import org.eclipse.swt.graphics.Image;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ITemplateAcceptor;
import org.eclipse.xtext.ui.editor.templates.ContextTypeIdHelper;
import org.eclipse.xtext.ui.editor.templates.DefaultTemplateProposalProvider;
import org.pragmaticmodeling.pxgen.pxGen.FragmentEnablement;
import org.pragmaticmodeling.pxgen.pxGen.GenConf;
import org.pragmaticmodeling.pxgen.runtime.IPxGeneratorsExtensionManager;

import com.google.inject.Inject;

public class PxGenTemplateProposalProvider extends DefaultTemplateProposalProvider {

	private PxGenTemplateStore templateStore;
	private static final Logger logger = Logger.getLogger(PxGenTemplateProposalProvider.class);
	
	@Inject
	IPxGeneratorsExtensionManager extensionsManager;

	@Inject
	public PxGenTemplateProposalProvider(TemplateStore templateStore, ContextTypeRegistry registry,
			ContextTypeIdHelper helper) {
		super(templateStore, registry, helper);
		this.templateStore = (PxGenTemplateStore) templateStore;
		try {
			templateStore.load();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	@Override
	protected void createTemplates(TemplateContext templateContext, ContentAssistContext context,
			ITemplateAcceptor acceptor) {
		TemplateContextType contextType = templateContext.getContextType();
		Template[] templates = null;
		if (contextType.getId().equals("org.pragmaticmodeling.pxgen.PxGen.FragmentEnablement")) {
			EObject model = context.getCurrentModel();
			JvmTypeReference jvmType = null;
			if (model instanceof GenConf) {
				jvmType = ((GenConf)model).getGenerator();
			} else if (model instanceof FragmentEnablement) {
				jvmType = ((FragmentEnablement)model).getExtender();
			}
			if (jvmType != null) {
				templates = templateStore.getTemplates(contextType.getId(), jvmType.getQualifiedName());
				for (Template template : templates) {
					if (!acceptor.canAcceptMoreTemplates())
						return;
					if (validate(template, templateContext)) {
						acceptor.accept(createProposalWithAction(template, templateContext, context, getImage(template),
								getRelevance(template) + 1000));
					}
				}	
			}
		} else {
			templates = templateStore.getTemplates(contextType.getId());
			for (Template template : templates) {
				if (!acceptor.canAcceptMoreTemplates())
					return;
				if (validate(template, templateContext)) {
					acceptor.accept(createProposal(template, templateContext, context, getImage(template),
							getRelevance(template)));
				}
			}
		}
	}
	
	protected TemplateProposal createProposalWithAction(Template template, TemplateContext templateContext,
			ContentAssistContext context, Image image, int relevance) {
		if (!validate(template, context))
			return null;
		IProject project = getIProject(context.getCurrentModel());
		if (project != null && project.exists()) {
			return new PxGenTemplateProposal(project, template, templateContext, context.getReplaceRegion(), image, relevance);
		} else {
			return doCreateProposal(template, templateContext, context, image, relevance);
		}
	}
	
	@Override
	public int getRelevance(Template template) {
		return super.getRelevance(template);
	}

	private IProject getIProject(EObject eObject) {
		IProject project = null;
		if (eObject != null) {
			if (eObject.eIsProxy()) {
				
			}
			IResource r = getIResource(eObject.eResource());
			if (r != null && r.exists()) {
				project = r.getProject();
			}
		}
		return project;
	}

	private IResource getIResource(Resource pEmfResource) {
		if (pEmfResource != null) {
			IWorkspaceRoot lWR = ResourcesPlugin.getWorkspace().getRoot();
			String path = pEmfResource.getURI().toPlatformString(true);
			if (path == null && pEmfResource.getURI().isFile()) {
				String fileURI = pEmfResource.getURI().toFileString().replaceAll("\\\\", "/");
				if (fileURI.indexOf(lWR.getLocation().toPortableString()) > -1)
					path = fileURI.substring(lWR.getLocation().toPortableString().length());
			}
			return lWR.findMember(new Path(path));
		}
		return null;
	}
}
