package org.pragmaticmodeling.pxgen.ui.templates;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.persistence.TemplatePersistenceData;
import org.eclipse.jface.text.templates.persistence.TemplateReaderWriter;
import org.eclipse.jface.text.templates.persistence.TemplateStore;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.Constants;
import org.osgi.framework.Bundle;
import org.pragmaticmodeling.pxgen.runtime.IPxGeneratorsExtensionManager;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

/**
 * @author Sven Efftinge - Initial contribution and API
 * 
 */
@Singleton
public class PxGenTemplateStore extends TemplateStore {

	@Inject
	private IPxGeneratorsExtensionManager extensionsManager;
	
	private static final String TEMPLATES = "templates/templates.xml";
	private final static Logger logger = Logger.getLogger(PxGenTemplateStore.class);
	private List<URL> resources;
	private AbstractUIPlugin plugin;

	@Inject
	public PxGenTemplateStore(ContextTypeRegistry registry, IPreferenceStore store,
			@Named(Constants.LANGUAGE_NAME) String key, AbstractUIPlugin plugin) {
		super(registry, store, key + ".templates");
		this.plugin = plugin;
	}

	protected URL getTemplateFileURL(AbstractUIPlugin plugin) {
		return plugin.getBundle().getEntry(TEMPLATES);
	}

	@Override
	protected void loadContributedTemplates() throws IOException {
		resources = new ArrayList<URL>();
		URL res = getTemplateFileURL(plugin);
		if (res != null)
			resources.add(res);
		for (String bundleId : extensionsManager.getGeneratorFragmentPlugins()) {
			Bundle bundle = Platform.getBundle(bundleId);
			if (bundle != null) {
				URL url = bundle.getEntry(TEMPLATES);
				if (url != null) {
					resources.add(url);
				}
			}
		}
		if (resources.isEmpty())
			return;
		TemplateReaderWriter reader = new TemplateReaderWriter();
		InputStream openStream = null;
		for (URL r : resources) {
			try {
				openStream = r.openStream();
				try {
					TemplatePersistenceData[] read = reader.read(openStream, null);
					for (TemplatePersistenceData templatePersistenceData : read) {
						internalAdd(templatePersistenceData);
					}
				} finally {
					openStream.close();
				}
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}

		}
	}

	@Override
	protected void handleException(IOException x) {
		logger.error(x.getMessage(), x);
	}

	public Template[] getTemplates(String id, String parentQualifiedName) {
		String match = id + "@" + parentQualifiedName;
		List<Template> templates= new ArrayList<>();
		for (Template t : getTemplates()) {
			if (t.getContextTypeId().startsWith(match)) {
				templates.add(t);
			}
		}
		return templates.toArray(new Template[templates.size()]);
	}
}
