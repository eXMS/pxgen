package org.pragmaticmodeling.pxgen.ui.templates;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.jface.text.templates.persistence.TemplatePersistenceData;
import org.eclipse.jface.text.templates.persistence.TemplateStore;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.ui.codetemplates.ui.preferences.AdvancedTemplatesPreferencePage;
import org.eclipse.xtext.ui.codetemplates.ui.preferences.TemplateResourceProvider;
import org.eclipse.xtext.ui.codetemplates.ui.preferences.TemplatesLanguageConfiguration;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditor;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditorModelAccess;

import com.google.inject.Inject;

public class PxGenTemplatesPreferencePage extends AdvancedTemplatesPreferencePage {

	@Inject
	private TemplatesLanguageConfiguration configuration;
	
	@Inject
	private IGrammarAccess grammarAccess;
	
	@Inject
	private TemplateResourceProvider resourceProvider;
	
	private EmbeddedEditorModelAccess partialEditor;
	
	@Inject
	public PxGenTemplatesPreferencePage(IPreferenceStore preferenceStore, ContextTypeRegistry registry,
			TemplateStore templateStore) {
		super(preferenceStore, registry, templateStore);
	}

	@Override
	protected void updateViewerInput() {
		IStructuredSelection selection= (IStructuredSelection) getTableViewer().getSelection();
		if (selection.size() == 1) {
			TemplatePersistenceData data= (TemplatePersistenceData) selection.getFirstElement();
			Template template= data.getTemplate();
			String name = template.getName();
			int suffixIndex = template.getContextTypeId().indexOf("@");
			String id = template.getContextTypeId(); 
			if (suffixIndex > -1) {
				id = id.substring(0, suffixIndex);
			}
			TemplateContextType contextType = getContextTypeRegistry().getContextType(id);
			String prefix = 
				"templates for " + grammarAccess.getGrammar().getName() +
				"'" + name + "'" + " for " + getContextTypeForGrammar(contextType) + ">>";
			String editablePart = template.getPattern();
			String suffix = "";
			partialEditor.updateModel(prefix, editablePart, suffix);
		} else {
			partialEditor.updateModel("", "", "");
		}
	}
	
	@Override
	protected SourceViewer createViewer(Composite parent) {
		EmbeddedEditor handle = configuration.getEmbeddedEditorFactory().newEditor(resourceProvider).readOnly().withParent(parent);
		partialEditor = handle.createPartialEditor(true);
		return handle.getViewer();
	}
}
