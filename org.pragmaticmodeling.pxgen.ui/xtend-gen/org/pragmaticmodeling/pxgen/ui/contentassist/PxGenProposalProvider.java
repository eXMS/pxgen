/**
 * generated by Xtext 2.11.0
 */
package org.pragmaticmodeling.pxgen.ui.contentassist;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.common.types.JvmAnnotationReference;
import org.eclipse.xtext.common.types.JvmFeature;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.pragmaticmodeling.pxgen.pxGen.File;
import org.pragmaticmodeling.pxgen.pxGen.FragmentEnablement;
import org.pragmaticmodeling.pxgen.pxGen.FragmentProperty;
import org.pragmaticmodeling.pxgen.pxGen.GenConf;
import org.pragmaticmodeling.pxgen.pxGen.GenerateInstruction;
import org.pragmaticmodeling.pxgen.pxGen.GenerationDirectory;
import org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition;
import org.pragmaticmodeling.pxgen.pxGen.ParameterDeclaration;
import org.pragmaticmodeling.pxgen.pxGen.Project;
import org.pragmaticmodeling.pxgen.runtime.IPxGeneratorsExtensionManager;
import org.pragmaticmodeling.pxgen.runtime.PxGeneratorsExtensionManager;
import org.pragmaticmodeling.pxgen.ui.contentassist.AbstractPxGenProposalProvider;
import org.pragmaticmodeling.pxgen.util.PxGenHelper;

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
@SuppressWarnings("all")
public class PxGenProposalProvider extends AbstractPxGenProposalProvider {
  @Override
  public void completeGenerateFileInstruction_File(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    GenerateInstruction instruction = ((GenerateInstruction) model);
    List<File> files = CollectionLiterals.<File>newArrayList();
    Project project = instruction.getProject();
    if (((project != null) && (!project.eIsProxy()))) {
      Iterable<File> _filter = Iterables.<File>filter(project.getElements(), File.class);
      Iterables.<File>addAll(files, _filter);
    }
    GeneratorDefinition parentGenerator = EcoreUtil2.<GeneratorDefinition>getContainerOfType(model, GeneratorDefinition.class);
    List<File> _list = IteratorExtensions.<File>toList(Iterators.<File>filter(parentGenerator.eAllContents(), File.class));
    Iterables.<File>addAll(files, _list);
    List<GeneratorDefinition> _allExtendedDefinitions = PxGenHelper.allExtendedDefinitions(parentGenerator);
    for (final GeneratorDefinition p : _allExtendedDefinitions) {
      List<File> _list_1 = IteratorExtensions.<File>toList(Iterators.<File>filter(p.eAllContents(), File.class));
      Iterables.<File>addAll(files, _list_1);
    }
    final Function1<File, String> _function = (File it) -> {
      return it.getName();
    };
    files = IterableExtensions.<File, String>sortBy(files, _function);
    for (final File file : files) {
      String _name = file.getName();
      String _name_1 = file.getName();
      StyledString _styledString = new StyledString(_name_1);
      acceptor.accept(this.createCompletionProposal(_name, _styledString, null, context));
    }
  }
  
  @Override
  public void completeGenerateFileInstruction_Dir(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    GenerateInstruction instruction = ((GenerateInstruction) model);
    Project project = instruction.getProject();
    if (((project != null) && (!project.eIsProxy()))) {
      final Function1<GenerationDirectory, String> _function = (GenerationDirectory it) -> {
        return it.getName();
      };
      List<GenerationDirectory> folders = IterableExtensions.<GenerationDirectory, String>sortBy(Iterables.<GenerationDirectory>filter(project.getElements(), GenerationDirectory.class), _function);
      for (final GenerationDirectory folder : folders) {
        String _name = folder.getName();
        String _name_1 = folder.getName();
        StyledString _styledString = new StyledString(_name_1);
        acceptor.accept(this.createCompletionProposal(_name, _styledString, null, context));
      }
    } else {
      super.completeGenerateFileInstruction_Dir(model, assignment, context, acceptor);
    }
  }
  
  @Override
  public void completeFragmentEnablement_Extender(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    EObject parentExtender = EcoreUtil2.<FragmentEnablement>getContainerOfType(model.eContainer(), FragmentEnablement.class);
    JvmTypeReference parentDefinition = null;
    if ((parentExtender == null)) {
      parentExtender = EcoreUtil2.<GenConf>getContainerOfType(model.eContainer(), GenConf.class);
      if ((parentExtender != null)) {
        parentDefinition = ((GenConf) parentExtender).getGenerator();
      }
    } else {
      parentDefinition = ((FragmentEnablement) parentExtender).getExtender();
    }
    if (((parentDefinition != null) && (!parentDefinition.eIsProxy()))) {
      final String parentDefClass = parentDefinition.getQualifiedName();
      IPxGeneratorsExtensionManager fragmentsManager = new PxGeneratorsExtensionManager();
      Set<String> _fragmentsExtending = fragmentsManager.getFragmentsExtending(parentDefClass);
      for (final String plugin : _fragmentsExtending) {
        {
          StyledString _styledString = new StyledString(plugin);
          ICompletionProposal proposal = this.createCompletionProposal((plugin + " {}"), _styledString, null, context);
          if ((proposal instanceof ConfigurableCompletionProposal)) {
            int _priority = ((ConfigurableCompletionProposal)proposal).getPriority();
            int _plus = (_priority + 1000);
            ((ConfigurableCompletionProposal)proposal).setPriority(_plus);
            int _length = plugin.length();
            int _plus_1 = (_length + 2);
            ((ConfigurableCompletionProposal)proposal).setCursorPosition(_plus_1);
          }
          acceptor.accept(proposal);
        }
      }
    } else {
      super.completeFragmentEnablement_Extender(model, assignment, context, acceptor);
    }
  }
  
  public boolean isExtensionFor(final IEObjectDescription description, final GeneratorDefinition parent) {
    return true;
  }
  
  @Override
  public void completeFragmentProperty_Name(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    if ((model instanceof GenConf)) {
      JvmTypeReference generator = ((GenConf)model).getGenerator();
      GeneratorDefinition gendef = PxGenHelper.getGeneratorDefinition(generator);
      if ((gendef != null)) {
        Iterable<ParameterDeclaration> vars = Iterables.<ParameterDeclaration>filter(gendef.getElements(), ParameterDeclaration.class);
        boolean _isEmpty = IterableExtensions.isEmpty(vars);
        boolean _not = (!_isEmpty);
        if (_not) {
          for (final ParameterDeclaration v : vars) {
            String _name = v.getName();
            String _name_1 = v.getName();
            StyledString _styledString = new StyledString(_name_1);
            acceptor.accept(this.createCompletionProposal(_name, _styledString, null, context));
          }
        }
      }
    } else {
      if ((model instanceof FragmentEnablement)) {
        JvmType _type = ((FragmentEnablement)model).getExtender().getType();
        JvmGenericType type = ((JvmGenericType) _type);
        Iterable<String> vars_1 = this.visibleParameters(type);
        boolean _isEmpty_1 = IterableExtensions.isEmpty(vars_1);
        boolean _not_1 = (!_isEmpty_1);
        if (_not_1) {
          for (final String v_1 : vars_1) {
            StyledString _styledString_1 = new StyledString(v_1);
            acceptor.accept(this.createCompletionProposal(v_1, _styledString_1, null, context));
          }
        }
      } else {
        if ((model instanceof FragmentProperty)) {
          FragmentProperty prop = ((FragmentProperty) model);
          EObject _eContainer = prop.eContainer();
          FragmentEnablement extender = ((FragmentEnablement) _eContainer);
          Iterable<ParameterDeclaration> vars_2 = Iterables.<ParameterDeclaration>filter(PxGenHelper.getGeneratorDefinition(extender.getExtender()).getElements(), ParameterDeclaration.class);
          boolean _isEmpty_2 = IterableExtensions.isEmpty(vars_2);
          boolean _not_2 = (!_isEmpty_2);
          if (_not_2) {
            for (final ParameterDeclaration v_2 : vars_2) {
              String _name_2 = v_2.getName();
              String _name_3 = v_2.getName();
              StyledString _styledString_2 = new StyledString(_name_3);
              acceptor.accept(this.createCompletionProposal(_name_2, _styledString_2, null, context));
            }
          }
        } else {
          super.completeFragmentProperty_Name(model, assignment, context, acceptor);
        }
      }
    }
  }
  
  protected Iterable<String> visibleParameters(final JvmGenericType type) {
    Set<String> _xblockexpression = null;
    {
      final Function1<JvmFeature, Boolean> _function = (JvmFeature f) -> {
        return this.isParameter(f);
      };
      Iterable<JvmFeature> jvmFeatures = IterableExtensions.<JvmFeature>filter(type.getAllFeatures(), _function);
      Set<String> paramNames = new HashSet<String>();
      for (final JvmFeature f : jvmFeatures) {
        paramNames.add(this.toFieldName(f.getSimpleName()));
      }
      _xblockexpression = paramNames;
    }
    return _xblockexpression;
  }
  
  public String toFieldName(final String string) {
    if (((string.startsWith("get") && (string.length() > 3)) && Character.isUpperCase(string.charAt(3)))) {
      return StringExtensions.toFirstLower(string.substring(3));
    }
    if (((string.startsWith("set") && (string.length() > 3)) && Character.isUpperCase(string.charAt(3)))) {
      return StringExtensions.toFirstLower(string.substring(3));
    }
    return string;
  }
  
  public Boolean isParameter(final JvmFeature feature) {
    EList<JvmAnnotationReference> _annotations = feature.getAnnotations();
    for (final JvmAnnotationReference a : _annotations) {
      String _qualifiedName = a.getAnnotation().getQualifiedName();
      boolean _equals = Objects.equal("org.pragmaticmodeling.pxgen.runtime.PxGenParameter", _qualifiedName);
      if (_equals) {
        return Boolean.valueOf(true);
      }
    }
    return Boolean.valueOf(false);
  }
}
