package org.pragmaticmodeling.pxgen.ui.highlighting;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EnumRule;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.ide.editor.syntaxcoloring.ISemanticHighlightingCalculator;
import org.eclipse.xtext.nodemodel.BidiTreeIterable;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.xbase.ide.highlighting.XbaseHighlightingCalculator;
import org.pragmaticmodeling.pxgen.ui.highlighting.PxGenHighlightingConfiguration;

@SuppressWarnings("all")
public class PxGenHighlightingCalculator extends XbaseHighlightingCalculator implements ISemanticHighlightingCalculator {
  private static List<String> pxgenKeywords = null;
  
  private static List<String> pxgenKeywordFeatures = null;
  
  public PxGenHighlightingCalculator() {
    super();
    ArrayList<String> _arrayList = new ArrayList<String>();
    PxGenHighlightingCalculator.pxgenKeywords = _arrayList;
    PxGenHighlightingCalculator.pxgenKeywords.add("generatorConf");
    PxGenHighlightingCalculator.pxgenKeywords.add("enable");
    PxGenHighlightingCalculator.pxgenKeywords.add("file");
    PxGenHighlightingCalculator.pxgenKeywords.add("project");
    PxGenHighlightingCalculator.pxgenKeywords.add("generatorDef");
    PxGenHighlightingCalculator.pxgenKeywords.add("extends");
    PxGenHighlightingCalculator.pxgenKeywords.add("param");
    PxGenHighlightingCalculator.pxgenKeywords.add("run");
    PxGenHighlightingCalculator.pxgenKeywords.add("generate-file");
    PxGenHighlightingCalculator.pxgenKeywords.add("generate-java");
    PxGenHighlightingCalculator.pxgenKeywords.add("source");
    PxGenHighlightingCalculator.pxgenKeywords.add("dir");
    ArrayList<String> _arrayList_1 = new ArrayList<String>();
    PxGenHighlightingCalculator.pxgenKeywordFeatures = _arrayList_1;
    PxGenHighlightingCalculator.pxgenKeywordFeatures.add("enabled:");
    PxGenHighlightingCalculator.pxgenKeywordFeatures.add("path:");
    PxGenHighlightingCalculator.pxgenKeywordFeatures.add("name:");
    PxGenHighlightingCalculator.pxgenKeywordFeatures.add("genDir:");
    PxGenHighlightingCalculator.pxgenKeywordFeatures.add("to:");
    PxGenHighlightingCalculator.pxgenKeywordFeatures.add("overwrite:");
  }
  
  @Override
  public void provideHighlightingFor(final XtextResource resource, final IHighlightedPositionAcceptor acceptor, final CancelIndicator cancelIndicator) {
    super.provideHighlightingFor(resource, acceptor, cancelIndicator);
    if (((resource == null) || (resource.getParseResult() == null))) {
      return;
    }
    INode root = resource.getParseResult().getRootNode();
    BidiTreeIterable<INode> _asTreeIterable = root.getAsTreeIterable();
    for (final INode node : _asTreeIterable) {
      {
        EObject grammarElement = node.getGrammarElement();
        if (((grammarElement instanceof RuleCall) && (((RuleCall) grammarElement).getRule() instanceof EnumRule))) {
          acceptor.addPosition(node.getOffset(), node.getLength(), 
            PxGenHighlightingConfiguration.PXGEN_ENUM_ID);
        } else {
          boolean _contains = PxGenHighlightingCalculator.pxgenKeywords.contains(node.getText());
          if (_contains) {
            acceptor.addPosition(node.getOffset(), node.getLength(), 
              PxGenHighlightingConfiguration.PXGEN_KEYWORD_ID);
          } else {
            boolean _contains_1 = PxGenHighlightingCalculator.pxgenKeywordFeatures.contains(node.getText());
            if (_contains_1) {
              acceptor.addPosition(node.getOffset(), node.getLength(), 
                PxGenHighlightingConfiguration.PXGEN_KEYWORD_FEATURE_ID);
            }
          }
        }
      }
    }
  }
}
