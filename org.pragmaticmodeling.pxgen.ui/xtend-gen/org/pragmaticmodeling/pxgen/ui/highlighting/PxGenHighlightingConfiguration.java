package org.pragmaticmodeling.pxgen.ui.highlighting;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor;
import org.eclipse.xtext.ui.editor.utils.TextStyle;
import org.pragmaticmodeling.pxgen.ui.highlighting.PxGenHighlightingHelper;

@SuppressWarnings("all")
public class PxGenHighlightingConfiguration extends DefaultHighlightingConfiguration {
  public final static String PXGEN_KEYWORD_ID = "pxgen.keyword";
  
  public final static String PXGEN_KEYWORD_FEATURE_ID = "pxdoc.keyword.feature";
  
  public final static String PXGEN_ENUM_ID = "pxdoc.enum";
  
  public final static String STATIC_METHOD_INVOCATION = "xbase.static.method.invocation";
  
  public final static String STATIC_FIELD = "xbase.static.field";
  
  public final static String FIELD = "xbase.field";
  
  public final static String ANNOTATION = "xbase.annotation";
  
  public final static String EXTENSION_METHOD_INVOCATION = "xbase.extension.method.invacation";
  
  public final static String DEPRECATED_MEMBERS = "xbase.deprecated.members";
  
  @Override
  public void configure(final IHighlightingConfigurationAcceptor acceptor) {
    acceptor.acceptDefaultHighlighting(PxGenHighlightingConfiguration.STATIC_METHOD_INVOCATION, "Static method invocation", 
      this.staticMethodInvocation());
    acceptor.acceptDefaultHighlighting(PxGenHighlightingConfiguration.STATIC_FIELD, "Static Field", this.staticField());
    acceptor.acceptDefaultHighlighting(PxGenHighlightingConfiguration.FIELD, "Field", this.field());
    acceptor.acceptDefaultHighlighting(PxGenHighlightingConfiguration.ANNOTATION, "Annotation", this.annotation());
    acceptor.acceptDefaultHighlighting(PxGenHighlightingConfiguration.EXTENSION_METHOD_INVOCATION, "Extension method invocation", 
      this.extensionMethodInvocation());
    acceptor.acceptDefaultHighlighting(PxGenHighlightingConfiguration.DEPRECATED_MEMBERS, "Deprecated members", this.deprecatedMembers());
    acceptor.acceptDefaultHighlighting(PxGenHighlightingConfiguration.PXGEN_KEYWORD_ID, "pxGen Keywords", this.pxgenKeywordTextStyle());
    acceptor.acceptDefaultHighlighting(PxGenHighlightingConfiguration.PXGEN_KEYWORD_FEATURE_ID, "pxGen Keywords Features", 
      this.pxgenKeywordFeatureTextStyle());
    acceptor.acceptDefaultHighlighting(PxGenHighlightingConfiguration.PXGEN_ENUM_ID, "Enumeration values", this.pxgenEnumTextStyle());
    super.configure(acceptor);
  }
  
  public TextStyle staticMethodInvocation() {
    TextStyle textStyle = this.defaultTextStyle().copy();
    textStyle.setStyle(SWT.ITALIC);
    return textStyle;
  }
  
  public TextStyle staticField() {
    TextStyle textStyle = this.defaultTextStyle().copy();
    textStyle.setStyle(SWT.ITALIC);
    RGB _rGB = new RGB(0, 0, 192);
    textStyle.setColor(_rGB);
    return textStyle;
  }
  
  public TextStyle field() {
    TextStyle textStyle = this.defaultTextStyle().copy();
    RGB _rGB = new RGB(0, 26, 171);
    textStyle.setColor(_rGB);
    return textStyle;
  }
  
  public TextStyle annotation() {
    TextStyle textStyle = this.defaultTextStyle().copy();
    RGB _rGB = new RGB(100, 100, 100);
    textStyle.setColor(_rGB);
    return textStyle;
  }
  
  public TextStyle extensionMethodInvocation() {
    TextStyle textStyle = this.defaultTextStyle().copy();
    RGB _rGB = new RGB(171, 48, 0);
    textStyle.setColor(_rGB);
    return textStyle;
  }
  
  public TextStyle deprecatedMembers() {
    TextStyle textStyle = this.defaultTextStyle().copy();
    textStyle.setStyle(TextAttribute.STRIKETHROUGH);
    return textStyle;
  }
  
  public TextStyle pxgenKeywordTextStyle() {
    TextStyle textStyle = new TextStyle();
    RGB _rGB = new RGB(105, 0, 210);
    textStyle.setColor(_rGB);
    textStyle.setStyle(SWT.BOLD);
    return textStyle;
  }
  
  public TextStyle pxgenEnumTextStyle() {
    TextStyle textStyle = new TextStyle();
    RGB _rGB = new RGB(0, 0, 192);
    textStyle.setColor(_rGB);
    textStyle.setStyle(PxGenHighlightingHelper.italicAndBold());
    return textStyle;
  }
  
  public TextStyle pxgenStyleTextStyle() {
    TextStyle textStyle = new TextStyle();
    textStyle.setStyle(SWT.ITALIC);
    return textStyle;
  }
  
  public TextStyle pxgenKeywordFeatureTextStyle() {
    TextStyle textStyle = new TextStyle();
    RGB _rGB = new RGB(0, 164, 242);
    textStyle.setColor(_rGB);
    textStyle.setStyle(SWT.ITALIC);
    return textStyle;
  }
}
