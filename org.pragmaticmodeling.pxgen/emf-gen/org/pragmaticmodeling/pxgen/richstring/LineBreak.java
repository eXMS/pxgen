/**
 */
package org.pragmaticmodeling.pxgen.richstring;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Line Break</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.pragmaticmodeling.pxgen.richstring.ProcessedRichStringPackage#getLineBreak()
 * @model
 * @generated
 */
public interface LineBreak extends Literal {
} // LineBreak
