/**
 */
package org.pragmaticmodeling.pxgen.richstring.impl;

import org.eclipse.emf.ecore.EClass;

import org.pragmaticmodeling.pxgen.richstring.LineBreak;
import org.pragmaticmodeling.pxgen.richstring.ProcessedRichStringPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Line Break</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LineBreakImpl extends LiteralImpl implements LineBreak {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LineBreakImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProcessedRichStringPackage.Literals.LINE_BREAK;
	}

} //LineBreakImpl
