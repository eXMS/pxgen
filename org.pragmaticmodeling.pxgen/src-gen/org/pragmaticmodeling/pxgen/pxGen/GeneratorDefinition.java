/**
 * generated by Xtext 2.11.0
 */
package org.pragmaticmodeling.pxgen.pxGen;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generator Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition#getName <em>Name</em>}</li>
 *   <li>{@link org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition#getExtended <em>Extended</em>}</li>
 * </ul>
 *
 * @see org.pragmaticmodeling.pxgen.pxGen.PxGenPackage#getGeneratorDefinition()
 * @model
 * @generated
 */
public interface GeneratorDefinition extends PxGenElement
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.pragmaticmodeling.pxgen.pxGen.PxGenPackage#getGeneratorDefinition_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Extended</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Extended</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Extended</em>' reference.
   * @see #setExtended(GeneratorDefinition)
   * @see org.pragmaticmodeling.pxgen.pxGen.PxGenPackage#getGeneratorDefinition_Extended()
   * @model
   * @generated
   */
  GeneratorDefinition getExtended();

  /**
   * Sets the value of the '{@link org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition#getExtended <em>Extended</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Extended</em>' reference.
   * @see #getExtended()
   * @generated
   */
  void setExtended(GeneratorDefinition value);

} // GeneratorDefinition
