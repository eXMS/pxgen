/**
 * generated by Xtext 2.11.0
 */
package org.pragmaticmodeling.pxgen.pxGen;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.xtext.xbase.XExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.pragmaticmodeling.pxgen.pxGen.PxGenElement#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see org.pragmaticmodeling.pxgen.pxGen.PxGenPackage#getPxGenElement()
 * @model
 * @generated
 */
public interface PxGenElement extends EObject
{
  /**
   * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.xtext.xbase.XExpression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elements</em>' containment reference list.
   * @see org.pragmaticmodeling.pxgen.pxGen.PxGenPackage#getPxGenElement_Elements()
   * @model containment="true"
   * @generated
   */
  EList<XExpression> getElements();

} // PxGenElement
