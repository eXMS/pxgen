/*******************************************************************************
 * Copyright (c) 2011 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.pragmaticmodeling.pxgen.jvmmodel;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractValueConverter;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.xbase.scoping.featurecalls.OperatorMapping;
import org.pragmaticmodeling.pxgen.services.PxGenGrammarAccess;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @author Sebastian Zarnekow - Initial contribution and API
 */
@Singleton
public class FunctionIDValueConverter extends AbstractValueConverter<String> {

	@Inject
	private OperatorMapping operatorMapping;
	
	private IValueConverter<Object> delegate;

	private PxGenGrammarAccess grammarAccess;

	private PxGenValueConverterService service;

	@Inject
	protected void setDelegate(PxGenValueConverterService service, PxGenGrammarAccess grammarAccess) {
		this.service = service;
		this.grammarAccess = grammarAccess;
	}
	
	@Override
	public String toValue(String string, INode node) throws ValueConverterException {
		QualifiedName operatorMethodName = operatorMapping.getMethodName(QualifiedName.create(string));
		if (operatorMethodName != null) {
			return operatorMethodName.getFirstSegment();
		}
		return (String) getDelegate().toValue(string, node);
	}

	private IValueConverter<Object> getDelegate() {
		if (delegate != null)
			return delegate;
		return delegate = service.getConverter(grammarAccess.getValidIDRule().getName());
	}

	@Override
	public String toString(String value) throws ValueConverterException {
		QualifiedName operatorName = operatorMapping.getOperator(QualifiedName.create(value));
		if (operatorName != null) {
			return operatorName.getFirstSegment();
		}
		return getDelegate().toString(value);
	}
	
}
