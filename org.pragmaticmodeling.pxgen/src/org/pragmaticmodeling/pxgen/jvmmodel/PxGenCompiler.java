/*******************************************************************************
 * Copyright (c) 2011 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.pragmaticmodeling.pxgen.jvmmodel;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend.core.compiler.UnicodeAwarePostProcessor;
import org.eclipse.xtend.core.compiler.XtendGenerator;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtend2.lib.StringConcatenationClient;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.generator.trace.LocationData;
import org.eclipse.xtext.util.ITextRegionWithLineInformation;
import org.eclipse.xtext.util.Strings;
import org.eclipse.xtext.xbase.XClosure;
import org.eclipse.xtext.xbase.XExpression;
import org.eclipse.xtext.xbase.XFeatureCall;
import org.eclipse.xtext.xbase.XNullLiteral;
import org.eclipse.xtext.xbase.XStringLiteral;
import org.eclipse.xtext.xbase.XbasePackage;
import org.eclipse.xtext.xbase.compiler.IGeneratorConfigProvider;
import org.eclipse.xtext.xbase.compiler.XbaseCompiler;
import org.eclipse.xtext.xbase.compiler.output.ITreeAppendable;
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociations;
import org.eclipse.xtext.xbase.lib.Pair;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.eclipse.xtext.xbase.typesystem.references.LightweightTypeReference;
import org.pragmaticmodeling.pxgen.pxGen.File;
import org.pragmaticmodeling.pxgen.pxGen.GenerateInstruction;
import org.pragmaticmodeling.pxgen.pxGen.GenerationDirectory;
import org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition;
import org.pragmaticmodeling.pxgen.pxGen.Project;
import org.pragmaticmodeling.pxgen.pxGen.RichString;
import org.pragmaticmodeling.pxgen.pxGen.RichStringForLoop;
import org.pragmaticmodeling.pxgen.pxGen.RichStringIf;
import org.pragmaticmodeling.pxgen.pxGen.RichStringLiteral;
import org.pragmaticmodeling.pxgen.pxGen.StringExpression;
import org.pragmaticmodeling.pxgen.pxGen.XtextGenerator;
import org.pragmaticmodeling.pxgen.richstring.AbstractRichStringPartAcceptor;
import org.pragmaticmodeling.pxgen.richstring.DefaultIndentationHandler;
import org.pragmaticmodeling.pxgen.richstring.RichStringProcessor;
import org.pragmaticmodeling.pxgen.util.PxGenHelper;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * @author Sven Efftinge - Initial contribution and API
 * @author Jan Koehnlein
 * @author Sebastian Zarnekow
 */
public class PxGenCompiler extends XbaseCompiler {

	@Inject
	private RichStringProcessor richStringProcessor;

	@Inject
	private Provider<DefaultIndentationHandler> indentationHandler;

	@Inject
	private IGeneratorConfigProvider generatorConfigProvider;

	@Inject
	private IJvmModelAssociations associations;

	private IGenerator jvmModelGenerator;

	@Override
	protected String getFavoriteVariableName(EObject ex) {
		if (ex instanceof RichStringForLoop)
			return "hasAnyElements";
		return super.getFavoriteVariableName(ex);
	}

	public class RichStringPrepareCompiler extends AbstractRichStringPartAcceptor.ForLoopOnce {

		private final LinkedList<ITreeAppendable> appendableStack;
		private final LinkedList<RichStringIf> ifStack;
		private final LinkedList<RichStringForLoop> forStack;
		private final String variableName;
		private ITreeAppendable appendable;
		private ITreeAppendable currentAppendable;

		public RichStringPrepareCompiler(ITreeAppendable appendable, String variableName, RichString richString) {
			this.ifStack = Lists.newLinkedList();
			this.forStack = Lists.newLinkedList();
			this.appendableStack = Lists.newLinkedList();
			this.appendable = appendable;
			this.variableName = variableName;
			List<XExpression> expressions = richString.getExpressions();
			if (!expressions.isEmpty() && expressions.get(0) instanceof RichStringLiteral)
				setCurrentAppendable((RichStringLiteral) expressions.get(0));
		}

		@Override
		public void acceptSemanticLineBreak(int charCount, RichStringLiteral origin, boolean controlStructureSeen) {
			setCurrentAppendable(origin);
			currentAppendable.newLine();
			currentAppendable.append(variableName);
			if (!controlStructureSeen) {
				currentAppendable.append(".newLine();");
			} else {
				currentAppendable.append(".newLineIfNotEmpty();");
			}
		}

		protected void setCurrentAppendable(/* @Nullable */ RichStringLiteral origin) {
			if (currentAppendable == null && origin != null) {
				ITextRegionWithLineInformation region = (ITextRegionWithLineInformation) getLocationInFileProvider()
						.getSignificantTextRegion(origin, XbasePackage.Literals.XSTRING_LITERAL__VALUE, 0);
				currentAppendable = appendable.trace(new LocationData(region, null), true);
			}
		}

		@Override
		public void acceptTemplateLineBreak(int charCount, RichStringLiteral origin) {
			setCurrentAppendable(origin);
		}

		@Override
		public void acceptSemanticText(CharSequence text, /* @Nullable */ RichStringLiteral origin) {
			setCurrentAppendable(origin);
			if (text.length() == 0)
				return;
			currentAppendable.newLine();
			currentAppendable.append(variableName);
			currentAppendable.append(".append(\"");
			currentAppendable.append(Strings.convertToJavaString(text.toString(), false));
			currentAppendable.append("\");");
		}

		@Override
		public void acceptIfCondition(XExpression condition) {
			currentAppendable = null;
			ifStack.add((RichStringIf) condition.eContainer());
			appendable.newLine();
			pushAppendable(condition.eContainer());
			appendable.append("{").increaseIndentation();
			writeIf(condition);
		}

		protected void pushAppendable(EObject traceInfo) {
			appendableStack.add(appendable);
			appendable = appendable.trace(traceInfo);
		}

		protected void popAppendable() {
			appendable = appendableStack.removeLast();
		}

		@Override
		public void acceptElseIfCondition(XExpression condition) {
			currentAppendable = null;
			writeElse();
			writeIf(condition);
		}

		protected void writeIf(XExpression condition) {
			ITreeAppendable debugAppendable = appendable.trace(condition.eContainer(), true);
			internalToJavaStatement(condition, debugAppendable, true);
			debugAppendable.newLine();
			debugAppendable.append("if (");
			internalToJavaExpression(condition, debugAppendable);
			debugAppendable.append(") {").increaseIndentation();
		}

		protected void writeElse() {
			currentAppendable = null;
			appendable.decreaseIndentation();
			appendable.newLine();
			appendable.append("} else {");
			appendable.increaseIndentation();
		}

		@Override
		public void acceptElse() {
			currentAppendable = null;
			writeElse();
		}

		@Override
		public void acceptEndIf() {
			currentAppendable = null;
			RichStringIf richStringIf = ifStack.removeLast();
			for (int i = 0; i < richStringIf.getElseIfs().size() + 2; i++) {
				appendable.decreaseIndentation();
				appendable.newLine();
				appendable.append("}");
			}
			popAppendable();
		}

		@Override
		public void acceptForLoop(JvmFormalParameter parameter, /* @Nullable */ XExpression expression) {
			currentAppendable = null;
			super.acceptForLoop(parameter, expression);
			if (expression == null)
				throw new IllegalArgumentException("expression may not be null");
			RichStringForLoop forLoop = (RichStringForLoop) expression.eContainer();
			forStack.add(forLoop);
			appendable.newLine();
			pushAppendable(forLoop);
			appendable.append("{").increaseIndentation();

			ITreeAppendable debugAppendable = appendable.trace(forLoop, true);
			internalToJavaStatement(expression, debugAppendable, true);
			String variableName = null;
			if (forLoop.getBefore() != null || forLoop.getSeparator() != null || forLoop.getAfter() != null) {
				variableName = debugAppendable.declareSyntheticVariable(forLoop, "_hasElements");
				debugAppendable.newLine();
				debugAppendable.append("boolean ");
				debugAppendable.append(variableName);
				debugAppendable.append(" = false;");
			}
			debugAppendable.newLine();
			debugAppendable.append("for(final ");
			// TODO tracing if parameter was explicitly declared
			LightweightTypeReference paramType = getLightweightType(parameter);
			if (paramType != null) {
				debugAppendable.append(paramType);
			} else {
				debugAppendable.append("Object");
			}
			debugAppendable.append(" ");
			String loopParam = debugAppendable.declareVariable(parameter, parameter.getName());
			debugAppendable.append(loopParam);
			debugAppendable.append(" : ");
			internalToJavaExpression(expression, debugAppendable);
			debugAppendable.append(") {").increaseIndentation();
		}

		@Override
		public boolean forLoopHasNext(/* @Nullable */ XExpression before, /* @Nullable */ XExpression separator,
				CharSequence indentation) {
			currentAppendable = null;
			if (!super.forLoopHasNext(before, separator, indentation))
				return false;
			RichStringForLoop forLoop = forStack.getLast();
			if (appendable.hasName(forLoop)) {
				String varName = getVarName(forLoop, appendable);
				appendable.newLine();
				appendable.append("if (!");
				appendable.append(varName);
				appendable.append(") {");
				appendable.increaseIndentation();
				appendable.newLine();
				appendable.append(varName);
				appendable.append(" = true;");
				if (before != null) {
					writeExpression(before, indentation, false);
				}
				appendable.decreaseIndentation();
				appendable.newLine();
				appendable.append("}");
				if (separator != null) {
					appendable.append(" else {");
					appendable.increaseIndentation();
					writeExpression(separator, indentation, true);
					appendable.decreaseIndentation();
					appendable.newLine();
					appendable.append("}");
				}
			}
			return true;
		}

		@Override
		public void acceptEndFor(/* @Nullable */ XExpression after, CharSequence indentation) {
			currentAppendable = null;
			super.acceptEndFor(after, indentation);
			appendable.decreaseIndentation();
			appendable.newLine();
			appendable.append("}");

			RichStringForLoop forLoop = forStack.removeLast();
			if (after != null) {
				String varName = getVarName(forLoop, appendable);
				appendable.newLine();
				appendable.append("if (");
				appendable.append(varName);
				appendable.append(") {");
				appendable.increaseIndentation();
				writeExpression(after, indentation, false);
				appendable.decreaseIndentation();
				appendable.newLine();
				appendable.append("}");
			}

			appendable.decreaseIndentation();
			appendable.newLine();
			appendable.append("}");
			popAppendable();
		}

		@Override
		public void acceptExpression(XExpression expression, CharSequence indentation) {
			currentAppendable = null;
			if (!isEmptyEmission(expression)) {
				writeExpression(expression, indentation, false);
			}
		}

		private boolean isEmptyEmission(XExpression expression) {
			return expression instanceof XStringLiteral
					&& StringExtensions.isNullOrEmpty(((XStringLiteral) expression).getValue())
					|| expression instanceof XNullLiteral;
		}

		protected void writeExpression(XExpression expression, CharSequence indentation, boolean immediate) {
			boolean referenced = !isPrimitiveVoid(expression);
			internalToJavaStatement(expression, appendable, referenced);
			if (referenced) {
				ITreeAppendable tracingAppendable = appendable.trace(expression, true);
				tracingAppendable.newLine();
				tracingAppendable.append(variableName);
				if (immediate)
					tracingAppendable.append(".appendImmediate(");
				else
					tracingAppendable.append(".append(");
				internalToJavaExpression(expression, tracingAppendable);

				String javaIndentation = Strings.convertToJavaString(indentation.toString(), false);
				if (immediate || !javaIndentation.isEmpty()) {
					tracingAppendable.append(", \"");
					tracingAppendable.append(javaIndentation);
					tracingAppendable.append("\"");
				}
				tracingAppendable.append(");");
			}
		}

	}

	@Override
	protected XExpression normalizeBlockExpression(XExpression expr) {
		if (expr instanceof RichString)
			return expr;
		return super.normalizeBlockExpression(expr);
	}

	@Override
	public void doInternalToJavaStatement(XExpression obj, ITreeAppendable appendable, boolean isReferenced) {
		if (obj instanceof RichString)
			_toJavaStatement((RichString) obj, appendable, isReferenced);
		else if (obj instanceof GenerateInstruction) {
			_toJavaStatement((GenerateInstruction) obj, appendable, isReferenced);
		} else if (obj instanceof GenerationDirectory) {
			_toJavaStatement((GenerateInstruction) obj, appendable, isReferenced);
		} else if (obj instanceof StringExpression) {
			_toJavaStatement((StringExpression) obj, appendable, isReferenced);
		} else
			super.doInternalToJavaStatement(obj, appendable, isReferenced);
	}
	
	public void _toJavaExpression(StringExpression expression, ITreeAppendable b) {
		toJavaExpression(expression.getBody(), b);
	}
	
	public void _toJavaStatement(StringExpression expression, ITreeAppendable b, boolean isReferenced) {

	}
	
	public void _toJavaStatement(GenerateInstruction genIns, ITreeAppendable b, boolean isReferenced) {
		File file = genIns.getFile();
		String genDir = genIns.getDir();
		StringExpression pathExpression = (StringExpression) genIns.getPath();
		XExpression path = pathExpression != null ? pathExpression.getBody() : null;
		Project project = genIns.getProject();
		String fileName = file.getName();
		EObject fileContext = PxGenHelper.parentContext(file);

		if (file != null && genDir != null && path != null) {
			b.newLine();
			b.append("// generate file ");
			b.append(fileName);
			b.newLine();
			b.append("get");
			b.append(PxGenHelper.getName(project));
			b.append("().get");
			b.append(toFirstUpper(genDir));
			b.append("()");
			b.append(".generateFile(");
			this.toJavaExpression(path, b);
			b.append(", new ");
			b.append(PxGenHelper.className(file));
			b.append("(");
			if (fileContext instanceof Project) {
				this.toJavaExpression((Project)fileContext, b);
			} else if (fileContext instanceof GeneratorDefinition) {
				b.append("this");
			}
			int nbParams = genIns.getParameters().size();
			for (XExpression p : genIns.getParameters()) {
				b.append(", ");
				this.toJavaExpression(p, b);
			}
			b.append(").getContent());");
		}
	}

	

	@Override
	protected void toJavaExpression(XStringLiteral literal, ITreeAppendable appendable, boolean useUnicodeEscapes) {
		LightweightTypeReference type = getLightweightType(literal);
		if (type != null && type.isType(Character.TYPE)) {
			String javaString = Strings.convertToJavaString(literal.getValue(), useUnicodeEscapes);
			appendable.append("'").append(javaString).append("'");
		} else if (type != null && type.isType(Character.class)) {
			String javaString = Strings.convertToJavaString(literal.getValue(), useUnicodeEscapes);
			appendable.append("Character.valueOf('").append(javaString).append("')");
		} else {
			String javaString = Strings.convertToJavaString(literal.getValue(), useUnicodeEscapes);
			appendable.append("\"").append(javaString).append("\"");
		}
	}

	public void _toJavaStatement(GenerationDirectory genDir, ITreeAppendable b, boolean isReferenced) {
		Project genProject = EcoreUtil2.getContainerOfType(genDir, Project.class);
		b.append("get");
		b.append(genProject.getName());
		b.append("().get");
		b.append(genDir.getName());
		b.append("()");
	}

	public void _toJavaExpression(GenerationDirectory genDir, ITreeAppendable b) {
		Project genProject = EcoreUtil2.getContainerOfType(genDir, Project.class);
		b.append("get");
		b.append(genProject.getName());
		b.append("().get");
		b.append(toFirstUpper(genDir.getName()));
		b.append("()");
	}

	public void _toJavaExpression(Project project, ITreeAppendable b) {
		b.append("get");
		b.append(project.getName());
		b.append("()");
	}

	private String toFirstUpper(String name) {
		if (name != null && name.length() > 1) {
			return name.substring(0, 1).toUpperCase() + name.substring(1);
		}
		return name;
	}

	public void _toJavaStatement(RichString richString, ITreeAppendable b, boolean isReferenced) {
		LightweightTypeReference actualType = getLightweightType(richString);
		b = b.trace(richString);
		if (actualType.isType(StringConcatenationClient.class)) {
			String resultVariableName = b.declareSyntheticVariable(richString, "_client");
			b.newLine();
			b.append(actualType);
			b.append(" ");
			b.append(resultVariableName);
			b.append(" = new ");
			b.append(actualType);
			b.append("() {");
			b.openScope();
			reassignThisInClosure(b, actualType.getType());
			b.increaseIndentation().newLine();
			b.append("@");
			b.append(Override.class);
			b.newLine().append("protected void appendTo(");
			b.append(StringConcatenationClient.TargetStringConcatenation.class);
			String variableName = b.declareSyntheticVariable(richString, "_builder");
			b.append(" ").append(variableName).append(") {");
			b.increaseIndentation();
			RichStringPrepareCompiler compiler = new RichStringPrepareCompiler(b, variableName, richString);
			richStringProcessor.process(richString, compiler, indentationHandler.get());
			b.closeScope();
			b.decreaseIndentation().newLine().append("}").decreaseIndentation().newLine().append("};");
		} else {
			// declare variable
			String variableName = b.declareSyntheticVariable(richString, "_builder");
			b.newLine();
			b.append(StringConcatenation.class);
			b.append(" ");
			b.append(variableName);
			b.append(" = new ");
			b.append(StringConcatenation.class);
			b.append("();");
			RichStringPrepareCompiler compiler = new RichStringPrepareCompiler(b, variableName, richString);
			richStringProcessor.process(richString, compiler, indentationHandler.get());
		}
	}

	@Override
	public void internalToConvertedExpression(XExpression obj, ITreeAppendable appendable) {
		if (obj instanceof RichString)
			_toJavaExpression((RichString) obj, appendable);
		else if (obj instanceof GenerationDirectory) {
			_toJavaExpression((GenerationDirectory) obj, appendable);
		} else if (obj instanceof Project) {
			_toJavaExpression((Project) obj, appendable);
		} else if (obj instanceof StringExpression) {
			_toJavaExpression((StringExpression) obj, appendable);
		} else
			super.internalToConvertedExpression(obj, appendable);
	}
	public void _toJavaExpression(RichString richString, ITreeAppendable b) {
		b.append(getVarName(richString, b));
		if (getLightweightType(richString).isType(String.class))
			b.append(".toString()");
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Specialized since unicode escapes are handled in the
	 * {@link UnicodeAwarePostProcessor}.
	 */
	@Override
	public void _toJavaExpression(XStringLiteral expr, ITreeAppendable b) {
		toJavaExpression(expr, b, false);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Specialized since unicode escapes are handled in the
	 * {@link UnicodeAwarePostProcessor}.
	 */
	@Override
	public void _toJavaStatement(final XStringLiteral expr, ITreeAppendable b, boolean isReferenced) {
		toJavaStatement(expr, b, isReferenced, false);
	}


	/**
	 * Symmetric to
	 * {@link XtendGenerator#reassignThisType(ITreeAppendable, JvmDeclaredType)}
	 */
	@Override
	protected void doReassignThisInClosure(ITreeAppendable b, JvmType prevType) {
		if (prevType instanceof JvmDeclaredType && ((JvmDeclaredType) prevType).isLocal()) {
			if (b.hasName(Pair.of("this", prevType))) {
				b.declareVariable(prevType, b.getName(Pair.of("this", prevType)));
			} else {
				b.declareSyntheticVariable(prevType, "");
			}
			if (b.hasObject("super")) {
				Object superElement = b.getObject("super");
				if (superElement instanceof JvmType) {
					String superVariable = b.getName(superElement);
					if ("super".equals(superVariable)) {
						b.declareSyntheticVariable(superElement, prevType.getSimpleName() + ".super");
					}
				}
			}
		} else {
			super.doReassignThisInClosure(b, prevType);
		}
	}

	@Override
	protected boolean needSyntheticSelfVariable(XClosure closure, LightweightTypeReference typeRef) {
		JvmType jvmType = typeRef.getType();
		TreeIterator<EObject> closureIterator = closure.eAllContents();
		while (closureIterator.hasNext()) {
			EObject obj1 = closureIterator.next();
			if (obj1 instanceof XClosure) {
				closureIterator.prune();
			} else if (obj1 instanceof XtextGenerator) {
				TreeIterator<EObject> typeIterator = obj1.eAllContents();
				while (typeIterator.hasNext()) {
					EObject obj2 = typeIterator.next();
					if (obj2 instanceof XClosure) {
						typeIterator.prune();
					} else if (obj2 instanceof XFeatureCall && isReferenceToSelf((XFeatureCall) obj2, jvmType)) {
						return true;
					}
				}
				closureIterator.prune();
			}
		}
		return false;
	}

}
