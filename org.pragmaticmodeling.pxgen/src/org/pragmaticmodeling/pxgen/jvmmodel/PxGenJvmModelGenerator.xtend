package org.pragmaticmodeling.pxgen.jvmmodel

import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.util.Strings
import org.eclipse.xtext.xbase.XExpression
import org.eclipse.xtext.xbase.XNullLiteral
import org.eclipse.xtext.xbase.XNumberLiteral
import org.eclipse.xtext.xbase.XStringLiteral
import org.eclipse.xtext.xbase.compiler.JvmModelGenerator
import org.eclipse.xtext.xbase.compiler.XbaseCompiler
import org.pragmaticmodeling.pxgen.pxGen.GenConf
import org.pragmaticmodeling.pxgen.pxGen.PackageDeclaration
import static extension org.pragmaticmodeling.pxgen.util.PxGenHelper.*
import org.pragmaticmodeling.pxgen.pxGen.FragmentEnablement
import org.pragmaticmodeling.pxgen.pxGen.FragmentProperty
import org.pragmaticmodeling.pxgen.pxGen.XtextGenerator
import org.eclipse.xtext.xbase.XMemberFeatureCall
import org.eclipse.xtext.xbase.compiler.output.FakeTreeAppendable
import org.eclipse.xtext.common.types.TypesFactory
import org.eclipse.xtext.xbase.compiler.ImportManager
import org.eclipse.xtext.common.types.JvmGenericType

class PxGenJvmModelGenerator extends JvmModelGenerator {

	@Inject
	private XbaseCompiler compiler

	override void internalDoGenerate(EObject type, IFileSystemAccess fsa) {
		if (type instanceof XtextGenerator) {
			var pkgDeclaration = type.package
			if (pkgDeclaration !== null) {
				var genConf = pkgDeclaration.elements.filter(GenConf).head
				if (genConf !== null) {
					var String fileName = genConf.eResource.URI.trimFileExtension.lastSegment
					var pkg = pkgDeclaration.name
					if (pkg !== null) {
						fsa.generateFile(pkg.replaceAll("\\.", "/") + "/" + fileName + ".mwe2",
							mwe2Workflow(genConf, pkg, fileName))
					}
				} else {
					super.internalDoGenerate(type, fsa)
				}
			}
		} else if (type instanceof JvmGenericType ) {
			var isGenConf = isGenConf(type as JvmGenericType)
			if (isGenConf) {
				
			} else {
				
			super.internalDoGenerate(type, fsa)
			}
		} 
		else {
			super.internalDoGenerate(type, fsa)
		}
	}
	
	def boolean isGenConf(JvmGenericType object) {
		var result = false
		for (a : object.annotations) {
			if (a.annotation.simpleName == "PxGenConf") {
				result = true	
			}
		}
		result = object.annotations.exists[a|a.annotation.simpleName == "PxGenConf"]
		return result
	}

	def String mwe2Workflow(GenConf genConf, String javaPackage, String simpleName) {
		var className = genConf.generator.qualifiedName
		var pxGenerator = genConf?.generator
		val genConfClassName = '''«javaPackage».«simpleName»Conf'''
		val importManager = new ImportManager(true, getJvmType(genConfClassName))
		var workflowClassName = genConf.generator.componentClassName
		var simpleClassName = className.substring(className.lastIndexOf(".") + 1, className.length)
		var simpleWorkflowClassName = workflowClassName.substring(workflowClassName.lastIndexOf(".") + 1,
			workflowClassName.length)
		var body = '''
			Workflow {
					
				component = «simpleWorkflowClassName» {
						
					generator = «simpleClassName» {
							
						rootLocation = rootPath
						«FOR p : genConf.elements.filter(FragmentProperty)»
							«mwe2ExtenderProperty(p, 0, importManager)»
						«ENDFOR»
						
			«FOR p : genConf.elements.filter(FragmentEnablement)»
				«mwe2Extender(p, 3, importManager)»
			«ENDFOR»
					} 
				}
			
			}'''
		'''
		module «genConfClassName»
		import «className»
		import «workflowClassName»
		«FOR i : importManager.imports»
		import «i»
		«ENDFOR»
				
		var rootPath = ".."
		
		«body»
		'''
	}

	def String mwe2Extender(FragmentEnablement extender, int indentation, ImportManager importManager) {
		'''«indent(indentation)»
«indent(indentation)»fragment = «extender.extender.type.qualifiedName» {
«FOR p : extender.elements.filter(FragmentProperty)»
	«mwe2ExtenderProperty(p, indentation+1, importManager)»
«ENDFOR»
«FOR p : extender.elements.filter(FragmentEnablement)»
	«mwe2Extender(p, indentation+1, importManager)»
«ENDFOR»
«indent(indentation)»}'''
	}

	def String mwe2ExtenderProperty(FragmentProperty property, int indentation, ImportManager importManager) {
		'''«indent(indentation)»«property.name» = «property.value.toJavaExpression(importManager)»'''
	}

	def indent(int nb) {
		var indentation = "";
		for (var i = 0; i < nb; i++) {
			indentation += "\t"
		}
		indentation
	}

	def String toJavaExpression(XExpression expression, ImportManager importManager) {
		switch expression {
			XStringLiteral: {
				var String javaString = Strings.convertToJavaString(expression.getValue(), false);
				return "\"" + javaString + "\"";
			}
			XNumberLiteral: {
				expression.value
			}
			XNullLiteral: {
				"null"
			}
			default: {
				val result = new FakeTreeAppendable(importManager)
				compiler.toJavaExpression(expression, result)
				result.toString
			}
		}

	}

	def String toJavaExpression(XNumberLiteral expr) {
		return expr.value
	}
	

	def getJvmType(String type) {
		val declaredType = TypesFactory.eINSTANCE.createJvmGenericType
		declaredType.setSimpleName(type.substring(type.lastIndexOf(".")+1));
		declaredType.setPackageName(type.substring(0, type.lastIndexOf(".")))
		declaredType
	}

}
