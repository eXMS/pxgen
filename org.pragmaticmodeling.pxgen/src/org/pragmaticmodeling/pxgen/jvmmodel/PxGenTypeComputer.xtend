package org.pragmaticmodeling.pxgen.jvmmodel

import com.google.inject.Inject
import java.util.List
import org.eclipse.xtend2.lib.StringConcatenation
import org.eclipse.xtend2.lib.StringConcatenationClient
import org.eclipse.xtext.common.types.JvmFormalParameter
import org.eclipse.xtext.xbase.XCastedExpression
import org.eclipse.xtext.xbase.XExpression
import org.eclipse.xtext.xbase.XFeatureCall
import org.eclipse.xtext.xbase.XVariableDeclaration
import org.eclipse.xtext.xbase.XbasePackage
import org.eclipse.xtext.xbase.annotations.typesystem.XbaseWithAnnotationsTypeComputer
import org.eclipse.xtext.xbase.typesystem.computation.ITypeComputationState
import org.eclipse.xtext.xbase.typesystem.computation.ITypeExpectation
import org.eclipse.xtext.xbase.typesystem.conformance.ConformanceFlags
import org.eclipse.xtext.xbase.typesystem.references.LightweightTypeReference
import org.eclipse.xtext.xbase.typesystem.util.CommonTypeComputationServices
import org.pragmaticmodeling.pxgen.pxGen.BooleanExpression
import org.pragmaticmodeling.pxgen.pxGen.File
import org.pragmaticmodeling.pxgen.pxGen.Function
import org.pragmaticmodeling.pxgen.pxGen.GenerateInstruction
import org.pragmaticmodeling.pxgen.pxGen.GenerationDirectory
import org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition
import org.pragmaticmodeling.pxgen.pxGen.PackageDeclaration
import org.pragmaticmodeling.pxgen.pxGen.ParameterDeclaration
import org.pragmaticmodeling.pxgen.pxGen.Project
import org.pragmaticmodeling.pxgen.pxGen.RichString
import org.pragmaticmodeling.pxgen.pxGen.RichStringElseIf
import org.pragmaticmodeling.pxgen.pxGen.RichStringForLoop
import org.pragmaticmodeling.pxgen.pxGen.RichStringIf
import org.pragmaticmodeling.pxgen.pxGen.RichStringLiteral
import org.pragmaticmodeling.pxgen.pxGen.RunMethod
import org.pragmaticmodeling.pxgen.pxGen.StringExpression
import org.pragmaticmodeling.pxgen.pxGen.XtextGenerator

import static extension org.pragmaticmodeling.pxgen.util.PxGenHelper.*

@SuppressWarnings("restriction")
class PxGenTypeComputer extends XbaseWithAnnotationsTypeComputer {

	@Inject
	private CommonTypeComputationServices services;

	override computeTypes(XExpression expression, ITypeComputationState state) {
		if (expression instanceof RunMethod) {
			_computeTypes(expression, state);
		} else if (expression instanceof File) {
			_computeTypes(expression, state);
		} else if (expression instanceof Function) {
			_computeTypes(expression, state);
		} else if (expression instanceof RichString) {
			_computeTypes(expression, state);
		} else if (expression instanceof RichStringForLoop) {
			_computeTypes(expression, state);
		} else if (expression instanceof RichStringIf) {
			_computeTypes(expression, state);
		} else if (expression instanceof RichStringLiteral) {
			_computeTypes(expression, state);
		} else if (expression instanceof Project) {
			_computeTypes(expression, state);
		} else if (expression instanceof GenerateInstruction) {
			_computeTypes(expression, state);
		} else if (expression instanceof ParameterDeclaration) {
			_computeTypes(expression, state);
		} else if (expression instanceof BooleanExpression) {
			_computeTypes(expression, state);
		} else if (expression instanceof StringExpression) {
			_computeTypes(expression, state);
		} else if (expression instanceof GenerationDirectory) {
			_computeTypes(expression, state);
		} else {
			super.computeTypes(expression, state)
		}
	}

	protected def _computeTypes(XtextGenerator object, ITypeComputationState state) {
		state.acceptActualType(getTypeForName(Void.TYPE, state));
		if (object.package !== null)
			_computeTypes(object.package, state)
	}

	protected def _computeTypes(PackageDeclaration object, ITypeComputationState state) {
		state.acceptActualType(getTypeForName(Void.TYPE, state));
		//FIXME
//		for (e : object.elements) {
//			_computeTypes(e, state)
//		}
//		if (object.definition !== null) {
//			_computeTypes(object.definition, state)
//		}
	}
	protected def _computeTypes(GenerationDirectory expression, ITypeComputationState state) {
		state.acceptActualType(getTypeForName(Void.TYPE, state));
	}

	protected def _computeTypes(StringExpression expression, ITypeComputationState state) {
		if (expression.body !== null)
			computeTypesString(expression.body, state)
	}

	protected def _computeTypes(BooleanExpression expression, ITypeComputationState state) {
		state.acceptActualType(getTypeForName(Boolean.TYPE, state));
		if (expression.body !== null)
			computeTypesBoolean(expression.body, state)
	}

	protected def computeTypesString(XExpression expression, ITypeComputationState state) {
		computeTypes(expression, state)
		state.acceptActualType(getTypeForName(String, state));
	}

	protected def computeTypesBoolean(XExpression expression, ITypeComputationState state) {
		computeTypes(expression, state)
		state.acceptActualType(getTypeForName(Boolean, state));
	}

	protected def _computeTypes(ParameterDeclaration object, ITypeComputationState state) {
		if (object.right !== null)
			computeTypes(object.right, state)
		var LightweightTypeReference primitiveVoid = getPrimitiveVoid(state);
		state.acceptActualType(primitiveVoid);
	}

	protected def _computeTypes(GenerateInstruction object, ITypeComputationState state) {
		for (p : object.parameters) {
			computeTypes(p, state)
		}
		if (object.path !== null) {
			computeTypes(object.path, state)
		}
		state.acceptActualType(getTypeForName(Void.TYPE, state));
	}

	protected def _computeTypes(GeneratorDefinition object, ITypeComputationState state) {
		for (e : object.elements) {
			if (e instanceof XFeatureCall) {
				var ITypeComputationState expressionState = state.withoutExpectation();
				expressionState.computeTypes(e);
			}
		}
		state.acceptActualType(getTypeForName(Class.forName(object.className), state));
	}

	protected def _computeTypes(Project object, ITypeComputationState state) {
		for (e : object.elements) {
			if (e instanceof XFeatureCall) {
				var ITypeComputationState expressionState = state.withoutExpectation();
				expressionState.computeTypes(e);
			}
		}
		if (object instanceof Project) {
			if (object.projectName !== null) {
				computeTypes(object.projectName, state)
			}
		}
	}

	protected def _computeTypes(RichString object, ITypeComputationState state) {
		var List<XExpression> expressions = object.getExpressions();
		if (!expressions.isEmpty()) {
			for (XExpression expression : expressions) {
				var ITypeComputationState expressionState = state.withoutExpectation();
				expressionState.computeTypes(expression);
				if (expression instanceof XVariableDeclaration) {
					addLocalToCurrentScope(expression as XVariableDeclaration, state);
				}
			}
		}
		for (ITypeExpectation expectation : state.getExpectations()) {
			var LightweightTypeReference expectedType = expectation.getExpectedType();
			if (expectedType != null && expectedType.isType(StringConcatenation)) {
				expectation.acceptActualType(expectedType, ConformanceFlags.CHECKED_SUCCESS);
			} else if (expectedType != null && expectedType.isType(StringConcatenationClient)) {
				expectation.acceptActualType(expectedType, ConformanceFlags.CHECKED_SUCCESS);
			} else if (expectedType != null && expectedType.isType(String)) {
				expectation.acceptActualType(expectedType, ConformanceFlags.CHECKED_SUCCESS);
			// TODO this special treatment here should become obsolete as soon as the expectations are properly propagated
			} else if (!(object.eContainer() instanceof XCastedExpression) &&
				object.eContainingFeature() != XbasePackage.Literals.XMEMBER_FEATURE_CALL__MEMBER_CALL_TARGET &&
				(expectedType != null && !expectedType.isResolved() ||
					expectedType == null && !expectation.isVoidTypeAllowed())) {
				var LightweightTypeReference type = getRawTypeForName(String, state);
				expectation.acceptActualType(type, ConformanceFlags.UNCHECKED);
			} else {
				var LightweightTypeReference type = getRawTypeForName(CharSequence, state);
				expectation.acceptActualType(type, ConformanceFlags.UNCHECKED);
			}
		}
	}

	protected def _computeTypes(RichStringForLoop object, ITypeComputationState state) {
		var LightweightTypeReference charSequence = getRawTypeForName(CharSequence, state);
		var ITypeComputationState eachState = state.withExpectation(charSequence);
		var JvmFormalParameter parameter = object.getDeclaredParam();
		if (parameter != null) {
			var LightweightTypeReference parameterType = computeForLoopParameterType(object, state);
			eachState = eachState.assignType(parameter, parameterType);
		}
		eachState.computeTypes(object.getEachExpression());

		state.withNonVoidExpectation().computeTypes(object.getBefore());
		state.withNonVoidExpectation().computeTypes(object.getSeparator());
		state.withNonVoidExpectation().computeTypes(object.getAfter());

		var LightweightTypeReference primitiveVoid = getPrimitiveVoid(state);
		state.acceptActualType(primitiveVoid);

		state.acceptActualType(charSequence);
	}

	protected def _computeTypes(RichStringIf object, ITypeComputationState state) {
		var LightweightTypeReference charSequence = getRawTypeForName(CharSequence, state);
		var LightweightTypeReference booleanType = getRawTypeForName(Boolean.TYPE, state);

		var ITypeComputationState conditionExpectation = state.withExpectation(booleanType);
		var XExpression condition = object.getIf();
		conditionExpectation.computeTypes(condition);
		var XExpression thenExpression = object.getThen();
		var ITypeComputationState thenState = reassignCheckedType(condition, thenExpression, state);
		thenState.withExpectation(charSequence).computeTypes(thenExpression);
		for (RichStringElseIf elseIf : object.getElseIfs()) {
			state.withExpectation(booleanType).computeTypes(elseIf.getIf());
			var ITypeComputationState elseState = reassignCheckedType(elseIf.getIf(), elseIf.getThen(), state);
			elseState.withExpectation(charSequence).computeTypes(elseIf.getThen());
		}
		state.withExpectation(charSequence).computeTypes(object.getElse());
		state.acceptActualType(charSequence);
	}

	protected def _computeTypes(RichStringLiteral object, ITypeComputationState state) {
		var LightweightTypeReference type = getRawTypeForName(CharSequence, state);
		state.acceptActualType(type);
	}

	protected def _computeTypes(RunMethod expression, ITypeComputationState state) {
		state.acceptActualType(getTypeForName(Void.TYPE, state));
		if (expression.body !== null) {
			computeTypes(expression.body, state)
		}
	}

	protected def _computeTypes(File expression, ITypeComputationState state) {
		if (expression.body !== null) {
			computeTypes(expression.body, state)
		}
		for (variable : expression.params) {
			_computeTypes(variable, state)
		}
		for (function : expression.functions) {
			_computeTypes(function, state)
		}
	}

	protected def _computeTypes(Function expression, ITypeComputationState state) {
		if (expression.body !== null) {
			computeTypes(expression.body, state)
		}
		state.acceptActualType(getTypeForName(Void.TYPE, state));
	}

}
