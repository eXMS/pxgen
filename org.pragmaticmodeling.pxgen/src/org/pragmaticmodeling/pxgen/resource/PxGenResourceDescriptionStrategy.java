package org.pragmaticmodeling.pxgen.resource;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy;
import org.eclipse.xtext.util.IAcceptor;
import org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition;

import com.google.inject.Inject;

public class PxGenResourceDescriptionStrategy extends DefaultResourceDescriptionStrategy {

	@Inject
	private IQualifiedNameConverter.DefaultImpl defaultQualifiedNameConverter;

	private static Logger logger = Logger.getLogger(PxGenResourceDescriptionStrategy.class);

	@Override
	public boolean createEObjectDescriptions(EObject eObject, IAcceptor<IEObjectDescription> acceptor) {
		if (eObject instanceof GeneratorDefinition) {
			// FIXME
		}
		return super.createEObjectDescriptions(eObject, acceptor);
	}

	protected Map<String, String> createUserData(final GeneratorDefinition eObject) {
		Map<String, String> map = new HashMap<String, String>();
		if (eObject.getExtended() != null) {
			QualifiedName fqn = getQualifiedNameProvider().getFullyQualifiedName(eObject.getExtended());
			map.put("extenderFqn", fqn.toString());
		}
		return map;
	}

}
