/** 
 * Copyright (c) 2015 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.pragmaticmodeling.pxgen.runtime.launch

import com.google.inject.Binder
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtext.xtext.generator.CodeConfig
import org.eclipse.xtext.xtext.generator.XtextGenerator
import org.eclipse.xtext.xtext.generator.XtextGeneratorNaming
import org.eclipse.xtext.xtext.generator.model.project.XtextProjectConfig
import org.pragmaticmodeling.pxgen.runtime.IProjectCreator
import org.pragmaticmodeling.pxgen.runtime.projects.PxGenProjectCreator
import org.pragmaticmodeling.pxgen.PxGenRuntimeModule

/** 
 * An instance of this module is assigned to the {@code configuration} property
 * of {@link XtextGenerator}. It contains the {@link XtextProjectConfig project
 * configuration} and the {@link CodeConfig code configuration}. If you need to
 * configure more aspects of the generator, create a subclass and bind your
 * custom configuration classes. For example, in order to adapt the{@link XtextGeneratorNaming naming} of the generated code, use the following:
 * <pre>
 * class MyGeneratorModule extends DefaultGeneratorModule {
 * def Class&lt;? extends XtextGeneratorNaming&gt; bindXtextGeneratorNaming() {
 * MyGeneratorNaming
 * }
 * }
 * </pre>
 */
@SuppressWarnings("all") abstract class AbstractGeneratorModule extends PxGenRuntimeModule {
	
	@Accessors
	CodeConfig code = new CodeConfig

	def Class<? extends IProjectCreator> bindIProjectCreator() {
		return PxGenProjectCreator
	}

	def void configureCodeConfig(Binder binder) {
		binder.bind(CodeConfig).toInstance(code)
	}
}
