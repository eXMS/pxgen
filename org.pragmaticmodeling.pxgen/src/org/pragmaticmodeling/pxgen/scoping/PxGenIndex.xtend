package org.pragmaticmodeling.pxgen.scoping

import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.resource.impl.ResourceDescriptionsProvider
import org.pragmaticmodeling.pxgen.pxGen.PxGenPackage

class PxGenIndex {
	
	@Inject ResourceDescriptionsProvider rdp
	
	def getResourceDescription(EObject o) {
		val index = rdp.getResourceDescriptions(o.eResource)
		index.getResourceDescription(o.eResource.URI)
	}
	
	def getExportedEObjectDescriptions(EObject o) {
		o.resourceDescription.exportedObjects
	}
	
	def getExportedGeneratorDefinitionEObjectDescriptions(EObject o) {
		o.resourceDescription.getExportedObjectsByType(PxGenPackage.eINSTANCE.generatorDefinition)
	}
}