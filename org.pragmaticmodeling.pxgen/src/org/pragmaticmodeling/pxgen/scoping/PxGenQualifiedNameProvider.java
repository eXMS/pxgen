package org.pragmaticmodeling.pxgen.scoping;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.scoping.XbaseQualifiedNameProvider;
import org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition;
import org.pragmaticmodeling.pxgen.pxGen.Project;

public class PxGenQualifiedNameProvider extends XbaseQualifiedNameProvider {

	
	@Override
	public QualifiedName getFullyQualifiedName(EObject obj) {
		QualifiedName fqn = null;
		if (obj instanceof Project) {
			fqn = getConverter().toQualifiedName(((Project)obj).getName()); 
		} else if (obj instanceof GeneratorDefinition) {
			fqn = super.getFullyQualifiedName(obj);
		} else {
			fqn = super.getFullyQualifiedName(obj);
		}
		return fqn;
	}
}
