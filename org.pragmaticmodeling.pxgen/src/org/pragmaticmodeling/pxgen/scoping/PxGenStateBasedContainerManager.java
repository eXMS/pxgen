package org.pragmaticmodeling.pxgen.scoping;

import java.util.List;

import org.eclipse.xtext.resource.IContainer;
import org.eclipse.xtext.resource.IResourceDescription;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.resource.containers.StateBasedContainerManager;

public class PxGenStateBasedContainerManager extends StateBasedContainerManager {

	@Override
	public List<IContainer> getVisibleContainers(IResourceDescription desc,
			IResourceDescriptions resourceDescriptions) {
		return super.getVisibleContainers(desc, resourceDescriptions);
	}
}
