package org.pragmaticmodeling.pxgen.scoping;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.common.types.xtext.TypesAwareDefaultGlobalScopeProvider;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.IResourceDescription;
import org.eclipse.xtext.scoping.IScope;
import org.pragmaticmodeling.pxgen.runtime.IPxGeneratorsExtensionManager;

import com.google.common.base.Predicate;
import com.google.inject.Inject;

@SuppressWarnings("restriction")
public class PxGenTypesAwareDefaultGlobalScopeProvider extends TypesAwareDefaultGlobalScopeProvider {

	@Inject
	IResourceDescription.Manager mgr;

	@Inject
	IPxGeneratorsExtensionManager fragmentsManager;

	@Override
	public IScope getScope(Resource resource, EReference reference, Predicate<IEObjectDescription> filter) {
//		if (reference.equals(PxGenPackage.Literals.FRAGMENT_ENABLEMENT__EXTENDER) || reference.equals(TypesPackage.Literals.JVM_PARAMETERIZED_TYPE_REFERENCE__TYPE)) {
//			Iterable<IEObjectDescription> exportedObjects = new HashSet<IEObjectDescription>();
//			for (String id : fragmentsManager.getGeneratorFragmentIds()) {
//				URI uri = fragmentsManager.getModelUri(id);
//				Resource fragResource = resource.getResourceSet().getResource(uri, false);
//				if (fragResource == null || fragResource.getContents().isEmpty()) {
//					fragResource = resource.getResourceSet().createResource(uri);
////					IResourceDescription resourceDescription = mgr.getResourceDescription(fragResource);
////					exportedObjects = Iterables.concat(exportedObjects, resourceDescription.getExportedObjects());
//				}
//
//			}
//			return new SimpleScope(super.getScope(resource, reference, filter), exportedObjects, false);
//		}
		return super.getScope(resource, reference, filter);
	}
	
	

}
