package org.pragmaticmodeling.pxgen.util

import java.util.ArrayList
import java.util.List
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.common.types.JvmTypeReference
import org.eclipse.xtext.xbase.XExpression
import org.pragmaticmodeling.pxgen.pxGen.File
import org.pragmaticmodeling.pxgen.pxGen.Function
import org.pragmaticmodeling.pxgen.pxGen.GenConf
import org.pragmaticmodeling.pxgen.pxGen.GenerateInstruction
import org.pragmaticmodeling.pxgen.pxGen.GenerationDirectory
import org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition
import org.pragmaticmodeling.pxgen.pxGen.PackageDeclaration
import org.pragmaticmodeling.pxgen.pxGen.ParameterDeclaration
import org.pragmaticmodeling.pxgen.pxGen.Project
import org.pragmaticmodeling.pxgen.pxGen.XtextGenerator

class PxGenHelper {

	def static getGeneratorDefinition(JvmTypeReference reference) {
		var GeneratorDefinition genDef = null;
		if (reference !== null) {
			var String simpleName = reference.type.simpleName
			var EList<Resource> resources = reference.eResource().getResourceSet().getResources();
			for (var i = resources.size() - 1; i >= 0; i--) {
				var Resource r = resources.get(i);
				if (isPxGenResource(r)) {
					var XtextGenerator root = r.getContents().head as XtextGenerator
					if (root !== null && root.getPackage() !== null) {
						for (candidate : root.getPackage().elements) {
							if (candidate instanceof GeneratorDefinition) {
								if (candidate.getName().equals(simpleName)) {
									genDef = candidate;
								}
							}
						}
					}
				}
			}
		}
		return genDef;
	}

	def static boolean isPxGenResource(Resource r) {
		"pxgen".equals(r.getURI().fileExtension());
	}

	def static List<File> files(Project project) {
		return project.elements.filter(File).toList;
	}

	def static List<GenerationDirectory> genDirs(Project project) {
		return project.elements.filter(GenerationDirectory).toList;
	}

	def static List<GeneratorDefinition> allExtendedDefinitions(GeneratorDefinition definition) {
		var result = new ArrayList<GeneratorDefinition>()
		var GeneratorDefinition superDef = definition.extended
		while (superDef !== null) {
			result.add(superDef)
			superDef = superDef.extended
		}
		return result
	}

	def static List<XExpression> allFeatures(GeneratorDefinition genDef) {
		var result = new ArrayList()
		var allSuperDefs = new ArrayList()
		allSuperDefs += genDef
		allSuperDefs += genDef.allExtendedDefinitions
		for (GeneratorDefinition def : allSuperDefs) {
			result.addAll(def.features)
		}
		return result
	}

	def static List<XExpression> allFeatures(Project project) {
		var result = new ArrayList()
		var genDef = EcoreUtil2.getContainerOfType(project, GeneratorDefinition)
		result += project.features
		result += genDef.allFeatures
		return result
	}

	def static List<XExpression> features(EObject object) {
		switch object {
			Project: {
				return object.elements.filter[e|e.isFeature].toList
			}
			GeneratorDefinition: {
				return object.elements.filter[e|e.isFeature].toList
			}
		}
		return new ArrayList()
	}

	def static boolean isFeature(XExpression expression) {
		expression instanceof Function || expression instanceof ParameterDeclaration || expression instanceof Project
	}

	def static String getModuleClassName(GeneratorDefinition definition) {
		var packageDeclaration = definition.parentPackageDeclaration
		return packageDeclaration.name + "." + definition.name.toFirstUpper + "Module"
	}

	def static String className(JvmTypeReference genDef) {
		return genDef.qualifiedName
	}

	def static String className(GeneratorDefinition genDef) {
		var packageDeclaration = genDef.parentPackageDeclaration
		return packageDeclaration.name + "." + genDef.name
	}

	def static String componentClassName(GeneratorDefinition genDef) {
		return genDef.className + "Workflow"
	}

	def static String componentClassName(JvmTypeReference genDef) {
		return genDef.qualifiedName + "Workflow"
	}

	def static String standaloneSetupClassName(GeneratorDefinition genDef) {
		return genDef.className + "StandaloneSetup"
	}

	def static String className(File file) {
		if (file.eIsProxy)
			return "fixme"
		var packageDeclaration = file.parentPackageDeclaration
		return packageDeclaration.name + ".files." + file.name.toFirstUpper + "File"
	}

	def static String className2(Project project) {
		var GeneratorDefinition parentDef = EcoreUtil2.getContainerOfType(project, GeneratorDefinition)
		var prefix = parentDef.className
		return prefix + "." + project.name.toFirstUpper
	}

	def static String className(Project project) {
		var packageDeclaration = project.parentPackageDeclaration
		return packageDeclaration.name + "." + project.name.toFirstUpper
	}

	def static PackageDeclaration parentPackageDeclaration(EObject eObject) {
		if (eObject !== null) {
			return EcoreUtil2.getContainerOfType(eObject, PackageDeclaration);
		}
		return null;
	}

	def static List<Function> getFunctions(File file) {
		file.elements.filter(Function).toList
	}

	def static List<EObject> getInheritedMembers(EObject object) {
		var List<EObject> result = new ArrayList<EObject>
		result += object.eContents.filter(ParameterDeclaration)
		var EObject container = object.eContainer
		while (container !== null) {
			result += container.eContents.filter(ParameterDeclaration)
			container = container.eContainer
		}
		return result
	}

	def static List<ParameterDeclaration> getParams(EObject object) {
		object.eContents.filter(ParameterDeclaration).toList
	}

	def static String contextClassName(File file, boolean isInterface) {
		var Project parentProject = EcoreUtil2.getContainerOfType(file, Project)
		if (parentProject !== null) {
			return parentProject.contextClassName(isInterface)
		} else {
			var GeneratorDefinition parentDef = EcoreUtil2.getContainerOfType(file, GeneratorDefinition)
			return parentDef?.contextClassName(isInterface)
		}
	}

	def static EObject parentContext(File file) {
		var Project parentProject = EcoreUtil2.getContainerOfType(file, Project)
		if (parentProject !== null) {
			return parentProject
		} else {
			var GeneratorDefinition parentDef = EcoreUtil2.getContainerOfType(file, GeneratorDefinition)
			return parentDef
		}
	}

	def static String contextClassName(Project project, boolean isInterface) {
		if (project.eIsProxy)
			return "fixme"
		var packageDeclaration = project.parentPackageDeclaration
		var interfacePrefix = if(isInterface) "I" else ""
		return packageDeclaration.name + "." + interfacePrefix + project.name.toFirstUpper + "Context"
	}

	def static String contextClassName(GeneratorDefinition genDef, boolean isInterface) {
		var packageDeclaration = genDef.parentPackageDeclaration
		var interfacePrefix = if(isInterface) "I" else ""
		return packageDeclaration.name + "." + interfacePrefix + genDef.name.toFirstUpper + "Context"
	}

	def static List<EObject> getAllContexts(GeneratorDefinition definition) {
		var result = new ArrayList<EObject>
		result.add(definition);
		result.addAll(definition.elements.filter(Project));
		result
	}

	def static boolean definesGetter(EObject context, String fieldName, JvmTypeReference reference) {
		var Function getter = context.eContents.filter(Function).filter[f|f.name == "get" + fieldName.toFirstUpper].head
		return getter !== null
	}

	def static boolean definesSetter(EObject context, String fieldName, JvmTypeReference reference) {
		var Function setter = context.eContents.filter(Function).filter[f|f.name == "set" + fieldName.toFirstUpper].head
		return setter !== null
	}

	def static File getFile(Project project, String fileName) {
		for (XExpression e : project.getElements()) {
			if (e instanceof File && fileIsNamed(e as File, fileName)) {
				return e as File;
			}
		}
		var genDef = EcoreUtil2.getContainerOfType(project, GeneratorDefinition)
		var parentDefs = new ArrayList()
		parentDefs += genDef
		parentDefs += genDef.allExtendedDefinitions
		for (def : parentDefs) {
			for (XExpression e : def.getElements()) {
				if (e instanceof File && fileIsNamed(e as File, fileName)) {
					return e as File;
				}
			}
		}
		return null;
	}

	def static List<? extends EObject> visibleFiles(GenerateInstruction instruction) {
		var result = new ArrayList()
		var genDef = EcoreUtil2.getContainerOfType(instruction, GeneratorDefinition)
		var project = EcoreUtil2.getContainerOfType(instruction, Project)
		var allSuperDefs = genDef.allExtendedDefinitions
		allSuperDefs.add(genDef)
		for (GeneratorDefinition def : allSuperDefs) {
			result.addAll(def.files)
		}
		result.addAll(project.files)
		result
	}

	def static List<File> files(GeneratorDefinition definition) {
		return definition.elements.filter(File).toList;
	}
	
	def static boolean fileIsNamed(File e, String fileName) {
		return fileName.equals(e.getName());
	}

	def static String getName(Project e) {
		e.name
	}

	def static String className(GenConf genConf) {
		var String result = "WorkflowComponent"
		if (genConf.generator.type.simpleName != "void") {
			result = genConf.generator.qualifiedName + "WorkflowComponent"
		}
		return result
	}

	def static String javaPackage(EObject eObject) {
		var PackageDeclaration pkg = EcoreUtil2.getContainerOfType(eObject, PackageDeclaration);
		if (pkg !== null) {
			return pkg.name
		}
		return null
	}

}
