/*******************************************************************************
 * Copyright (c) 2017 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.pragmaticmodeling.pxgen.validation;

import org.eclipse.xtext.common.types.JvmParameterizedTypeReference;
import org.eclipse.xtext.diagnostics.DiagnosticMessage;
import org.eclipse.xtext.diagnostics.Severity;
import org.eclipse.xtext.linking.impl.LinkingDiagnosticMessageProvider;
import org.pragmaticmodeling.pxgen.pxGen.FragmentEnablement;
import org.pragmaticmodeling.pxgen.pxGen.GenConf;
import org.pragmaticmodeling.pxgen.pxGen.PxGenPackage;
import org.pragmaticmodeling.pxgen.pxGen.util.PxGenSwitch;
import org.pragmaticmodeling.pxgen.runtime.IPxGeneratorsExtensionManager;
import org.pragmaticmodeling.pxgen.runtime.PxGeneratorsExtensionManager;

public class PxGenLinkingDiagnosticMessageProvider extends LinkingDiagnosticMessageProvider {

	public static final String UNRESOLVED_FRAGMENT = "unresolved.fragment";

	@Override
	public DiagnosticMessage getUnresolvedProxyMessage(final ILinkingDiagnosticContext context) {
		DiagnosticMessage diagnosticMessage = new PxGenSwitch<DiagnosticMessage>() {

			IPxGeneratorsExtensionManager extensionsManager = new PxGeneratorsExtensionManager();

			@Override
			public DiagnosticMessage caseFragmentEnablement(FragmentEnablement parent) {
				if (context.getReference().equals(PxGenPackage.Literals.FRAGMENT_ENABLEMENT__EXTENDER)) {
					if (extensionsManager.isFragmentClass(context.getLinkText())) {
						String plugin = extensionsManager.getBundle(context.getLinkText());
						return new DiagnosticMessage(context.getLinkText() + " cannot be resolved.", Severity.ERROR,
								UNRESOLVED_FRAGMENT, context.getLinkText(), plugin);
					}
				}
				return null;
			}

			@Override
			public DiagnosticMessage caseGenConf(GenConf object) {
				if (context.getReference().equals(PxGenPackage.Literals.GEN_CONF__GENERATOR)) {
					if (extensionsManager.isFragmentClass(context.getLinkText())) {
						String plugin = extensionsManager.getBundle(context.getLinkText());
						return new DiagnosticMessage(context.getLinkText() + " cannot be resolved.", Severity.ERROR,
								UNRESOLVED_FRAGMENT, context.getLinkText(), plugin);
					}
				}
				return null;
			};
			
			public DiagnosticMessage defaultCase(org.eclipse.emf.ecore.EObject object) {
				if (object instanceof JvmParameterizedTypeReference) {
					if (extensionsManager.isFragmentClass(context.getLinkText())) {
						String plugin = extensionsManager.getBundle(context.getLinkText());
						return new DiagnosticMessage(context.getLinkText() + " cannot be resolved.", Severity.ERROR,
								UNRESOLVED_FRAGMENT, context.getLinkText(), plugin);
					}
				}
				return null;
			};
			
		}.doSwitch(context.getContext());
		return diagnosticMessage != null ? diagnosticMessage : super.getUnresolvedProxyMessage(context);
	}
}
