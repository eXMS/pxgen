package org.pragmaticmodeling.pxgen.jvmmodel;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.common.types.JvmAnnotationReference;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.TypesFactory;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.util.Strings;
import org.eclipse.xtext.xbase.XExpression;
import org.eclipse.xtext.xbase.XNullLiteral;
import org.eclipse.xtext.xbase.XNumberLiteral;
import org.eclipse.xtext.xbase.XStringLiteral;
import org.eclipse.xtext.xbase.compiler.ImportManager;
import org.eclipse.xtext.xbase.compiler.JvmModelGenerator;
import org.eclipse.xtext.xbase.compiler.XbaseCompiler;
import org.eclipse.xtext.xbase.compiler.output.FakeTreeAppendable;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.pragmaticmodeling.pxgen.pxGen.FragmentEnablement;
import org.pragmaticmodeling.pxgen.pxGen.FragmentProperty;
import org.pragmaticmodeling.pxgen.pxGen.GenConf;
import org.pragmaticmodeling.pxgen.pxGen.PackageDeclaration;
import org.pragmaticmodeling.pxgen.pxGen.XtextGenerator;
import org.pragmaticmodeling.pxgen.util.PxGenHelper;

@SuppressWarnings("all")
public class PxGenJvmModelGenerator extends JvmModelGenerator {
  @Inject
  private XbaseCompiler compiler;
  
  @Override
  public void internalDoGenerate(final EObject type, final IFileSystemAccess fsa) {
    if ((type instanceof XtextGenerator)) {
      PackageDeclaration pkgDeclaration = ((XtextGenerator)type).getPackage();
      if ((pkgDeclaration != null)) {
        GenConf genConf = IterableExtensions.<GenConf>head(Iterables.<GenConf>filter(pkgDeclaration.getElements(), GenConf.class));
        if ((genConf != null)) {
          String fileName = genConf.eResource().getURI().trimFileExtension().lastSegment();
          String pkg = pkgDeclaration.getName();
          if ((pkg != null)) {
            String _replaceAll = pkg.replaceAll("\\.", "/");
            String _plus = (_replaceAll + "/");
            String _plus_1 = (_plus + fileName);
            String _plus_2 = (_plus_1 + ".mwe2");
            fsa.generateFile(_plus_2, 
              this.mwe2Workflow(genConf, pkg, fileName));
          }
        } else {
          super.internalDoGenerate(type, fsa);
        }
      }
    } else {
      if ((type instanceof JvmGenericType)) {
        boolean isGenConf = this.isGenConf(((JvmGenericType) type));
        if (isGenConf) {
        } else {
          super.internalDoGenerate(type, fsa);
        }
      } else {
        super.internalDoGenerate(type, fsa);
      }
    }
  }
  
  public boolean isGenConf(final JvmGenericType object) {
    boolean result = false;
    EList<JvmAnnotationReference> _annotations = object.getAnnotations();
    for (final JvmAnnotationReference a : _annotations) {
      String _simpleName = a.getAnnotation().getSimpleName();
      boolean _equals = Objects.equal(_simpleName, "PxGenConf");
      if (_equals) {
        result = true;
      }
    }
    final Function1<JvmAnnotationReference, Boolean> _function = (JvmAnnotationReference a_1) -> {
      String _simpleName_1 = a_1.getAnnotation().getSimpleName();
      return Boolean.valueOf(Objects.equal(_simpleName_1, "PxGenConf"));
    };
    result = IterableExtensions.<JvmAnnotationReference>exists(object.getAnnotations(), _function);
    return result;
  }
  
  public String mwe2Workflow(final GenConf genConf, final String javaPackage, final String simpleName) {
    String _xblockexpression = null;
    {
      String className = genConf.getGenerator().getQualifiedName();
      JvmTypeReference _generator = null;
      if (genConf!=null) {
        _generator=genConf.getGenerator();
      }
      JvmTypeReference pxGenerator = _generator;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(javaPackage);
      _builder.append(".");
      _builder.append(simpleName);
      _builder.append("Conf");
      final String genConfClassName = _builder.toString();
      JvmGenericType _jvmType = this.getJvmType(genConfClassName);
      final ImportManager importManager = new ImportManager(true, _jvmType);
      String workflowClassName = PxGenHelper.componentClassName(genConf.getGenerator());
      int _lastIndexOf = className.lastIndexOf(".");
      int _plus = (_lastIndexOf + 1);
      String simpleClassName = className.substring(_plus, className.length());
      int _lastIndexOf_1 = workflowClassName.lastIndexOf(".");
      int _plus_1 = (_lastIndexOf_1 + 1);
      String simpleWorkflowClassName = workflowClassName.substring(_plus_1, 
        workflowClassName.length());
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Workflow {");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("component = ");
      _builder_1.append(simpleWorkflowClassName, "\t");
      _builder_1.append(" {");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t\t\t");
      _builder_1.newLine();
      _builder_1.append("\t\t");
      _builder_1.append("generator = ");
      _builder_1.append(simpleClassName, "\t\t");
      _builder_1.append(" {");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("\t\t\t\t");
      _builder_1.newLine();
      _builder_1.append("\t\t\t");
      _builder_1.append("rootLocation = rootPath");
      _builder_1.newLine();
      {
        Iterable<FragmentProperty> _filter = Iterables.<FragmentProperty>filter(genConf.getElements(), FragmentProperty.class);
        for(final FragmentProperty p : _filter) {
          _builder_1.append("\t\t\t");
          String _mwe2ExtenderProperty = this.mwe2ExtenderProperty(p, 0, importManager);
          _builder_1.append(_mwe2ExtenderProperty, "\t\t\t");
          _builder_1.newLineIfNotEmpty();
        }
      }
      _builder_1.append("\t\t\t");
      _builder_1.newLine();
      {
        Iterable<FragmentEnablement> _filter_1 = Iterables.<FragmentEnablement>filter(genConf.getElements(), FragmentEnablement.class);
        for(final FragmentEnablement p_1 : _filter_1) {
          String _mwe2Extender = this.mwe2Extender(p_1, 3, importManager);
          _builder_1.append(_mwe2Extender);
          _builder_1.newLineIfNotEmpty();
        }
      }
      _builder_1.append("\t\t");
      _builder_1.append("} ");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("}");
      _builder_1.newLine();
      _builder_1.newLine();
      _builder_1.append("}");
      String body = _builder_1.toString();
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("module ");
      _builder_2.append(genConfClassName);
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("import ");
      _builder_2.append(className);
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("import ");
      _builder_2.append(workflowClassName);
      _builder_2.newLineIfNotEmpty();
      {
        List<String> _imports = importManager.getImports();
        for(final String i : _imports) {
          _builder_2.append("import ");
          _builder_2.append(i);
          _builder_2.newLineIfNotEmpty();
        }
      }
      _builder_2.append("\t\t");
      _builder_2.newLine();
      _builder_2.append("var rootPath = \"..\"");
      _builder_2.newLine();
      _builder_2.newLine();
      _builder_2.append(body);
      _builder_2.newLineIfNotEmpty();
      _xblockexpression = _builder_2.toString();
    }
    return _xblockexpression;
  }
  
  public String mwe2Extender(final FragmentEnablement extender, final int indentation, final ImportManager importManager) {
    StringConcatenation _builder = new StringConcatenation();
    String _indent = this.indent(indentation);
    _builder.append(_indent);
    _builder.newLineIfNotEmpty();
    String _indent_1 = this.indent(indentation);
    _builder.append(_indent_1);
    _builder.append("fragment = ");
    String _qualifiedName = extender.getExtender().getType().getQualifiedName();
    _builder.append(_qualifiedName);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    {
      Iterable<FragmentProperty> _filter = Iterables.<FragmentProperty>filter(extender.getElements(), FragmentProperty.class);
      for(final FragmentProperty p : _filter) {
        String _mwe2ExtenderProperty = this.mwe2ExtenderProperty(p, (indentation + 1), importManager);
        _builder.append(_mwe2ExtenderProperty);
        _builder.newLineIfNotEmpty();
      }
    }
    {
      Iterable<FragmentEnablement> _filter_1 = Iterables.<FragmentEnablement>filter(extender.getElements(), FragmentEnablement.class);
      for(final FragmentEnablement p_1 : _filter_1) {
        String _mwe2Extender = this.mwe2Extender(p_1, (indentation + 1), importManager);
        _builder.append(_mwe2Extender);
        _builder.newLineIfNotEmpty();
      }
    }
    String _indent_2 = this.indent(indentation);
    _builder.append(_indent_2);
    _builder.append("}");
    return _builder.toString();
  }
  
  public String mwe2ExtenderProperty(final FragmentProperty property, final int indentation, final ImportManager importManager) {
    StringConcatenation _builder = new StringConcatenation();
    String _indent = this.indent(indentation);
    _builder.append(_indent);
    String _name = property.getName();
    _builder.append(_name);
    _builder.append(" = ");
    String _javaExpression = this.toJavaExpression(property.getValue(), importManager);
    _builder.append(_javaExpression);
    return _builder.toString();
  }
  
  public String indent(final int nb) {
    String _xblockexpression = null;
    {
      String indentation = "";
      for (int i = 0; (i < nb); i++) {
        String _indentation = indentation;
        indentation = (_indentation + "\t");
      }
      _xblockexpression = indentation;
    }
    return _xblockexpression;
  }
  
  public String toJavaExpression(final XExpression expression, final ImportManager importManager) {
    String _switchResult = null;
    boolean _matched = false;
    if (expression instanceof XStringLiteral) {
      _matched=true;
      String javaString = Strings.convertToJavaString(((XStringLiteral)expression).getValue(), false);
      return (("\"" + javaString) + "\"");
    }
    if (!_matched) {
      if (expression instanceof XNumberLiteral) {
        _matched=true;
        _switchResult = ((XNumberLiteral)expression).getValue();
      }
    }
    if (!_matched) {
      if (expression instanceof XNullLiteral) {
        _matched=true;
        _switchResult = "null";
      }
    }
    if (!_matched) {
      String _xblockexpression = null;
      {
        final FakeTreeAppendable result = new FakeTreeAppendable(importManager);
        this.compiler.toJavaExpression(expression, result);
        _xblockexpression = result.toString();
      }
      _switchResult = _xblockexpression;
    }
    return _switchResult;
  }
  
  public String toJavaExpression(final XNumberLiteral expr) {
    return expr.getValue();
  }
  
  public JvmGenericType getJvmType(final String type) {
    JvmGenericType _xblockexpression = null;
    {
      final JvmGenericType declaredType = TypesFactory.eINSTANCE.createJvmGenericType();
      int _lastIndexOf = type.lastIndexOf(".");
      int _plus = (_lastIndexOf + 1);
      declaredType.setSimpleName(type.substring(_plus));
      declaredType.setPackageName(type.substring(0, type.lastIndexOf(".")));
      _xblockexpression = declaredType;
    }
    return _xblockexpression;
  }
}
