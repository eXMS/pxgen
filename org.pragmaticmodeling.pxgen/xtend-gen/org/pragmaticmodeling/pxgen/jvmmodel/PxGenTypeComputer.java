package org.pragmaticmodeling.pxgen.jvmmodel;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtend2.lib.StringConcatenationClient;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.xbase.XBlockExpression;
import org.eclipse.xtext.xbase.XCastedExpression;
import org.eclipse.xtext.xbase.XExpression;
import org.eclipse.xtext.xbase.XFeatureCall;
import org.eclipse.xtext.xbase.XVariableDeclaration;
import org.eclipse.xtext.xbase.XbasePackage;
import org.eclipse.xtext.xbase.annotations.typesystem.XbaseWithAnnotationsTypeComputer;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.typesystem.computation.ITypeComputationState;
import org.eclipse.xtext.xbase.typesystem.computation.ITypeExpectation;
import org.eclipse.xtext.xbase.typesystem.conformance.ConformanceFlags;
import org.eclipse.xtext.xbase.typesystem.references.LightweightTypeReference;
import org.eclipse.xtext.xbase.typesystem.util.CommonTypeComputationServices;
import org.pragmaticmodeling.pxgen.pxGen.BooleanExpression;
import org.pragmaticmodeling.pxgen.pxGen.File;
import org.pragmaticmodeling.pxgen.pxGen.Function;
import org.pragmaticmodeling.pxgen.pxGen.GenerateInstruction;
import org.pragmaticmodeling.pxgen.pxGen.GenerationDirectory;
import org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition;
import org.pragmaticmodeling.pxgen.pxGen.PackageDeclaration;
import org.pragmaticmodeling.pxgen.pxGen.ParameterDeclaration;
import org.pragmaticmodeling.pxgen.pxGen.Project;
import org.pragmaticmodeling.pxgen.pxGen.RichString;
import org.pragmaticmodeling.pxgen.pxGen.RichStringElseIf;
import org.pragmaticmodeling.pxgen.pxGen.RichStringForLoop;
import org.pragmaticmodeling.pxgen.pxGen.RichStringIf;
import org.pragmaticmodeling.pxgen.pxGen.RichStringLiteral;
import org.pragmaticmodeling.pxgen.pxGen.RunMethod;
import org.pragmaticmodeling.pxgen.pxGen.StringExpression;
import org.pragmaticmodeling.pxgen.pxGen.XtextGenerator;
import org.pragmaticmodeling.pxgen.util.PxGenHelper;

@SuppressWarnings("restriction")
public class PxGenTypeComputer extends XbaseWithAnnotationsTypeComputer {
  @Inject
  private CommonTypeComputationServices services;
  
  @Override
  public void computeTypes(final XExpression expression, final ITypeComputationState state) {
    if ((expression instanceof RunMethod)) {
      this._computeTypes(((RunMethod)expression), state);
    } else {
      if ((expression instanceof File)) {
        this._computeTypes(((File)expression), state);
      } else {
        if ((expression instanceof Function)) {
          this._computeTypes(((Function)expression), state);
        } else {
          if ((expression instanceof RichString)) {
            this._computeTypes(((RichString)expression), state);
          } else {
            if ((expression instanceof RichStringForLoop)) {
              this._computeTypes(((RichStringForLoop)expression), state);
            } else {
              if ((expression instanceof RichStringIf)) {
                this._computeTypes(((RichStringIf)expression), state);
              } else {
                if ((expression instanceof RichStringLiteral)) {
                  this._computeTypes(((RichStringLiteral)expression), state);
                } else {
                  if ((expression instanceof Project)) {
                    this._computeTypes(((Project)expression), state);
                  } else {
                    if ((expression instanceof GenerateInstruction)) {
                      this._computeTypes(((GenerateInstruction)expression), state);
                    } else {
                      if ((expression instanceof ParameterDeclaration)) {
                        this._computeTypes(((ParameterDeclaration)expression), state);
                      } else {
                        if ((expression instanceof BooleanExpression)) {
                          this._computeTypes(((BooleanExpression)expression), state);
                        } else {
                          if ((expression instanceof StringExpression)) {
                            this._computeTypes(((StringExpression)expression), state);
                          } else {
                            if ((expression instanceof GenerationDirectory)) {
                              this._computeTypes(((GenerationDirectory)expression), state);
                            } else {
                              super.computeTypes(expression, state);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  protected void _computeTypes(final XtextGenerator object, final ITypeComputationState state) {
    state.acceptActualType(this.getTypeForName(Void.TYPE, state));
    PackageDeclaration _package = object.getPackage();
    boolean _tripleNotEquals = (_package != null);
    if (_tripleNotEquals) {
      this._computeTypes(object.getPackage(), state);
    }
  }
  
  protected void _computeTypes(final PackageDeclaration object, final ITypeComputationState state) {
    state.acceptActualType(this.getTypeForName(Void.TYPE, state));
  }
  
  protected void _computeTypes(final GenerationDirectory expression, final ITypeComputationState state) {
    state.acceptActualType(this.getTypeForName(Void.TYPE, state));
  }
  
  protected void _computeTypes(final StringExpression expression, final ITypeComputationState state) {
    XExpression _body = expression.getBody();
    boolean _tripleNotEquals = (_body != null);
    if (_tripleNotEquals) {
      this.computeTypesString(expression.getBody(), state);
    }
  }
  
  protected void _computeTypes(final BooleanExpression expression, final ITypeComputationState state) {
    state.acceptActualType(this.getTypeForName(Boolean.TYPE, state));
    XExpression _body = expression.getBody();
    boolean _tripleNotEquals = (_body != null);
    if (_tripleNotEquals) {
      this.computeTypesBoolean(expression.getBody(), state);
    }
  }
  
  protected void computeTypesString(final XExpression expression, final ITypeComputationState state) {
    this.computeTypes(expression, state);
    state.acceptActualType(this.getTypeForName(String.class, state));
  }
  
  protected void computeTypesBoolean(final XExpression expression, final ITypeComputationState state) {
    this.computeTypes(expression, state);
    state.acceptActualType(this.getTypeForName(Boolean.class, state));
  }
  
  protected void _computeTypes(final ParameterDeclaration object, final ITypeComputationState state) {
    XExpression _right = object.getRight();
    boolean _tripleNotEquals = (_right != null);
    if (_tripleNotEquals) {
      this.computeTypes(object.getRight(), state);
    }
    LightweightTypeReference primitiveVoid = this.getPrimitiveVoid(state);
    state.acceptActualType(primitiveVoid);
  }
  
  protected void _computeTypes(final GenerateInstruction object, final ITypeComputationState state) {
    EList<XExpression> _parameters = object.getParameters();
    for (final XExpression p : _parameters) {
      this.computeTypes(p, state);
    }
    XExpression _path = object.getPath();
    boolean _tripleNotEquals = (_path != null);
    if (_tripleNotEquals) {
      this.computeTypes(object.getPath(), state);
    }
    state.acceptActualType(this.getTypeForName(Void.TYPE, state));
  }
  
  protected void _computeTypes(final GeneratorDefinition object, final ITypeComputationState state) {
    try {
      EList<XExpression> _elements = object.getElements();
      for (final XExpression e : _elements) {
        if ((e instanceof XFeatureCall)) {
          ITypeComputationState expressionState = state.withoutExpectation();
          expressionState.computeTypes(e);
        }
      }
      state.acceptActualType(this.getTypeForName(Class.forName(PxGenHelper.className(object)), state));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected void _computeTypes(final Project object, final ITypeComputationState state) {
    EList<XExpression> _elements = object.getElements();
    for (final XExpression e : _elements) {
      if ((e instanceof XFeatureCall)) {
        ITypeComputationState expressionState = state.withoutExpectation();
        expressionState.computeTypes(e);
      }
    }
    if ((object instanceof Project)) {
      XExpression _projectName = object.getProjectName();
      boolean _tripleNotEquals = (_projectName != null);
      if (_tripleNotEquals) {
        this.computeTypes(object.getProjectName(), state);
      }
    }
  }
  
  protected void _computeTypes(final RichString object, final ITypeComputationState state) {
    List<XExpression> expressions = object.getExpressions();
    boolean _isEmpty = expressions.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      for (final XExpression expression : expressions) {
        {
          ITypeComputationState expressionState = state.withoutExpectation();
          expressionState.computeTypes(expression);
          if ((expression instanceof XVariableDeclaration)) {
            this.addLocalToCurrentScope(((XVariableDeclaration) expression), state);
          }
        }
      }
    }
    List<? extends ITypeExpectation> _expectations = state.getExpectations();
    for (final ITypeExpectation expectation : _expectations) {
      {
        LightweightTypeReference expectedType = expectation.getExpectedType();
        if (((!Objects.equal(expectedType, null)) && expectedType.isType(StringConcatenation.class))) {
          expectation.acceptActualType(expectedType, ConformanceFlags.CHECKED_SUCCESS);
        } else {
          if (((!Objects.equal(expectedType, null)) && expectedType.isType(StringConcatenationClient.class))) {
            expectation.acceptActualType(expectedType, ConformanceFlags.CHECKED_SUCCESS);
          } else {
            if (((!Objects.equal(expectedType, null)) && expectedType.isType(String.class))) {
              expectation.acceptActualType(expectedType, ConformanceFlags.CHECKED_SUCCESS);
            } else {
              if ((((!(object.eContainer() instanceof XCastedExpression)) && 
                (!Objects.equal(object.eContainingFeature(), XbasePackage.Literals.XMEMBER_FEATURE_CALL__MEMBER_CALL_TARGET))) && (((!Objects.equal(expectedType, null)) && (!expectedType.isResolved())) || 
                (Objects.equal(expectedType, null) && (!expectation.isVoidTypeAllowed()))))) {
                LightweightTypeReference type = this.getRawTypeForName(String.class, state);
                expectation.acceptActualType(type, ConformanceFlags.UNCHECKED);
              } else {
                LightweightTypeReference type_1 = this.getRawTypeForName(CharSequence.class, state);
                expectation.acceptActualType(type_1, ConformanceFlags.UNCHECKED);
              }
            }
          }
        }
      }
    }
  }
  
  protected void _computeTypes(final RichStringForLoop object, final ITypeComputationState state) {
    LightweightTypeReference charSequence = this.getRawTypeForName(CharSequence.class, state);
    ITypeComputationState eachState = state.withExpectation(charSequence);
    JvmFormalParameter parameter = object.getDeclaredParam();
    boolean _notEquals = (!Objects.equal(parameter, null));
    if (_notEquals) {
      LightweightTypeReference parameterType = this.computeForLoopParameterType(object, state);
      eachState = eachState.assignType(parameter, parameterType);
    }
    eachState.computeTypes(object.getEachExpression());
    state.withNonVoidExpectation().computeTypes(object.getBefore());
    state.withNonVoidExpectation().computeTypes(object.getSeparator());
    state.withNonVoidExpectation().computeTypes(object.getAfter());
    LightweightTypeReference primitiveVoid = this.getPrimitiveVoid(state);
    state.acceptActualType(primitiveVoid);
    state.acceptActualType(charSequence);
  }
  
  protected void _computeTypes(final RichStringIf object, final ITypeComputationState state) {
    LightweightTypeReference charSequence = this.getRawTypeForName(CharSequence.class, state);
    LightweightTypeReference booleanType = this.getRawTypeForName(Boolean.TYPE, state);
    ITypeComputationState conditionExpectation = state.withExpectation(booleanType);
    XExpression condition = object.getIf();
    conditionExpectation.computeTypes(condition);
    XExpression thenExpression = object.getThen();
    ITypeComputationState thenState = this.reassignCheckedType(condition, thenExpression, state);
    thenState.withExpectation(charSequence).computeTypes(thenExpression);
    EList<RichStringElseIf> _elseIfs = object.getElseIfs();
    for (final RichStringElseIf elseIf : _elseIfs) {
      {
        state.withExpectation(booleanType).computeTypes(elseIf.getIf());
        ITypeComputationState elseState = this.reassignCheckedType(elseIf.getIf(), elseIf.getThen(), state);
        elseState.withExpectation(charSequence).computeTypes(elseIf.getThen());
      }
    }
    state.withExpectation(charSequence).computeTypes(object.getElse());
    state.acceptActualType(charSequence);
  }
  
  protected void _computeTypes(final RichStringLiteral object, final ITypeComputationState state) {
    LightweightTypeReference type = this.getRawTypeForName(CharSequence.class, state);
    state.acceptActualType(type);
  }
  
  protected void _computeTypes(final RunMethod expression, final ITypeComputationState state) {
    state.acceptActualType(this.getTypeForName(Void.TYPE, state));
    XExpression _body = expression.getBody();
    boolean _tripleNotEquals = (_body != null);
    if (_tripleNotEquals) {
      this.computeTypes(expression.getBody(), state);
    }
  }
  
  protected void _computeTypes(final File expression, final ITypeComputationState state) {
    XBlockExpression _body = expression.getBody();
    boolean _tripleNotEquals = (_body != null);
    if (_tripleNotEquals) {
      this.computeTypes(expression.getBody(), state);
    }
    List<ParameterDeclaration> _params = PxGenHelper.getParams(expression);
    for (final ParameterDeclaration variable : _params) {
      this._computeTypes(variable, state);
    }
    List<Function> _functions = PxGenHelper.getFunctions(expression);
    for (final Function function : _functions) {
      this._computeTypes(function, state);
    }
  }
  
  protected void _computeTypes(final Function expression, final ITypeComputationState state) {
    XExpression _body = expression.getBody();
    boolean _tripleNotEquals = (_body != null);
    if (_tripleNotEquals) {
      this.computeTypes(expression.getBody(), state);
    }
    state.acceptActualType(this.getTypeForName(Void.TYPE, state));
  }
}
