/**
 * Copyright (c) 2015 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.pragmaticmodeling.pxgen.runtime.launch;

import com.google.inject.Binder;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.eclipse.xtext.xtext.generator.CodeConfig;
import org.eclipse.xtext.xtext.generator.XtextGenerator;
import org.eclipse.xtext.xtext.generator.XtextGeneratorNaming;
import org.eclipse.xtext.xtext.generator.model.project.XtextProjectConfig;
import org.pragmaticmodeling.pxgen.PxGenRuntimeModule;
import org.pragmaticmodeling.pxgen.runtime.IProjectCreator;
import org.pragmaticmodeling.pxgen.runtime.projects.PxGenProjectCreator;

/**
 * An instance of this module is assigned to the {@code configuration} property
 * of {@link XtextGenerator}. It contains the {@link XtextProjectConfig project
 * configuration} and the {@link CodeConfig code configuration}. If you need to
 * configure more aspects of the generator, create a subclass and bind your
 * custom configuration classes. For example, in order to adapt the{@link XtextGeneratorNaming naming} of the generated code, use the following:
 * <pre>
 * class MyGeneratorModule extends DefaultGeneratorModule {
 * def Class&lt;? extends XtextGeneratorNaming&gt; bindXtextGeneratorNaming() {
 * MyGeneratorNaming
 * }
 * }
 * </pre>
 */
@SuppressWarnings("all")
public abstract class AbstractGeneratorModule extends PxGenRuntimeModule {
  @Accessors
  private CodeConfig code = new CodeConfig();
  
  public Class<? extends IProjectCreator> bindIProjectCreator() {
    return PxGenProjectCreator.class;
  }
  
  public void configureCodeConfig(final Binder binder) {
    binder.<CodeConfig>bind(CodeConfig.class).toInstance(this.code);
  }
  
  @Pure
  public CodeConfig getCode() {
    return this.code;
  }
  
  public void setCode(final CodeConfig code) {
    this.code = code;
  }
}
