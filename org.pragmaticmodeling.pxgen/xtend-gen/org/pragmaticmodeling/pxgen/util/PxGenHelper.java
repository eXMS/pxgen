package org.pragmaticmodeling.pxgen.util;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.xbase.XExpression;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.pragmaticmodeling.pxgen.pxGen.File;
import org.pragmaticmodeling.pxgen.pxGen.Function;
import org.pragmaticmodeling.pxgen.pxGen.GenConf;
import org.pragmaticmodeling.pxgen.pxGen.GenerateInstruction;
import org.pragmaticmodeling.pxgen.pxGen.GenerationDirectory;
import org.pragmaticmodeling.pxgen.pxGen.GeneratorDefinition;
import org.pragmaticmodeling.pxgen.pxGen.PackageDeclaration;
import org.pragmaticmodeling.pxgen.pxGen.ParameterDeclaration;
import org.pragmaticmodeling.pxgen.pxGen.Project;
import org.pragmaticmodeling.pxgen.pxGen.PxGenElement;
import org.pragmaticmodeling.pxgen.pxGen.XtextGenerator;

@SuppressWarnings("all")
public class PxGenHelper {
  public static GeneratorDefinition getGeneratorDefinition(final JvmTypeReference reference) {
    GeneratorDefinition genDef = null;
    if ((reference != null)) {
      String simpleName = reference.getType().getSimpleName();
      EList<Resource> resources = reference.eResource().getResourceSet().getResources();
      for (int i = (resources.size() - 1); (i >= 0); i--) {
        {
          Resource r = resources.get(i);
          boolean _isPxGenResource = PxGenHelper.isPxGenResource(r);
          if (_isPxGenResource) {
            EObject _head = IterableExtensions.<EObject>head(r.getContents());
            XtextGenerator root = ((XtextGenerator) _head);
            if (((root != null) && (root.getPackage() != null))) {
              EList<PxGenElement> _elements = root.getPackage().getElements();
              for (final PxGenElement candidate : _elements) {
                if ((candidate instanceof GeneratorDefinition)) {
                  boolean _equals = ((GeneratorDefinition)candidate).getName().equals(simpleName);
                  if (_equals) {
                    genDef = ((GeneratorDefinition)candidate);
                  }
                }
              }
            }
          }
        }
      }
    }
    return genDef;
  }
  
  public static boolean isPxGenResource(final Resource r) {
    return "pxgen".equals(r.getURI().fileExtension());
  }
  
  public static List<File> files(final Project project) {
    return IterableExtensions.<File>toList(Iterables.<File>filter(project.getElements(), File.class));
  }
  
  public static List<GenerationDirectory> genDirs(final Project project) {
    return IterableExtensions.<GenerationDirectory>toList(Iterables.<GenerationDirectory>filter(project.getElements(), GenerationDirectory.class));
  }
  
  public static List<GeneratorDefinition> allExtendedDefinitions(final GeneratorDefinition definition) {
    ArrayList<GeneratorDefinition> result = new ArrayList<GeneratorDefinition>();
    GeneratorDefinition superDef = definition.getExtended();
    while ((superDef != null)) {
      {
        result.add(superDef);
        superDef = superDef.getExtended();
      }
    }
    return result;
  }
  
  public static List<XExpression> allFeatures(final GeneratorDefinition genDef) {
    ArrayList<XExpression> result = new ArrayList<XExpression>();
    ArrayList<GeneratorDefinition> allSuperDefs = new ArrayList<GeneratorDefinition>();
    allSuperDefs.add(genDef);
    List<GeneratorDefinition> _allExtendedDefinitions = PxGenHelper.allExtendedDefinitions(genDef);
    Iterables.<GeneratorDefinition>addAll(allSuperDefs, _allExtendedDefinitions);
    for (final GeneratorDefinition def : allSuperDefs) {
      result.addAll(PxGenHelper.features(def));
    }
    return result;
  }
  
  public static List<XExpression> allFeatures(final Project project) {
    ArrayList<XExpression> result = new ArrayList<XExpression>();
    GeneratorDefinition genDef = EcoreUtil2.<GeneratorDefinition>getContainerOfType(project, GeneratorDefinition.class);
    List<XExpression> _features = PxGenHelper.features(project);
    Iterables.<XExpression>addAll(result, _features);
    List<XExpression> _allFeatures = PxGenHelper.allFeatures(genDef);
    Iterables.<XExpression>addAll(result, _allFeatures);
    return result;
  }
  
  public static List<XExpression> features(final EObject object) {
    boolean _matched = false;
    if (object instanceof Project) {
      _matched=true;
      final Function1<XExpression, Boolean> _function = (XExpression e) -> {
        return Boolean.valueOf(PxGenHelper.isFeature(e));
      };
      return IterableExtensions.<XExpression>toList(IterableExtensions.<XExpression>filter(((Project)object).getElements(), _function));
    }
    if (!_matched) {
      if (object instanceof GeneratorDefinition) {
        _matched=true;
        final Function1<XExpression, Boolean> _function = (XExpression e) -> {
          return Boolean.valueOf(PxGenHelper.isFeature(e));
        };
        return IterableExtensions.<XExpression>toList(IterableExtensions.<XExpression>filter(((GeneratorDefinition)object).getElements(), _function));
      }
    }
    return new ArrayList<XExpression>();
  }
  
  public static boolean isFeature(final XExpression expression) {
    return (((expression instanceof Function) || (expression instanceof ParameterDeclaration)) || (expression instanceof Project));
  }
  
  public static String getModuleClassName(final GeneratorDefinition definition) {
    PackageDeclaration packageDeclaration = PxGenHelper.parentPackageDeclaration(definition);
    String _name = packageDeclaration.getName();
    String _plus = (_name + ".");
    String _firstUpper = StringExtensions.toFirstUpper(definition.getName());
    String _plus_1 = (_plus + _firstUpper);
    return (_plus_1 + "Module");
  }
  
  public static String className(final JvmTypeReference genDef) {
    return genDef.getQualifiedName();
  }
  
  public static String className(final GeneratorDefinition genDef) {
    PackageDeclaration packageDeclaration = PxGenHelper.parentPackageDeclaration(genDef);
    String _name = packageDeclaration.getName();
    String _plus = (_name + ".");
    String _name_1 = genDef.getName();
    return (_plus + _name_1);
  }
  
  public static String componentClassName(final GeneratorDefinition genDef) {
    String _className = PxGenHelper.className(genDef);
    return (_className + "Workflow");
  }
  
  public static String componentClassName(final JvmTypeReference genDef) {
    String _qualifiedName = genDef.getQualifiedName();
    return (_qualifiedName + "Workflow");
  }
  
  public static String standaloneSetupClassName(final GeneratorDefinition genDef) {
    String _className = PxGenHelper.className(genDef);
    return (_className + "StandaloneSetup");
  }
  
  public static String className(final File file) {
    boolean _eIsProxy = file.eIsProxy();
    if (_eIsProxy) {
      return "fixme";
    }
    PackageDeclaration packageDeclaration = PxGenHelper.parentPackageDeclaration(file);
    String _name = packageDeclaration.getName();
    String _plus = (_name + ".files.");
    String _firstUpper = StringExtensions.toFirstUpper(file.getName());
    String _plus_1 = (_plus + _firstUpper);
    return (_plus_1 + "File");
  }
  
  public static String className2(final Project project) {
    GeneratorDefinition parentDef = EcoreUtil2.<GeneratorDefinition>getContainerOfType(project, GeneratorDefinition.class);
    String prefix = PxGenHelper.className(parentDef);
    String _firstUpper = StringExtensions.toFirstUpper(project.getName());
    return ((prefix + ".") + _firstUpper);
  }
  
  public static String className(final Project project) {
    PackageDeclaration packageDeclaration = PxGenHelper.parentPackageDeclaration(project);
    String _name = packageDeclaration.getName();
    String _plus = (_name + ".");
    String _firstUpper = StringExtensions.toFirstUpper(project.getName());
    return (_plus + _firstUpper);
  }
  
  public static PackageDeclaration parentPackageDeclaration(final EObject eObject) {
    if ((eObject != null)) {
      return EcoreUtil2.<PackageDeclaration>getContainerOfType(eObject, PackageDeclaration.class);
    }
    return null;
  }
  
  public static List<Function> getFunctions(final File file) {
    return IterableExtensions.<Function>toList(Iterables.<Function>filter(file.getElements(), Function.class));
  }
  
  public static List<EObject> getInheritedMembers(final EObject object) {
    List<EObject> result = new ArrayList<EObject>();
    Iterable<ParameterDeclaration> _filter = Iterables.<ParameterDeclaration>filter(object.eContents(), ParameterDeclaration.class);
    Iterables.<EObject>addAll(result, _filter);
    EObject container = object.eContainer();
    while ((container != null)) {
      {
        Iterable<ParameterDeclaration> _filter_1 = Iterables.<ParameterDeclaration>filter(container.eContents(), ParameterDeclaration.class);
        Iterables.<EObject>addAll(result, _filter_1);
        container = container.eContainer();
      }
    }
    return result;
  }
  
  public static List<ParameterDeclaration> getParams(final EObject object) {
    return IterableExtensions.<ParameterDeclaration>toList(Iterables.<ParameterDeclaration>filter(object.eContents(), ParameterDeclaration.class));
  }
  
  public static String contextClassName(final File file, final boolean isInterface) {
    Project parentProject = EcoreUtil2.<Project>getContainerOfType(file, Project.class);
    if ((parentProject != null)) {
      return PxGenHelper.contextClassName(parentProject, isInterface);
    } else {
      GeneratorDefinition parentDef = EcoreUtil2.<GeneratorDefinition>getContainerOfType(file, GeneratorDefinition.class);
      String _contextClassName = null;
      if (parentDef!=null) {
        _contextClassName=PxGenHelper.contextClassName(parentDef, isInterface);
      }
      return _contextClassName;
    }
  }
  
  public static EObject parentContext(final File file) {
    Project parentProject = EcoreUtil2.<Project>getContainerOfType(file, Project.class);
    if ((parentProject != null)) {
      return parentProject;
    } else {
      GeneratorDefinition parentDef = EcoreUtil2.<GeneratorDefinition>getContainerOfType(file, GeneratorDefinition.class);
      return parentDef;
    }
  }
  
  public static String contextClassName(final Project project, final boolean isInterface) {
    boolean _eIsProxy = project.eIsProxy();
    if (_eIsProxy) {
      return "fixme";
    }
    PackageDeclaration packageDeclaration = PxGenHelper.parentPackageDeclaration(project);
    String _xifexpression = null;
    if (isInterface) {
      _xifexpression = "I";
    } else {
      _xifexpression = "";
    }
    String interfacePrefix = _xifexpression;
    String _name = packageDeclaration.getName();
    String _plus = (_name + ".");
    String _plus_1 = (_plus + interfacePrefix);
    String _firstUpper = StringExtensions.toFirstUpper(project.getName());
    String _plus_2 = (_plus_1 + _firstUpper);
    return (_plus_2 + "Context");
  }
  
  public static String contextClassName(final GeneratorDefinition genDef, final boolean isInterface) {
    PackageDeclaration packageDeclaration = PxGenHelper.parentPackageDeclaration(genDef);
    String _xifexpression = null;
    if (isInterface) {
      _xifexpression = "I";
    } else {
      _xifexpression = "";
    }
    String interfacePrefix = _xifexpression;
    String _name = packageDeclaration.getName();
    String _plus = (_name + ".");
    String _plus_1 = (_plus + interfacePrefix);
    String _firstUpper = StringExtensions.toFirstUpper(genDef.getName());
    String _plus_2 = (_plus_1 + _firstUpper);
    return (_plus_2 + "Context");
  }
  
  public static List<EObject> getAllContexts(final GeneratorDefinition definition) {
    ArrayList<EObject> _xblockexpression = null;
    {
      ArrayList<EObject> result = new ArrayList<EObject>();
      result.add(definition);
      Iterables.<EObject>addAll(result, Iterables.<Project>filter(definition.getElements(), Project.class));
      _xblockexpression = result;
    }
    return _xblockexpression;
  }
  
  public static boolean definesGetter(final EObject context, final String fieldName, final JvmTypeReference reference) {
    final Function1<Function, Boolean> _function = (Function f) -> {
      String _name = f.getName();
      String _firstUpper = StringExtensions.toFirstUpper(fieldName);
      String _plus = ("get" + _firstUpper);
      return Boolean.valueOf(Objects.equal(_name, _plus));
    };
    Function getter = IterableExtensions.<Function>head(IterableExtensions.<Function>filter(Iterables.<Function>filter(context.eContents(), Function.class), _function));
    return (getter != null);
  }
  
  public static boolean definesSetter(final EObject context, final String fieldName, final JvmTypeReference reference) {
    final Function1<Function, Boolean> _function = (Function f) -> {
      String _name = f.getName();
      String _firstUpper = StringExtensions.toFirstUpper(fieldName);
      String _plus = ("set" + _firstUpper);
      return Boolean.valueOf(Objects.equal(_name, _plus));
    };
    Function setter = IterableExtensions.<Function>head(IterableExtensions.<Function>filter(Iterables.<Function>filter(context.eContents(), Function.class), _function));
    return (setter != null);
  }
  
  public static File getFile(final Project project, final String fileName) {
    EList<XExpression> _elements = project.getElements();
    for (final XExpression e : _elements) {
      if (((e instanceof File) && PxGenHelper.fileIsNamed(((File) e), fileName))) {
        return ((File) e);
      }
    }
    GeneratorDefinition genDef = EcoreUtil2.<GeneratorDefinition>getContainerOfType(project, GeneratorDefinition.class);
    ArrayList<GeneratorDefinition> parentDefs = new ArrayList<GeneratorDefinition>();
    parentDefs.add(genDef);
    List<GeneratorDefinition> _allExtendedDefinitions = PxGenHelper.allExtendedDefinitions(genDef);
    Iterables.<GeneratorDefinition>addAll(parentDefs, _allExtendedDefinitions);
    for (final GeneratorDefinition def : parentDefs) {
      EList<XExpression> _elements_1 = def.getElements();
      for (final XExpression e_1 : _elements_1) {
        if (((e_1 instanceof File) && PxGenHelper.fileIsNamed(((File) e_1), fileName))) {
          return ((File) e_1);
        }
      }
    }
    return null;
  }
  
  public static List<? extends EObject> visibleFiles(final GenerateInstruction instruction) {
    ArrayList<File> _xblockexpression = null;
    {
      ArrayList<File> result = new ArrayList<File>();
      GeneratorDefinition genDef = EcoreUtil2.<GeneratorDefinition>getContainerOfType(instruction, GeneratorDefinition.class);
      Project project = EcoreUtil2.<Project>getContainerOfType(instruction, Project.class);
      List<GeneratorDefinition> allSuperDefs = PxGenHelper.allExtendedDefinitions(genDef);
      allSuperDefs.add(genDef);
      for (final GeneratorDefinition def : allSuperDefs) {
        result.addAll(PxGenHelper.files(def));
      }
      result.addAll(PxGenHelper.files(project));
      _xblockexpression = result;
    }
    return _xblockexpression;
  }
  
  public static List<File> files(final GeneratorDefinition definition) {
    return IterableExtensions.<File>toList(Iterables.<File>filter(definition.getElements(), File.class));
  }
  
  public static boolean fileIsNamed(final File e, final String fileName) {
    return fileName.equals(e.getName());
  }
  
  public static String getName(final Project e) {
    return e.getName();
  }
  
  public static String className(final GenConf genConf) {
    String result = "WorkflowComponent";
    String _simpleName = genConf.getGenerator().getType().getSimpleName();
    boolean _notEquals = (!Objects.equal(_simpleName, "void"));
    if (_notEquals) {
      String _qualifiedName = genConf.getGenerator().getQualifiedName();
      String _plus = (_qualifiedName + "WorkflowComponent");
      result = _plus;
    }
    return result;
  }
  
  public static String javaPackage(final EObject eObject) {
    PackageDeclaration pkg = EcoreUtil2.<PackageDeclaration>getContainerOfType(eObject, PackageDeclaration.class);
    if ((pkg != null)) {
      return pkg.getName();
    }
    return null;
  }
}
